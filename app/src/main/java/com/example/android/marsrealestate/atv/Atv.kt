package com.example.android.marsrealestate.atv

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class Atv (
        val id: Long,
        val userId: String,
        val model: String,
        val typeOfProduct: String,
        val manufacturer: String,
        val manufacturerCountry: String,
        val productIsNew: Boolean,
        val description: String,
//        val city: String,
//        val price: Int,
//        val category: String,
//        val seller: String,
//        val productCondition: String,
        val url: String,
        val mileage: Int,
        val firstRegistration: String,
        val cubicCapacity: Int,
        val powerSupply: String,
        val gearBox: String
) : Parcelable


