package com.example.android.marsrealestate.shoes

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.enums.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class ShoesRepository(val coroutineScope: CoroutineScope) {

    val shoes = MutableLiveData<List<Shoes>>()
   // val saveShoesObservable = MutableLiveData<ShoesSaveInfo>()
    val receiveShoesObservable = MutableLiveData<ShoesGetInfo>()

    val wordsToSearchShoesObservable = MutableLiveData<String>()
    val sexShoesObservable = MutableLiveData<String>()
    val sizeShoesObservable = MutableLiveData<String>()
    val colorShoesObservable = MutableLiveData<String>()
    val modelShoesObservable = MutableLiveData<String>()
    val manufacturerShoesObservable = MutableLiveData<String>()
    val manufacturerCountryShoesObservable = MutableLiveData<String>()
    val descriptionShoesObservable = MutableLiveData<String>()
    val cityShoesObservable = MutableLiveData<String>()
    val priceShoesObservable = MutableLiveData<String>()
    val typeOfShoesShoesObservable = MutableLiveData<String>()
    val sellerShoesObservable = MutableLiveData<String>()
    val productConditionShoesObservable = MutableLiveData<String>()
   // val lengthShoesObservable = MutableLiveData<String>()
    //val widthShoesObservable = MutableLiveData<String>()
    //val weightShoesObservable = MutableLiveData<String>()


    fun getShoesList() {
        receiveShoesObservable.postValue(ShoesGetInfo(Status.LOADING))
        Log.d("ShoesRepository", "  1")
        coroutineScope.launch {
            Log.d("ShoesRepository", " 2")
            var getPropertiesDeferred = RetrofitApi.retrofitApiService.getShoesList()

            try {
                var listResult = getPropertiesDeferred.await()
                shoes.value = listResult
                Log.d("ShoesRepository", "11 / shoes.value = ${shoes.value?.size}" )
                receiveShoesObservable.postValue(ShoesGetInfo(Status.SUCCESS))
            } catch (e: Exception) {
                Log.d("ShoesRepository", "$e")
                shoes.value = ArrayList()
                receiveShoesObservable.postValue(ShoesGetInfo(Status.ERROR_CONNECTION))
            }

        }
    }


    fun receiveShoesById(id:Long,isSaveNewDressOnServer:Boolean){
        receiveShoesObservable.postValue(ShoesGetInfo(Status.LOADING))

        if (isSaveNewDressOnServer) {
            receiveShoesObservable.postValue(ShoesGetInfo(Status.SUCCESS))
            Log.d("ShoesRepository", "getMarsRealEstateProperties / id = ${id} - means new Dress")

        } else
            coroutineScope.launch {
                Log.d("ShoesRepository", "getMarsRealEstateProperties / id = ${id}")
                var getPropertiesDeferred = RetrofitApi.retrofitApiService.getShoesById(id)
                try {
                    val shoesOne = getPropertiesDeferred.await()
                    Log.d("ShoesRepository", "dm-> try shoes = ${shoesOne.toString()}")
                    //userIdDressObservable.postValue(dressOne.userId)
                    modelShoesObservable.postValue(shoesOne.model)
                    cityShoesObservable.postValue(shoesOne.city)
                    priceShoesObservable.postValue(shoesOne.price)
                    typeOfShoesShoesObservable.postValue(shoesOne.typeOfShoes)
                    sellerShoesObservable.postValue(shoesOne.seller)
                    productConditionShoesObservable.postValue(shoesOne.productCondition)
                   // lengthShoesObservable.postValue(shoesOne.length)
                    //widthShoesObservable.postValue(shoesOne.width)
                    //weightShoesObservable.postValue(shoesOne.weight)
                    manufacturerShoesObservable.postValue(shoesOne.manufacturer)
                    manufacturerCountryShoesObservable.postValue(shoesOne.manufacturerCountry)
                    descriptionShoesObservable.postValue(shoesOne.description)
                    colorShoesObservable.postValue(shoesOne.color)
                    wordsToSearchShoesObservable.postValue(shoesOne.wordsToSearch)
                    sexShoesObservable.postValue(shoesOne.sex)
                    sizeShoesObservable.postValue(shoesOne.size)


                    receiveShoesObservable.postValue(ShoesGetInfo(Status.SUCCESS))
                } catch (e: Exception) {
                    Log.d("ShoesRepository", "catch ")
                    receiveShoesObservable.postValue(ShoesGetInfo(Status.ERROR_CONNECTION))
                }
            }
    }

}