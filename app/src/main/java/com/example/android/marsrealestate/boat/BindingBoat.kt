package com.example.android.marsrealestate.boat

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.R
import com.example.android.marsrealestate.boat.model.Boat
import com.example.android.marsrealestate.di.mv.boat.BoatApiStatus
import com.example.android.marsrealestate.repository.boat.BoatItemListener
import com.example.android.marsrealestate.ui.adapters.BoatRecyclerAdapter

@BindingAdapter("listDataBoates", "listenerBoat")
fun bindRecyclerViewPhones(recyclerView: RecyclerView, data: List<Boat>, listenerBoat: BoatItemListener) {
    val adapter = recyclerView.adapter as BoatRecyclerAdapter
    adapter.addItems(data)
    adapter.addListener(listenerBoat)
}



@BindingAdapter("boatApiStatus")
fun bindStatusPhoto(statusImageView: ImageView, status: BoatApiStatus?) {
    when (status) {
        BoatApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        BoatApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        BoatApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}