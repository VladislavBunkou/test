package com.example.android.marsrealestate.bicycle

import android.util.Log
import com.example.android.marsrealestate.bicycle.Bicycle
import com.example.android.marsrealestate.bicycle.BicycleItemListener
import com.example.android.marsrealestate.di.mv.BaseViewModel

class ItemBicycleViewModel: BaseViewModel<Any>() {

    var listener : BicycleItemListener? = null
    lateinit var bicycle: Bicycle

    fun bind(bicycle: Bicycle, listener : BicycleItemListener?){
        this.bicycle= bicycle
        this.listener = listener

    }

    fun onClickItem(){
        Log.d("", "Is clicked bicycle = ${bicycle.model}")
        listener?.onSubUserItemClick(bicycle)
    }
}