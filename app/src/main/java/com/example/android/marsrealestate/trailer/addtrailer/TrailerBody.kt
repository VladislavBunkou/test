package com.example.android.marsrealestate.trailer.addtrailer

import com.google.gson.annotations.SerializedName

data class TrailerBody(
        var userId: String = "",
        var model: String = "",
        var typeOfProduct: String = "trailer",
        var manufacturer: String = "",
        var manufacturerCountry: String = "",
        var productIsNew: Boolean = false,
        var url: String = "https://www.leonardusa.com/sites/default/files/styles/extra_large/public/images/category/category/Enclosed-Contractor-Series-Trailer-cat-pic_0.jpg?itok=ged5HK4i",
        var latitude: Double = -1.0,
        var longitude: Double = -1.0
)