package com.example.android.marsrealestate.bicycle.addNewBicycle

class ForSaveBicycle (

        var userId: String="",
        var model: String = "",
        var manufacturer: String = "",
        var manufacturerCountry: String = "",
        var typeOfProduct: String= "bicycle",
        var description: String = "",
        var productIsNew: Boolean = false,
        var imageId: String = "",
        var city: String = "",

        var latitude: Double=0.0,
        var longitude: Double=0.0,

        var price: String = "",
        var category: String = "",
        var productCondition: String = "",
        var wordToSearch: String = "",
        var species: String = "",
        var seller: String="",

        var folding: Boolean = false,
        var mountain: Boolean = false,
        var road: Boolean = false,
        var citybike: Boolean = false,
        var hibrid: Boolean = false
)




