package com.example.android.marsrealestate.utils

import android.util.Log
import java.io.File

class ValidationUtils {

    companion object {
        fun isFileHasGoodSize(pathStr: String): Boolean {
            val file = File(pathStr)
            val sizeInMb = file.length() / (1024.0 * 1024)
            Log.d("ValidationUtils", "dm:: size = $sizeInMb Mb")
            if (sizeInMb < 12.0) {
                return true
            }else {
                return false
            }
        }
    }



}

fun File.isFileHasGoodSize(): Boolean = this.length() < 12.0 / (1024.0 * 1024)
