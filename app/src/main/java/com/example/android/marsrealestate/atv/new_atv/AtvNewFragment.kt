package com.example.android.marsrealestate.atv.new_atv

import android.app.Activity
import android.content.ContentResolver
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentAtvNewBinding
import com.example.android.marsrealestate.atv.AtvBody
import com.example.android.marsrealestate.atv.new_atv.atw_enum.AtvManufacturerCountryEnum
import com.example.android.marsrealestate.atv.new_atv.atw_enum.AtvManufacturerEnum
import com.example.android.marsrealestate.atv.new_atv.atw_enum.AtvTypeOfProductEnum
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso

class AtvNewFragment: Fragment(), AtvNewNavigator {
    lateinit var binding: FragmentAtvNewBinding
    private val viewModel: AtvNewViewModel by lazy {
        ViewModelProviders.of(this).get(AtvNewViewModel::class.java)
    }

    private lateinit var storage: FirebaseStorage
    private lateinit var storageRef: StorageReference
    private var imageUri: Uri = Uri.EMPTY
    private var uploadedImageUri: Uri = Uri.EMPTY
    private var fileIsSelected = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = FragmentAtvNewBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        viewModel.setNavigator(this)

        storage = FirebaseStorage.getInstance()
        storageRef = storage.reference

        return binding.root
    }

    override fun openFileChooser() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, viewModel.PICK_IMAGE_REQUEST)
        fileIsSelected = true
    }

    private fun getFileExtension(uri: Uri): String? {
        val cR: ContentResolver = activity!!.contentResolver
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(cR.getType(uri))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == viewModel.PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK
                && data?.data != null) {
            imageUri = data.data!!
            Picasso.get().load(imageUri).into(binding.imgImage)
        }
    }

    override fun uploadFile() {
        val fileReference: StorageReference = storageRef.child(
                "image/atv/${System.currentTimeMillis()}.${getFileExtension(imageUri)}")

        val uploadTask = fileReference.putFile(imageUri)
                .addOnCompleteListener {
                    val handler = Handler()
                    handler.postDelayed({ binding.imgProgress.progress = 0 }, 500)
                    Picasso.get().load(R.drawable.ic_image_is_uploded).into(binding.imgImage)
                }
                .addOnFailureListener { e ->
                    Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
                }
                .addOnProgressListener {
                    val progress = 100.0 * it.bytesTransferred / it.totalByteCount
                    binding.imgProgress.progress = progress.toInt()
                }

        val urlTask = uploadTask.continueWithTask {
            if (!it.isSuccessful) {
                it.exception?.let { ex ->
                    throw ex
                }
            }
            fileReference.downloadUrl
        }
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        uploadedImageUri = it.result ?: Uri.EMPTY
                        Log.d("dm->2", "Url: $uploadedImageUri")
                        Toast.makeText(context, "Image uploaded!", Toast.LENGTH_SHORT).show()
                    } else {
                        // Handle failures
                        Toast.makeText(context, "Something wrong!!!", Toast.LENGTH_SHORT).show()
                    }
                }
    }

    override fun getPhotoId() = uploadedImageUri.toString()

    override fun showToast(atv: AtvBody) {
        val output: String
        atv.apply {
            output =
                    "Model: $model;\n" +
                    "Type: $typeOfProduct;\n" +
                    "Manufacturer: $manufacturer;\n" +
                    "Country: $manufacturerCountry;\n" +
                    "Mileage: $mileage;\n" +
                    "First registration: $firstRegistration;\n" +
                    "Cubic capacity: $cubicCapacity;\n" +
                    "Power supply: $powerSupply;\n" +
                    "Gear box: $gearBox;\n" +
                    "Is product new: ${if (productIsNew) "Yes" else "No"};\n" +
                    "Description: $description"
        }
        Toast.makeText(context, output, Toast.LENGTH_LONG).show()
    }

    override fun showErrors(isModelFilled: Boolean, isManufacturerFilled: Boolean,
                            isManufacturerCountryFilled: Boolean, isTypeOfProductFilled: Boolean,
                            isDescriptionFilled: Boolean, isMileageFilled: Boolean,
                            isFirstRegistrationFilled: Boolean, isCubicCapacityFilled: Boolean,
                            isPowerSupplyFilled: Boolean, isGearBoxFilled: Boolean, isUriFilled: Boolean) {
        viewModel.apply {
            if (!isModelFilled) binding.atvModel.error = "Input model"
            else binding.atvModel.error = null
            if (!isTypeOfProductFilled) binding.atvType.error = "Input type"
            else binding.atvType.error = null
            if (!isManufacturerFilled) binding.atvManufacturer.error = "Input manufacturer"
            else binding.atvManufacturer.error = null
            if (!isManufacturerCountryFilled) binding.atvCountry.error = "Input country"
            else binding.atvCountry.error = null
            if (!isMileageFilled) binding.atvMileage.error = "Input mileage"
            else binding.atvMileage.error = null
            if (!isFirstRegistrationFilled) binding.atvFirstRegistration.error = "Input first registration date"
            else binding.atvFirstRegistration.error = null
            if (!isCubicCapacityFilled) binding.atvCubic.error = "Input cubic capacity"
            else binding.atvCubic.error = null
            if (!isPowerSupplyFilled) binding.atvPower.error = "Input power supply"
            else binding.atvPower.error = null
            if (!isGearBoxFilled) binding.atvGearBox.error = "Input gear box"
            else binding.atvGearBox.error = null
            if (!isDescriptionFilled) binding.atvDescription.error = "Input description"
            else binding.atvDescription.error = null
            if (!isUriFilled) Toast.makeText(context, "Error! Choose and upload image to add ATV", Toast.LENGTH_SHORT).show()
        }
    }

    override fun createSelectTypeOfProductDialog() {
        val listOfEnum = listOf(
                AtvTypeOfProductEnum.UTILITY.get(),
                AtvTypeOfProductEnum.SPORT.get(),
                AtvTypeOfProductEnum.RHINO.get(),
                AtvTypeOfProductEnum.CHILDREN.get()
        )

        var currentIndex = -1

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setSingleChoiceItems(listOfEnum.toTypedArray(),
                        currentIndex) { dialog, which ->
                    currentIndex = which
                    viewModel.updateTypeOfProduct(which + 1)
                    dialog.dismiss()
                    binding.atvType.error = null
                }
                setTitle("Type")
                setNegativeButton("Cancel") { _, _ ->
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }

    override fun createSelectManufacturerDialog() {
        val listOfEnum = listOf(
                AtvManufacturerEnum.HONDA.get(),
                AtvManufacturerEnum.POLARIS.get(),
                AtvManufacturerEnum.YAMAHA.get(),
                AtvManufacturerEnum.SUZUKI.get(),
                AtvManufacturerEnum.KAWASAKI.get(),
                AtvManufacturerEnum.ARCTIC_CAT.get(),
                AtvManufacturerEnum.OTHER.get()
        )

        var currentIndex = -1

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setSingleChoiceItems(listOfEnum.toTypedArray(),
                        currentIndex) { dialog, which ->
                    currentIndex = which
                    viewModel.updateManufacturer(which + 1)
                    dialog.dismiss()
                    binding.atvManufacturer.error = null
                }
                setTitle("Manufacturer")
                setNegativeButton("Cancel") { _, _ ->
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }

    override fun createSelectManufacturerCountryDialog() {
        val listOfEnum = listOf(
                AtvManufacturerCountryEnum.CHINA.get(),
                AtvManufacturerCountryEnum.INDONESIA.get(),
                AtvManufacturerCountryEnum.JAPAN.get(),
                AtvManufacturerCountryEnum.MALAYSIA.get(),
                AtvManufacturerCountryEnum.OTHER.get()
        )

        var currentIndex = -1

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setSingleChoiceItems(listOfEnum.toTypedArray(),
                        currentIndex) { dialog, which ->
                    currentIndex = which
                    viewModel.updateManufacturerCountry(which + 1)
                    dialog.dismiss()
                    binding.atvCountry.error = null
                }
                setTitle("Country")
                setNegativeButton("Cancel") { _, _ ->
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }
}