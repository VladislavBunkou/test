package com.example.android.marsrealestate.ui.trailer


import android.app.Activity
import android.content.ContentResolver
import android.content.Intent
import android.net.Uri
import android.Manifest
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentNewTrailerBinding
import com.example.android.marsrealestate.di.mv.nav.NavViewModel
import com.example.android.marsrealestate.di.mv.trailer.TrailerCreateViewModel
import com.example.android.marsrealestate.enums.Status
import com.example.android.marsrealestate.itemInCabinet
import com.example.android.marsrealestate.repository.trailer.TrailerSaveInfo
import com.example.android.marsrealestate.ui.adapters.TrailerNavigator
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
class NewTrailer : Fragment(), TrailerNavigator {

    lateinit var binding: FragmentNewTrailerBinding
    private val viewModel by viewModels<TrailerCreateViewModel>()

    private lateinit var navViewModel: NavViewModel

    //For upload image
    lateinit var storage: FirebaseStorage
    lateinit var storageRef: StorageReference
    lateinit var imageView: ImageView
    lateinit var textResult: TextView

    private var imageUri: Uri = Uri.EMPTY
    private var uploadedImageUri: Uri = Uri.EMPTY
    private var fileIsSelected = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val application = requireNotNull(activity).application

        binding = FragmentNewTrailerBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        viewModel.setNavigator(this)

        imageView = binding.imageView
        textResult = binding.textViewShowUploads
        storage = FirebaseStorage.getInstance()
        storageRef = storage.reference

        activity?.let{
            navViewModel = ViewModelProviders.of(it).get(NavViewModel::class.java)
        }

        navViewModel.id.observe(    this, Observer {
            if (navViewModel.isCreateNew.value == true) {
                viewModel.receiveTrailerById(navViewModel.id.value ?: 1, true)
            }else {
                viewModel.receiveTrailerById(navViewModel.id.value ?: 1, false)
                navViewModel.isCreateNew.postValue(true)
        }
        })

        binding.buttonChooseFile.setOnClickListener {
           preOpenFileChooserWithPermissionCheck() // step 3 -add this fun (it is genegated!!)
        }
//
//        binding.buttonSaveTrailer.setOnClickListener{
//            preSaveTrailerButtonWithPermissionCheck()
//        }

        return binding.root
    }

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun preOpenFileChooser(){
        openFileChooser()
    }


    @OnPermissionDenied(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun permissionDeniedImage() {
        Log.d("TrailerNewFragment", "dm:: OnPermissionDenied")
    }
    // step 4
    @OnNeverAskAgain(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun neverAskAgainImage() {
        Log.d("TrailerNewFragment", "dm:: OnNeverAskAgain")
    }


    override fun showToast(model: String) {
        Toast.makeText(context, "model: $model", Toast.LENGTH_SHORT).show()
    }

    override fun errorMessage(isModelFilled: Boolean, isCountryFilled: Boolean) {
        if(!isModelFilled) {
            binding.trailerModelLayout.isErrorEnabled = true
            binding.trailerModelLayout.error = "Enter model"
        } else {
            binding.trailerModelLayout.isErrorEnabled = false
        }

        if(!isCountryFilled) {
            binding.trailerCountryLayout.isErrorEnabled = true
            binding.trailerCountryLayout.error = "Enter country"
        } else {
            binding.trailerCountryLayout.isErrorEnabled = false
        }
    }


    private var currentSelectedManufacturerIndex = -1

    override fun createSelectManufacturerDialog() {
        val listCategory = listOf("Fruehauf", "Great Dane", "MAC", "Manac", "Other")

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)

            builder.apply {
                setSingleChoiceItems(listCategory.toTypedArray(),
                        currentSelectedManufacturerIndex) { dialog, which ->
                    currentSelectedManufacturerIndex = which
                    viewModel.updateManufacturer(which + 1)
                    dialog.dismiss()
                }
                setTitle("Trailer producer")
                setNegativeButton("Cancel") { _, _ -> }
            }
            builder.create()

        }
        alertDialog?.show()
    }

    override fun openFileChooser() {
        textResult.text = "_"

        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, viewModel.PICK_IMAGE_REQUEST)
        fileIsSelected = true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == viewModel.PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK
                && data?.data != null) {
            imageUri = data.data!!
            Picasso.get().load(imageUri).into(imageView)
        }
    }

    private fun getFileExtension(uri: Uri): String? {
        val cR: ContentResolver = activity!!.contentResolver
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(cR.getType(uri))
    }

    override fun uploadFile() {
        if (imageUri != null) {
            val fileReference: StorageReference = storageRef.child(
                    "image/trailer/${System.currentTimeMillis()}.${getFileExtension(imageUri)}"
            )

            val uploadTask = fileReference.putFile(imageUri)
                    .addOnCompleteListener(){
                        val handler = Handler()
                        handler.postDelayed( {binding.progressBar.progress = 0} , 500)
                        Toast.makeText(context, "Upload succed", Toast.LENGTH_SHORT).show()
                        Picasso.get().load(R.drawable.ic_image_is_uploded).into(imageView)
                    }
                    .addOnFailureListener{ e ->
                        Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
                    }
                    .addOnProgressListener {
                        val progress = 100.0 * it.bytesTransferred / it.totalByteCount
                        binding.progressBar.progress = progress.toInt()
                    }

            val urlTask = uploadTask.continueWithTask{
                if(!it.isSuccessful)
                    it.exception?.let { ex ->
                        throw ex
                    }
                fileReference.downloadUrl
            }
                    .addOnCompleteListener {

                        if (it.isSuccessful) {
                            uploadedImageUri = it.result ?: Uri.EMPTY
                            Toast.makeText(context, "Url: $uploadedImageUri", Toast.LENGTH_SHORT).show()
                            Log.d("UploadURL", "Url: $uploadedImageUri")
                            //activity?.let { it1 -> viewModel.onSendButton(it1) }
                            viewModel.onSendButton()
                            textResult.text = "Image is uploaded!"

                        } else {
                            binding.root.isClickable = true
                            Toast.makeText(context, "Something wrong", Toast.LENGTH_SHORT).show()
                        }
                    }
        } else {
            Toast.makeText(context, "No file selected", Toast.LENGTH_SHORT).show()
        }

    }

    override fun getPhotoId() = uploadedImageUri.toString()
    override fun onProductIsNew() {
        viewModel.productIsNew.set(binding.switchIsTrailerNew.isChecked)
    }

    override fun onDestroy() {
        super.onDestroy()
        itemInCabinet = false
        Log.d("GridItem", "close new trailer fragment $itemInCabinet")
    }



}
