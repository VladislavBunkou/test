package com.example.android.marsrealestate.bicycle

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class Bicycle (

        val id: Long,
        val userId: String,
        val model: String,
        val typeOfProduct: String,
        var productIsNew: Boolean,
        var description: String,
        val city: String,

        val seller: String,

        val latitude: Double,
        val longitude: Double,

        val price: String,
        val category: String,
        val productCondition: String,
        val wordsToSearch: String,
        val species: String,

        val manufacturer: String,
        val manufacturerCountry: String,

        val url: String

): Parcelable

@Parcelize
class BicycleResponse(
        val id: Long,
        val userId: String,
        val model: String,
        val typeOfProduct: String,
        var productIsNew: Boolean,
        var description: String,
        val city: String,

        val latitude: Double,
        val longitude: Double,

        val price: String,
        val category: String,
        val productCondition: String,
        val wordsToSearch: String,
        val species: String,

        val manufacturer: String,
        val manufacturerCountry: String,

        val url: String

) : Parcelable