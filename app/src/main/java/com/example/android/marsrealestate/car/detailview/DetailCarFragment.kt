package com.example.android.marsrealestate.car.detailview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentCar2Binding

class DetailCarFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater,  container: ViewGroup?, savedInstanceState: Bundle? ): View? {

        val application = requireNotNull(activity).application
        val binding = FragmentCar2Binding.inflate(inflater)
        binding.lifecycleOwner = this

        val car = DetailCarFragmentArgs.fromBundle(arguments!!).selectedProperty
        val viewModelFactory = DetailCarViewModelFactory(car, application)
        binding.viewModel = ViewModelProviders.of(this, viewModelFactory).get(DetailCarViewModel::class.java)

        return binding.root
    }

}