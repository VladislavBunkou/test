package com.example.android.marsrealestate.atv.new_atv.atw_enum

enum class AtvManufacturerCountryEnum (val id: String) {
    OTHER("0"){ override fun get(): String = "Other" },
    CHINA("1"){ override fun get(): String = "China" },
    INDONESIA("2"){ override fun get(): String = "Indonesia" },
    JAPAN("3"){ override fun get(): String = "Japan" },
    MALAYSIA("4"){ override fun get(): String = "Malaysia" };

    abstract fun get(): String
}