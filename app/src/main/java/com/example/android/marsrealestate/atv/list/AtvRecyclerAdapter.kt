package com.example.android.marsrealestate.atv.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.GridViewItemAtvBinding
import com.example.android.marsrealestate.atv.Atv

class AtvRecyclerAdapter(private val onClickListenerAtv: OnClickListenerAtv) : ListAdapter<Atv, AtvRecyclerAdapter.AtvPropertyViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AtvPropertyViewHolder
            = AtvPropertyViewHolder(GridViewItemAtvBinding.inflate(LayoutInflater.from(parent.context)))

    override fun onBindViewHolder(holder: AtvPropertyViewHolder, position: Int) {
        val property = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListenerAtv.onClick(property)
        }
        holder.bind(property)
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Atv>() {
        override fun areItemsTheSame(oldItem: Atv, newItem: Atv) = oldItem === newItem
        override fun areContentsTheSame(oldItem: Atv, newItem: Atv) = oldItem.model == newItem.model
    }

    class AtvPropertyViewHolder(private var binding: GridViewItemAtvBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(atv: Atv) {
            binding.property = atv
            binding.executePendingBindings()
        }

    }

    class OnClickListenerAtv(val clickListener: (atv: Atv) -> Unit) {
        fun onClick(atv: Atv) = clickListener(atv)
    }
}