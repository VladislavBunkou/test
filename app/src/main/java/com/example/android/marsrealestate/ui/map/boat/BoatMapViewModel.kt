package com.example.android.marsrealestate.ui.map.boat

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.boat.model.Boat
import com.example.android.marsrealestate.network.RetrofitApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class BoatMapViewModel() : ViewModel() {
    private val TAG = "GoogleMapFragment2VM"

    //    val status = MutableLiveData<MainApiStatus>()
    val boats = MutableLiveData<List<Boat>>()

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
        getListBoats()
    }

    fun getListBoats(str: String = "") {
        coroutineScope.launch {
            var getPropertiesDeferred = if (str.isEmpty()) RetrofitApi.retrofitApiService.getBoatList()
            else RetrofitApi.retrofitApiService.getBoatList(str)
            try {
//                status.value = MainApiStatus.LOADING
                // this will run on a thread managed by Retrofit
                val listResult = getPropertiesDeferred.await()
//                status.value = MainApiStatus.DONE
                boats.value = listResult
            } catch (e: Exception) {
//                status.value = MainApiStatus.ERROR
                boats.value = listOf()
            }
        }
    }
}