package com.example.android.marsrealestate.guitar.guitar_add.guitar_enum

enum class GuitarTypeOfProductEnum (val id: String) {
    ACOUSTIC("1"){ override fun get(): String = "Acoustic" },
    BASS("2"){ override fun get(): String = "Bass" },
    ELECTROACOUSTIC("3"){ override fun get(): String = "Electro-acoustic" },
    ELECTRIC("4"){ override fun get(): String = "Electric" };

    abstract fun get(): String
}