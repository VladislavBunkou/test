package com.example.android.marsrealestate.di.mv.prod

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.model.Prod
import com.example.android.marsrealestate.model.ProdOne
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class ProdDetailViewModel(prod: Prod, app: Application) : AndroidViewModel(app) {


    private val _selectedProperty = MutableLiveData<ProdOne>()
    val selectedProperty: LiveData<ProdOne>
        get() = _selectedProperty

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
//        _selectedProperty.value = marsProperty


        if(prod.typeOfProduct == "tablet"){
            Log.d("DetailViewModel", "dm-> typeOfProduct is Tablet")
        }
        Log.d("DetailViewModel", "dm-> typeOfProduct is Tablet")
        getMarsRealEstateProperties(prod.id)
    }







    private fun getMarsRealEstateProperties(id: Long) {
        coroutineScope.launch {

            //            var getPropertiesDeferred = MarsApi.retrofitService.getProperties(filter.value)
            var getPropertiesDeferred = RetrofitApi.retrofitApiService.getProd(id)
            try {
//                _status.value = MarsApiStatus.LOADING
                // this will run on a thread managed by Retrofit
                val listResult = getPropertiesDeferred.await()

                Log.d("DetailViewModel", "dm-> try = $listResult")
//                _status.value = MarsApiStatus.DONE
//                listResult.id = id
                _selectedProperty.value = listResult
            } catch (e: Exception) {
//                _status.value = MarsApiStatus.ERROR
//                _selectedProperty.value = Prod()
                Log.d("DetailViewModel", "dm-> catch ")
            }
        }
    }


    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}

