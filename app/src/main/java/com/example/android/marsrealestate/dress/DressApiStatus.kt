package com.example.android.marsrealestate.dress

enum class DressApiStatus {
    LOADING, ERROR, DONE
}