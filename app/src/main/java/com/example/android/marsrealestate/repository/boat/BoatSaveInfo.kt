package com.example.android.marsrealestate.repository.boat

import com.example.android.marsrealestate.enums.ResponseCode
import com.example.android.marsrealestate.enums.Status


class BoatSaveInfo(
//        protected
        var status: Status,
        val code: ResponseCode = ResponseCode.SUCCESS,
        val message: String = "")


class BoatGetInfo(
//        protected
        var status: Status,
        val code: ResponseCode = ResponseCode.SUCCESS,
        val message: String = "")