package com.example.android.marsrealestate.shoes

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

@BindingAdapter("listDataShoes", "listenerShoes")
fun bindRecyclerViewShoes(recyclerView: RecyclerView, listDataShoes: List<Shoes>, listenerShoes: ShoesItemListener) {
    val adapter = recyclerView.adapter as ShoesRecyclerAdapter
    adapter.addItems(listDataShoes)
    adapter.addListener(listenerShoes)
}