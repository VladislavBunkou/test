package com.example.android.marsrealestate.dress.addDress

enum class DressSizeType(val id: String) {

    XS("1"){ override fun getSizeType(): String = "XS" },
    S("2"){ override fun getSizeType(): String = "S" },
    M("3"){ override fun getSizeType(): String = "M" },
    L("4"){ override fun getSizeType(): String = "L" },
    XL("5"){ override fun getSizeType(): String = "XL" },
    ONESIZE("6"){ override fun getSizeType(): String = "One size" },
    OTHER("7"){ override fun getSizeType(): String = "Other" };

    abstract fun getSizeType(): String
}