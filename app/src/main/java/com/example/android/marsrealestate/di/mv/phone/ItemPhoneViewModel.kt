package com.example.android.marsrealestate.di.mv.phone

import android.util.Log
import com.example.android.marsrealestate.di.mv.BaseViewModel
import com.example.android.marsrealestate.model.Phone
import com.example.android.marsrealestate.repository.phone.PhoneItemListener

class ItemPhoneViewModel : BaseViewModel<Any>(){

    var listener : PhoneItemListener? = null
    lateinit var phone: Phone

    fun bind(phone: Phone, listener : PhoneItemListener){
        this.phone = phone
        this.listener = listener
    }

      fun onClickItem(){
        Log.d("", "dm-> Is clicked phone = ${phone.model}")
        listener?.onSubUserItemClick(phone)
    }

}