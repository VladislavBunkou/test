package com.example.android.marsrealestate.di

import android.content.Context
import com.example.android.marsrealestate.MainActivity

import com.example.android.marsrealestate.ui.phone.PhonesFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@Component// (modules = [StorageModule::class])
interface AppComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): AppComponent
    }

    fun loginComponent(): PhoneCreateComponent.Factory
//    fun inject(activity: PhonesFragment)
//    fun inject(activity: MainActivity)

}