package com.example.android.marsrealestate.guitar.guitar_add

import android.util.Log
import androidx.databinding.ObservableField
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.di.mv.BaseViewModel
import com.example.android.marsrealestate.guitar.Guitar
import com.example.android.marsrealestate.guitar.guitar_add.guitar_enum.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GuitarAddViewModel : BaseViewModel<GuitarAddNavigator>() {
    //var context: GuitarAddFragment? = null

    var model = ObservableField<String>("")
    var typeOfProduct = ObservableField<String>("")
    var manufacturer = ObservableField<String>("")
    var manufacturerCountry = ObservableField<String>("")
    var guitarNeck = ObservableField<String>("")
    var guitarBody = ObservableField<String>("")
    var guitarScale = ObservableField<String>("")
    var scaleNumberOfFrets = ObservableField<String>("")
    var guitarColor = ObservableField<String>("")
    var productIsNew = false
    var description = ObservableField<String>("")
    //var userId = ObservableField<String>("")

    var typeOfProductEnum: GuitarTypeOfProductEnum? = null
    var manufacturerEnum: GuitarManufacturerEnum? = null
    var manufacturerCountryEnum: GuitarManufacturerCountryEnum? = null
    var guitarNeckEnum: GuitarNeckEnum? = null
    var guitarBodyEnum: GuitarBodyEnum? = null
    var scaleNumberOfFretsEnum: GuitarNumberOfFretsEnum? = null

    //var navigateToGuitarCharacteristics = MutableLiveData<Boolean>(false)

    var PICK_IMAGE_REQUEST: Int = 1

    private var viewModelJob = Job()
    private val coroutineScope =
            CoroutineScope(viewModelJob + Dispatchers.Main)

    fun onSendButton() {
        if (checkFields()) {
            val imageId = getNavigator()?.getPhotoId() ?: ""
            val guitar = Guitar(model.get()!!, typeOfProductEnum!!.get(), manufacturerEnum!!.get(),
                    manufacturerCountryEnum!!.get(), imageId, guitarNeckEnum!!.get(),
                    guitarBodyEnum!!.get(), guitarScale.get()!!, scaleNumberOfFretsEnum!!.get(),
                    guitarColor.get()!!, productIsNew, description.get()!!, "some user")

            sendPost(guitar)
            getNavigator()?.showToast(guitar)

            model.set("")
            typeOfProductEnum = null
            manufacturerEnum = null
            manufacturerCountryEnum = null
            guitarNeckEnum = null
            guitarBodyEnum = null
            guitarScale.set("")
            scaleNumberOfFretsEnum = null
            guitarColor.set("")
            productIsNew = false
            description.set("")
            //userId.set("")
        }
    }

    private fun sendPost(newGuitar: Guitar) {
        coroutineScope.launch {
            RetrofitApi.retrofitApiService
                    .addGuitar(newGuitar).enqueue(object : Callback<Guitar> {
                        override fun onResponse(call: Call<Guitar>, response: Response<Guitar>) {
                            Log.d("GuitarAddViewModel", "Guitar added successfully")
                        }

                        override fun onFailure(call: Call<Guitar>, t: Throwable) {
                            Log.d("GuitarAddViewModel", "Error occurred while adding guitar")
                        }
                    })
        }
    }

    private fun checkFields(): Boolean {
        var isModelCorrect = true
        var isTypeOfProductCorrect = true
        var isManufacturerCorrect = true
        var isManufacturerCountryCorrect = true
        var isGuitarNeckCorrect = true
        var isGuitarBodyCorrect = true
        var isGuitarScaleCorrect = true
        var isScaleNumberOfFretsCorrect = true
        var isGuitarColorCorrect = true
        var isDescriptionCorrect = true
        var isUriCorrect = true

        if (model.get().isNullOrEmpty()) isModelCorrect = false
        if (typeOfProductEnum?.get().isNullOrEmpty()) isTypeOfProductCorrect = false
        if (manufacturerEnum?.get().isNullOrEmpty()) isManufacturerCorrect = false
        if (manufacturerCountryEnum?.get().isNullOrEmpty()) isManufacturerCountryCorrect = false
        if (guitarNeckEnum?.get().isNullOrEmpty()) isGuitarNeckCorrect = false
        if (guitarBodyEnum?.get().isNullOrEmpty()) isGuitarBodyCorrect = false
        if (guitarScale.get().isNullOrEmpty() || guitarScale.get()?.toIntOrNull() == null) isGuitarScaleCorrect = false
        if (scaleNumberOfFretsEnum?.get().isNullOrEmpty()) isScaleNumberOfFretsCorrect = false
        if (guitarColor.get().isNullOrEmpty()) isGuitarColorCorrect = false
        if (description.get().isNullOrEmpty()) isDescriptionCorrect = false
        if (getNavigator()?.getPhotoId().isNullOrEmpty()) isUriCorrect = false

        return if (isModelCorrect && isTypeOfProductCorrect &&
                isManufacturerCorrect && isManufacturerCountryCorrect &&
                isGuitarNeckCorrect && isGuitarBodyCorrect &&
                isGuitarScaleCorrect && isScaleNumberOfFretsCorrect &&
                isGuitarColorCorrect && isDescriptionCorrect && isUriCorrect) {
            true
        } else {
            getNavigator()?.showErrors(isModelCorrect, isTypeOfProductCorrect,
                    isManufacturerCorrect, isManufacturerCountryCorrect,
                    isGuitarNeckCorrect, isGuitarBodyCorrect,
                    isGuitarScaleCorrect, isScaleNumberOfFretsCorrect,
                    isGuitarColorCorrect, isDescriptionCorrect, isUriCorrect)
            false
        }
    }

    fun updateTypeOfProduct(selectedIndex:  Int){
        typeOfProductEnum = when (selectedIndex.toString()){
            GuitarTypeOfProductEnum.ACOUSTIC.id -> GuitarTypeOfProductEnum.ACOUSTIC
            GuitarTypeOfProductEnum.BASS.id -> GuitarTypeOfProductEnum.BASS
            GuitarTypeOfProductEnum.ELECTROACOUSTIC.id -> GuitarTypeOfProductEnum.ELECTROACOUSTIC
            else -> GuitarTypeOfProductEnum.ELECTRIC
        }
        typeOfProduct.set(typeOfProductEnum!!.get())
    }

    fun updateManufacturer(selectedIndex:  Int){
        manufacturerEnum = when (selectedIndex.toString()){
            GuitarManufacturerEnum.YAMAHA.id -> GuitarManufacturerEnum.YAMAHA
            GuitarManufacturerEnum.GIBSON.id -> GuitarManufacturerEnum.GIBSON
            GuitarManufacturerEnum.FENDER.id -> GuitarManufacturerEnum.FENDER
            GuitarManufacturerEnum.IBANEZ.id -> GuitarManufacturerEnum.IBANEZ
            GuitarManufacturerEnum.WASHBURN.id -> GuitarManufacturerEnum.WASHBURN
            GuitarManufacturerEnum.BEHRINGER.id -> GuitarManufacturerEnum.BEHRINGER
            GuitarManufacturerEnum.CORT.id -> GuitarManufacturerEnum.CORT
            else -> GuitarManufacturerEnum.OTHER
        }
        manufacturer.set(manufacturerEnum!!.get())
    }

    fun updateManufacturerCountry(selectedIndex:  Int){
        manufacturerCountryEnum = when(selectedIndex.toString()){
            GuitarManufacturerCountryEnum.JAPAN.id -> GuitarManufacturerCountryEnum.JAPAN
            GuitarManufacturerCountryEnum.CHINA.id -> GuitarManufacturerCountryEnum.CHINA
            GuitarManufacturerCountryEnum.INDONESIA.id -> GuitarManufacturerCountryEnum.INDONESIA
            GuitarManufacturerCountryEnum.KOREA.id -> GuitarManufacturerCountryEnum.KOREA
            GuitarManufacturerCountryEnum.AUSTRALIA.id -> GuitarManufacturerCountryEnum.AUSTRALIA
            GuitarManufacturerCountryEnum.MEXICO.id -> GuitarManufacturerCountryEnum.MEXICO
            GuitarManufacturerCountryEnum.USA.id -> GuitarManufacturerCountryEnum.USA
            GuitarManufacturerCountryEnum.UK.id -> GuitarManufacturerCountryEnum.UK
            GuitarManufacturerCountryEnum.FRANCE.id -> GuitarManufacturerCountryEnum.FRANCE
            GuitarManufacturerCountryEnum.GERMANY.id -> GuitarManufacturerCountryEnum.GERMANY
            else -> GuitarManufacturerCountryEnum.OTHER
        }
        manufacturerCountry.set(manufacturerCountryEnum!!.get())
    }

    fun updateGuitarNeck(selectedIndex:  Int){
        guitarNeckEnum = when(selectedIndex.toString()){
            GuitarNeckEnum.ALDER.id -> GuitarNeckEnum.ALDER
            GuitarNeckEnum.ASH.id -> GuitarNeckEnum.ASH
            GuitarNeckEnum.BASSWOOD.id -> GuitarNeckEnum.BASSWOOD
            GuitarNeckEnum.MAHOGANY.id -> GuitarNeckEnum.MAHOGANY
            GuitarNeckEnum.MAPLE.id -> GuitarNeckEnum.MAPLE
            else -> GuitarNeckEnum.OTHER
        }
        guitarNeck.set(guitarNeckEnum!!.get())
    }

    fun updateGuitarBody(selectedIndex:  Int){
        guitarBodyEnum = when(selectedIndex.toString()){
            GuitarBodyEnum.DREADNOUGHT.id -> GuitarBodyEnum.DREADNOUGHT
            GuitarBodyEnum.PARLOUR.id -> GuitarBodyEnum.PARLOUR
            GuitarBodyEnum.JUMBO.id -> GuitarBodyEnum.JUMBO
            GuitarBodyEnum.AUDITORIUM.id -> GuitarBodyEnum.AUDITORIUM
            GuitarBodyEnum.CLASSICAL.id -> GuitarBodyEnum.CLASSICAL
            GuitarBodyEnum.TRAVEL.id -> GuitarBodyEnum.TRAVEL
            else -> GuitarBodyEnum.OTHER
        }
        guitarBody.set(guitarBodyEnum!!.get())
    }

    fun updateNumberOfFrets(selectedIndex:  Int){
        scaleNumberOfFretsEnum = when(selectedIndex.toString()){
            GuitarNumberOfFretsEnum.NINETEEN.id -> GuitarNumberOfFretsEnum.NINETEEN
            GuitarNumberOfFretsEnum.TWENTY.id -> GuitarNumberOfFretsEnum.TWENTY
            GuitarNumberOfFretsEnum.TWENTYONE.id -> GuitarNumberOfFretsEnum.TWENTYONE
            GuitarNumberOfFretsEnum.TWENTYFOUR.id -> GuitarNumberOfFretsEnum.TWENTYFOUR
            GuitarNumberOfFretsEnum.TWENTYSEVEN.id -> GuitarNumberOfFretsEnum.TWENTYSEVEN
            else -> GuitarNumberOfFretsEnum.OTHER
        }
        scaleNumberOfFrets.set(scaleNumberOfFretsEnum!!.get())
    }

    fun onManufacturerClick() = getNavigator()?.createSelectManufacturerDialog()
    fun onGuitarBodyClick() = getNavigator()?.createSelectGuitarBodyDialog()
    fun onGuitarNeckClick() = getNavigator()?.createSelectGuitarNeckDialog()
    fun onManufacturerCountryClick() = getNavigator()?.createSelectManufacturerCountryDialog()
    fun onNumberOfFretsClick() = getNavigator()?.createSelectNumberOfFretsDialog()
    fun onTypeOfProductClick() = getNavigator()?.createSelectTypeOfProductDialog()
    fun openFileChooser() = getNavigator()?.openFileChooser()
    fun uploadFile() = getNavigator()?.uploadFile()
    fun onProductIsNewSwitchClick() = getNavigator()?.onProductIsNewSwitchClick()

//    fun displaySetting() {
//        navigateToGuitarCharacteristics.value = true
//    }
//
//    fun displaySettingComplete() {
//        navigateToGuitarCharacteristics.value = false
//    }
}