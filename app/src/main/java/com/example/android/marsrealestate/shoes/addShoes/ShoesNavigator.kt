package com.example.android.marsrealestate.shoes.addShoes

import androidx.databinding.ObservableField

interface ShoesNavigator {

    fun showToast(model: String)

    fun errorMassages(
            //isUserIdFilled: Boolean,
            isModelFilled: Boolean,
            isBrandFilled: Boolean,
            isManufacturerCountryFilled: Boolean,
            isCityFilled: Boolean,
            isPriceFilled: Boolean,
            isSellerFilled: Boolean,
            isProductConditionFilled: Boolean,
            isSexFilled: Boolean,
            isTitleFilled: Boolean,
            isColorFilled: Boolean
    )

    fun getWinter(): ObservableField<Boolean>
    fun getSpring(): ObservableField<Boolean>
    fun getFall(): ObservableField<Boolean>
    fun getSummer(): ObservableField<Boolean>
    fun getDemi(): ObservableField<Boolean>

    fun getProductIsNew() : ObservableField<Boolean>

    fun createSelectSizeDialog()

    fun openFileChooser()
    fun uploadFile()
    fun getPhotoId(): String
}