package com.example.android.marsrealestate.atv

class AtvBody (
        var model : String,
        val typeOfProduct: String,
        var manufacturer: String,
        var manufacturerCountry: String,
        var productIsNew: Boolean,
        val description: String,
        val url: String,
        val mileage: Int,
        val firstRegistration: String,
        val cubicCapacity: Int,
        val powerSupply: String,
        val gearBox: String
)