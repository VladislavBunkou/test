package com.example.android.marsrealestate.bicycle.addNewBicycle

enum class BicycleManufacturerType (val id: String) {
    GIANT("1"){ override fun getBrand(): String = "Giant" },
    TREK("2"){ override fun getBrand(): String = "Trek" },
    MERIDA("3"){ override fun getBrand(): String = "Merida" },
    SCOTT("4"){ override fun getBrand(): String = "Scott" },
    CUBE("5"){ override fun getBrand(): String = "Cube" },
    OTHER("6"){ override fun getBrand(): String = "Other" };

    abstract fun getBrand(): String
}