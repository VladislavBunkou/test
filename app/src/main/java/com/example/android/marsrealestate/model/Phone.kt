package com.example.android.marsrealestate.model


import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Phone(
        val id: Long,
        val model: String,
        val typeOfProduct: String,
        val manufacturer: String,
        val manufacturerCountry: String,
        val url: String,
        val operatingSystem: String,
        val screenDiagonal: String,
        val chargeCapacity: String,
        val hasStylus: Boolean,
        val has3g: Boolean,
        val has4g: Boolean,
        val has5g: Boolean,
        val phoneCondition: String,
        val seller: String

) : Parcelable

@Parcelize
 class PhoneResponse(
        val model: String,
//        val typeOfProduct: String = "",
        val manufacturer: String ,
        val manufacturerCountry: String ,
        val url: String
//        val operatingSystem: String = "",
//        val screenDiagonal: String = "",
//        val chargeCapacity: String = "",
//        val hasStylus: Boolean = false,
//        val has3g: Boolean  = false,
//        val has4g: Boolean  = false,
//        val has5g: Boolean  = false,
//        val phoneCondition: String = "",
//        val seller: String = ""

) : Parcelable


@Parcelize
class PhoneOne(
        val model: String,
        val typeOfProduct: String,
        val manufacturer: String,
        val manufacturerCountry: String,
        val url: String,
        val operatingSystem: String,
        val screenDiagonal: String,
        val chargeCapacity: String,
        val hasStylus: Boolean,
        val has3g: Boolean,
        val has4g: Boolean,
        val has5g: Boolean,
        val phoneCondition: String,
        val seller: String

) : Parcelable