package com.example.android.marsrealestate.car

import android.os.Parcelable
import androidx.annotation.Nullable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Car (
    val id: Int,
    @Nullable val category :String?,
    @Nullable val typeMessage :String?,
    val price :String,

    @Json(name = "image") val imgSrcUrl: Array<String>,

    val car :String,
    val model :String,
    val year :Int,
    val mileage :Int,
    val engineType :String,
    val engineSize :Int,
    val transmissionType :String,
    val bodyType :String,
    val color :String,
    val url: String) : Parcelable