package com.example.android.marsrealestate.shoes.addShoes

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.android.marsrealestate.di.mv.BaseViewModel
import com.example.android.marsrealestate.enums.Status
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.shoes.ShoesRepository
import com.example.android.marsrealestate.shoes.ShoesSaveInfo
import com.example.android.marsrealestate.ui.authentification.LoginFragment
import com.example.android.marsrealestate.ui.authentification.LoginFragment.Companion.TAG
import com.example.android.marsrealestate.ui.dress.d_latitude
import com.example.android.marsrealestate.ui.dress.d_longitude
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class AddShoesViewModel : BaseViewModel<ShoesNavigator>(){

    lateinit var shoesRepository: ShoesRepository

    var modelShoes=ObservableField<String>("")
    var manufacturerShoes=ObservableField<String>("")
    var countryShoes=ObservableField<String>("")
    var descriotionShoes=ObservableField<String>("")
    var cityShoes=ObservableField<String>("")
    var priceShoes=ObservableField<String>("")
    var typeOfShoesShoes=ObservableField<String>("")
    var sellerShoes=ObservableField<String>("")
    var productConditionShoes=ObservableField<String>("")
    var sexShoes=ObservableField<String>("")
    var colorShoes=ObservableField<String>("")
    var wordsToSearchShoes=ObservableField<String>("")
    var sizeShoes = ObservableField<String>("")

    private var sizeType: ShoesSizeType?=null

    val saveShoesObservavle = MutableLiveData<ShoesSaveInfo>()

    var navigateToShoesCharacteristics = MutableLiveData<Boolean>(false)

    var PICK_IMAGE_REQUEST: Int = 1

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val saveShoesObserver = Observer<ShoesSaveInfo> {
        when (it.status) {
            Status.INITIAL, Status.LOADING -> {
                setIsLoading(true)
                Log.d("AddShoesViewModel", "Status.Loading")
            }
            Status.SUCCESS -> {
                setIsLoading(false)
                Log.d("AddShoesViewModel", "Status.SUCCESS")
            }
            Status.ERROR -> {
                setIsLoading(false)
                Log.d("AddShoesViewModel", "Status.Error")
            }
            Status.ERROR_CONNECTION -> {
                setIsLoading(false)
            }
            else -> {
            }
        }
    }

    init {

        saveShoesObservavle.observeForever(saveShoesObserver)
        shoesRepository = ShoesRepository(coroutineScope)

        //dressRepository.userIdDressObservable.observeForever(Observer {userIdDress.set(it) })

        shoesRepository.modelShoesObservable.observeForever(Observer {modelShoes.set(it) })
        shoesRepository.manufacturerCountryShoesObservable.observeForever(Observer {manufacturerShoes.set(it) })
        shoesRepository.manufacturerCountryShoesObservable.observeForever(Observer {countryShoes.set(it) })
        shoesRepository.descriptionShoesObservable.observeForever(Observer {descriotionShoes.set(it) })
        shoesRepository.cityShoesObservable.observeForever(Observer {cityShoes.set(it) })
        shoesRepository.priceShoesObservable.observeForever(Observer {priceShoes.set(it) })
        shoesRepository.typeOfShoesShoesObservable.observeForever(Observer {typeOfShoesShoes.set(it) })
        shoesRepository.sellerShoesObservable.observeForever(Observer {sellerShoes.set(it) })
        shoesRepository.wordsToSearchShoesObservable.observeForever(Observer {wordsToSearchShoes.set(it) })
        shoesRepository.sexShoesObservable.observeForever(Observer {sexShoes.set(it) })
        shoesRepository.colorShoesObservable.observeForever(Observer {colorShoes.set(it) })
        shoesRepository.productConditionShoesObservable.observeForever(Observer {productConditionShoes.set(it) })
        //shoesRepository.sizeShoesObservable.observeForever(Observer {sizeShoes.set(it) })
    }

    fun receiveShoesById(id: Long, isSaveNewShoesOnServer: Boolean) {
        shoesRepository.receiveShoesById(id, isSaveNewShoesOnServer)
    }

    fun onSendButton() {

        if (allFilldsIGood()) {
            val idUserFirebase = if (FirebaseAuth.getInstance().currentUser != null) FirebaseAuth.getInstance().currentUser?.uid else "-"
            Log.d(TAG, "dm-> idUserFirebase = $idUserFirebase")

            val shoes = ShoesForSave().apply {
                userId = idUserFirebase ?: ""
                model = modelShoes.get() ?: ""
                manufacturer = manufacturerShoes.get() ?: ""
                manufacturerCountry = countryShoes.get() ?: ""
                description = descriotionShoes.get() ?: ""
                city = cityShoes.get()?:""
                price = priceShoes.get()?:""
                typeOfShoes = typeOfShoesShoes.get()?:""
                seller = sellerShoes.get()?:""
                wordsToSearch = wordsToSearchShoes.get()?:""
                color = colorShoes.get()?:""
                sex = sexShoes.get()?:""
                //size = sizeShoes.get()?:""
                productCondition = productConditionShoes.get()?:""

                url = getNavigator()?.getPhotoId() ?: ""

                winter = getNavigator()?.getWinter()?.get()?:false
                spring = getNavigator()?.getSpring()?.get()?:false
                summer = getNavigator()?.getSummer()?.get()?:false
                fall = getNavigator()?.getFall()?.get()?:false
                demi = getNavigator()?.getDemi()?.get()?:false

                productIsNew = getNavigator()?.getProductIsNew()?.get() ?: false

                latitude = d_latitude
                longitude = d_longitude

                Log.d(TAG,"AddShoesViewModel: km->Location  latitude=$latitude, lontitude=$longitude")


            }

            saveShoesWithDeferred(shoes)

             getNavigator()?.showToast(model = "Shoes ${shoes.model} is SAVED!")

            modelShoes.set("")
            manufacturerShoes.set("")
            countryShoes.set("")
            descriotionShoes.set("")
            cityShoes.set("")
            priceShoes.set("")
            typeOfShoesShoes.set("")
            sellerShoes.set("")
            wordsToSearchShoes.set("")
            colorShoes.set("")
            sexShoes.set("")
            //sizeShoes.set("")
            productConditionShoes.set("")


        } else {
            getNavigator()?.showToast("Shoes NOT SAVED !!")
        }


    }


    private fun saveShoesWithDeferred(shoes: ShoesForSave) {
        saveShoesObservavle.postValue(ShoesSaveInfo(Status.LOADING))

            coroutineScope.launch {
                var getPropertiesDeferred = RetrofitApi.retrofitApiService.saveShoes(shoes)
                try {
                    Log.d(TAG, "all good")
                    var shoesResponse = getPropertiesDeferred.await()
                    Log.d(TAG, "code ${shoesResponse.code()}")
                    if (shoesResponse.isSuccessful) {
                        Log.d(TAG, "dm-> code ${shoesResponse.code()}")
                        saveShoesObservavle.postValue(ShoesSaveInfo(Status.SUCCESS))
                    } else {
                        saveShoesObservavle.postValue(ShoesSaveInfo(Status.ERROR))
                    }
                } catch (e: Exception) {
                    Log.d(TAG, "Error :: ${e.message}")
                    saveShoesObservavle.postValue(ShoesSaveInfo(Status.ERROR_CONNECTION))
                }
            }
        }


    fun allFilldsIGood(): Boolean {
        var isModelFilled = false
        var isBrandFilled = false
        var isManufacturerCountryFilled = false
        var isCityFilled = false
        var isPriceFilled = false
        var isSellerFilled = false
        var isProductConditionFilled = false
        var isSexFilled = false
        var isTitleFilled = false
        var isColorFilled = false

        if (!modelShoes.get().isNullOrEmpty()) isModelFilled = true
        if (!manufacturerShoes.get().isNullOrEmpty())isBrandFilled = true
        if (!countryShoes.get().isNullOrEmpty()) isManufacturerCountryFilled = true
        if (!productConditionShoes.get().isNullOrEmpty()) isProductConditionFilled = true
        if (!cityShoes.get().isNullOrEmpty()) isCityFilled = true
        if (!priceShoes.get().isNullOrEmpty())isPriceFilled = true
        if (!sellerShoes.get().isNullOrEmpty()) isSellerFilled = true

        if (!sexShoes.get().isNullOrEmpty()) isSexFilled = true
        if (!wordsToSearchShoes.get().isNullOrEmpty())isTitleFilled = true
        if (!colorShoes.get().isNullOrEmpty()) isColorFilled = true

        if (!isModelFilled || !isBrandFilled || !isManufacturerCountryFilled || !isCityFilled || !isPriceFilled
                || !isSellerFilled || !isSexFilled || !isTitleFilled || !isColorFilled || !isProductConditionFilled) {
            getNavigator()?.errorMassages(isModelFilled, isBrandFilled, isManufacturerCountryFilled,
            isCityFilled,isPriceFilled,isSellerFilled,isProductConditionFilled,isSexFilled,isTitleFilled,isColorFilled)
            return false
        } else {
            return true
        }
    }

    fun updateSize(selectedIndex:  Int){
        sizeType = when(selectedIndex.toString()){
            ShoesSizeType.ONE.id -> ShoesSizeType.ONE
            ShoesSizeType.TWO.id -> ShoesSizeType.TWO
            ShoesSizeType.THREE.id -> ShoesSizeType.THREE
            ShoesSizeType.FOUR.id -> ShoesSizeType.FOUR
            ShoesSizeType.FIVE.id -> ShoesSizeType.FIVE
            ShoesSizeType.SIX.id -> ShoesSizeType.SIX
            ShoesSizeType.SEVEN.id -> ShoesSizeType.SEVEN
            ShoesSizeType.EIGHT.id -> ShoesSizeType.EIGHT
            else -> ShoesSizeType.OTHER
        }
        sizeShoes.set(sizeType?.getSizeType()?:"")
    }


    fun onShoesSizeClick() {
        getNavigator()?.createSelectSizeDialog()
    }


    fun openFileChooser() = getNavigator()?.openFileChooser()
    fun uploadFile() = getNavigator()?.uploadFile()


    fun displayCharacteristic() {
        navigateToShoesCharacteristics.value = true
    }

    fun displayCharacteristicsComplete() {
        navigateToShoesCharacteristics.value = false
    }



}