package com.example.android.marsrealestate.computer.addcomputer

import android.app.Application
import androidx.lifecycle.AndroidViewModel

/**
 * Класс ComputerInfoViewModel
 * @author Ilya Osipov (mailto:il.osipov.gm@gmail.com)
 * @since 28.04.2020
 * @version $Id$
 */

class ComputerInfoViewModel(app: Application) : AndroidViewModel(app) {

}