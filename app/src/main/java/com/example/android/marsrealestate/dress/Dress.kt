package com.example.android.marsrealestate.dress

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Dress (
        val id: Long,
        val userId: String,
        val model: String,
        val typeOfProduct: String,
        val manufacturer: String ,
        val manufacturerCountry: String,
        val productIsNew: Boolean,
        val description: String,
        val city: String,

        val latitude: Double,
        val longitude: Double,

        val price: String,
        val category: String,
        val seller: String,
        val productCondition: String,

        val url: String,
        val wordsToSearch: String,
        val innerId: String,
        val article: String,
        val sleeve: String,
        val silhouette: String,
        val fabric: String,

        val size: String,
        val height: String,
        val typeOfCollection: String,

        val sex: String,
        val color: String


        //val product: String

): Parcelable

@Parcelize
class DressResponse(
        val id: Long,
        val innerId: String,
        val userId: String,
        val model: String,
        val article: String,
        val sleeve: String,
        val fabric: String,
        val silhouette: String,
        val size: String,
        val height: String,
        val typeOfProduct: String,
        val typeOfCollection: String,
        val manufacturer: String ,
        val manufacturerCountry: String,
        var productIsNew: Boolean,
        var description: String,
        val url: String

) : Parcelable


@Parcelize
class DressOne(
        //val id: Long,
        val innerId: String,
        val userId: String,
        val model: String,
        val article: String,
        val sleeve: String,
        val fabric: String,
        val silhouette: String,
        val size: String,
        val height: String,
        val typeOfProduct: String,
        val typeOfCollection: String,
        val manufacturer: String ,
        val manufacturerCountry: String,
        var productIsNew: Boolean,
        var description: String,
        val url: String

) : Parcelable