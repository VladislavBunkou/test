package com.example.android.marsrealestate.computer

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.R
import com.example.android.marsrealestate.computer.model.Computer

/**
 * BindingComputer
 * @author Ilya Osipov (mailto:il.osipov.gm@gmail.com)
 * @since 27.04.2020
 * @version $Id$
 */

@BindingAdapter("listDataComputers")
fun bindRecyclerViewComputers(recyclerView: RecyclerView, data: List<Computer>?) {
    val adapter = recyclerView.adapter as ComputerRecyclerAdapter
    adapter.submitList(data)
}

@BindingAdapter("computerApiStatus")
fun bindStatusComputer(statusImageView: ImageView, status: ComputerApiStatus?) {
    when (status) {
        ComputerApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        ComputerApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }

        ComputerApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}