package com.example.android.marsrealestate.computer.addcomputer

import androidx.fragment.app.Fragment
import com.akordirect.vmccnc.databinding.FragmentComputerNewBinding

/**
 * Класс ComputerNewFragment
 * @author Ilya Osipov (mailto:il.osipov.gm@gmail.com)
 * @since 28.04.2020
 * @version $Id$
 */

class ComputerNewFragment : Fragment(), ComputerNavigator {

    lateinit var binding : FragmentComputerNewBinding

    override fun createSelectManufacturerDialog() {
    }
}