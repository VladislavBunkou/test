package com.example.android.marsrealestate.shoes.addShoes

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ShoesCharacteristicViewModel : ViewModel(){
    val winterSelected  = MutableLiveData<Boolean>(false)
    val springSelected = MutableLiveData<Boolean>(false)
    val fallSelected = MutableLiveData<Boolean>(false)
    val summerSelected = MutableLiveData<Boolean>(false)
    val demiSelected  = MutableLiveData<Boolean>(false)

    val productIsNewSelected = MutableLiveData<Boolean>(false)
}