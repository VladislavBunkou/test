package com.example.android.marsrealestate.di.mv.authentification

import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.example.android.marsrealestate.authentification.FirebaseUserLiveData

class LoginViewModel : ViewModel() {
    enum class AuthenticationState { AUTHENTICATED, UNAUTHENTICATED, INVALID_AUTHENTICATION }

    val authenticationState = FirebaseUserLiveData().map {firebaseUser->
        if (firebaseUser != null) AuthenticationState.AUTHENTICATED
        else AuthenticationState.UNAUTHENTICATED
    } //  other classes can now query for whether a user is logged in or not through the LoginViewModel
}
