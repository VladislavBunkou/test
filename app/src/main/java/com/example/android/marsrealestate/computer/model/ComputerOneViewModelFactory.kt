package com.example.android.marsrealestate.computer.model

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

/**
 * Класс ComputerOneViewModelFactory
 * @author Ilya Osipov (mailto:il.osipov.gm@gmail.com)
 * @since 27.04.2020
 * @version $Id$
 */

class ComputerOneViewModelFactory(private val computer: Computer,
                                  private val application: Application) :
        ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ComputerOneViewModel::class.java)) {
            return ComputerOneViewModel(computer, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}