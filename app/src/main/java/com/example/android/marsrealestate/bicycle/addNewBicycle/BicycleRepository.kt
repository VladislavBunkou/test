package com.example.android.marsrealestate.bicycle.addNewBicycle

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.bicycle.Bicycle
import com.example.android.marsrealestate.bicycle.BicycleGetInfo
import com.example.android.marsrealestate.bicycle.BicycleSaveInfo
import com.example.android.marsrealestate.enums.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class BicycleRepository(val coroutineScope: CoroutineScope){

    val bicycle = MutableLiveData<List<Bicycle>>()
    val saveBicycleObservable = MutableLiveData<BicycleSaveInfo>()
    val receiveBicycleObservable = MutableLiveData<BicycleGetInfo>()


    val wordsToSearchBicycleObservable = MutableLiveData<String>()
    val manufacturerBicycleObservable = MutableLiveData<String>()
    val modelBicycleObservable = MutableLiveData<String>()
    val cityBicycleObservable = MutableLiveData<String>()
    val speciesBicycleObservable = MutableLiveData<String>()
   // val typeBicycleObservable = MutableLiveData<String>()
    val priceBicycleObservable = MutableLiveData<String>()
    val descriptionBicycleObservable = MutableLiveData<String>()
    val sellerBicycleObservable = MutableLiveData<String>()
    val productConditionBicycleObservable = MutableLiveData<String>()

    fun getBicycleList(/*str: String = ""*/){
        receiveBicycleObservable.postValue(BicycleGetInfo(Status.LOADING))
        coroutineScope.launch {
            var getPropertiesDeferred = /*if (str.isEmpty())*/ RetrofitApi.retrofitApiService.getBicycleList()
            /*else RetrofitApi.retrofitApiService.getBicycleList(str)*/
            Log.d("BicycleViewModel", "Status.LOADING")
            try {
                var listResult = getPropertiesDeferred.await()

                Log.d("BicycleViewModel", "$listResult")
                bicycle.value = listResult
                Log.d("BicycleViewModel", "$bicycle.value")
                receiveBicycleObservable.postValue(BicycleGetInfo(Status.SUCCESS))
            } catch (e: Exception) {
                bicycle.value = ArrayList()
                receiveBicycleObservable.postValue(BicycleGetInfo(Status.ERROR_CONNECTION))
            }

        }}


        fun receiveBicycleById(id: Long, isSaveNewBicycleOnServer: Boolean) {
            receiveBicycleObservable.postValue(BicycleGetInfo(Status.LOADING)) // change
            if (isSaveNewBicycleOnServer) {
                receiveBicycleObservable.postValue(BicycleGetInfo(Status.SUCCESS))
            } else
                coroutineScope.launch {
                    var getPropertiesDeferred = RetrofitApi.retrofitApiService.getBicycleById(id)
                    try {
                        val bicycleOne = getPropertiesDeferred.await()
                        wordsToSearchBicycleObservable.postValue(bicycleOne.wordsToSearch)
                        modelBicycleObservable.postValue(bicycleOne.model)
                        manufacturerBicycleObservable.postValue(bicycleOne.manufacturer)
                        cityBicycleObservable.postValue(bicycleOne.city)
                        speciesBicycleObservable.postValue(bicycleOne.species)
                        priceBicycleObservable.postValue(bicycleOne.price)
                        descriptionBicycleObservable.postValue(bicycleOne.description)
                        sellerBicycleObservable.postValue(bicycleOne.seller)
                        productConditionBicycleObservable.postValue(bicycleOne.productCondition)

                        receiveBicycleObservable.postValue(BicycleGetInfo(Status.SUCCESS))
                    } catch (e: Exception) {
                        receiveBicycleObservable.postValue(BicycleGetInfo(Status.ERROR_CONNECTION))
                    }
                }
        }
    }
