package com.example.android.marsrealestate.di.binding

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.R
import com.example.android.marsrealestate.di.mv.phone.PhoneApiStatus
import com.example.android.marsrealestate.model.Phone
import com.example.android.marsrealestate.repository.phone.PhoneItemListener
import com.example.android.marsrealestate.ui.adapters.phone.PhoneRecyclerAdapter


@BindingAdapter("listDataPhones", "listenerPhone")
fun bindRecyclerViewPhones(recyclerView: RecyclerView, listDataPhones: List<Phone>, listenerPhone: PhoneItemListener) {
    val adapter = recyclerView.adapter as PhoneRecyclerAdapter
    adapter.addItems(listDataPhones)
    adapter.addListener(listenerPhone)
}



@BindingAdapter("phoneApiStatus")
fun bindStatusPhoto(statusImageView: ImageView, status: PhoneApiStatus?) {
    when (status) {
        PhoneApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        PhoneApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        PhoneApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}