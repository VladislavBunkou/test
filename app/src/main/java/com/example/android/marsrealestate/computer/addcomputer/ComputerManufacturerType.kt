package com.example.android.marsrealestate.computer.addcomputer

enum class ComputerManufacturerType(val id: String) {
    ;

    abstract fun getBrand() : String
}