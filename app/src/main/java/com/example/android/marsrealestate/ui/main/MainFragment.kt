package com.example.android.marsrealestate.ui.main

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentMainBinding
import com.example.android.marsrealestate.di.mv.main.MainViewModel
import com.example.android.marsrealestate.ui.adapters.mycabinet.MyCabinetAdapter
import com.example.android.marsrealestate.di.mv.nav.NavViewModel
import com.example.android.marsrealestate.ui.BaseFragment
import com.google.android.material.bottomsheet.BottomSheetDialog

class MainFragment : BaseFragment(), CategoriesListener {

    private val viewModel by viewModels<MainViewModel>()
    private lateinit var navViewModel: NavViewModel // Where we save data about phone

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentMainBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.photosGrid.adapter = MyCabinetAdapter(MyCabinetAdapter.OnClickListener {
            this.findNavController().navigate(MainFragmentDirections.actionShowDetail(it))
        })

        binding.categoryAdapter.adapter = CategoriesAdapter(this)

        activity?.let {
            navViewModel = ViewModelProviders.of(it).get(NavViewModel::class.java)
        }

        setHasOptionsMenu(true)
        return binding.root
    }


    override fun onClickCategory(categoryName: String) {
        val nav: NavDirections = when (categoryName) {
            "Auto&\nBoats" -> MainFragmentDirections.actionOverviewFragmentToNavCar()
            "Electric" -> MainFragmentDirections.actionOverviewFragmentToNavElectronikFragment()
            "Houses" -> { showToast("Flat TEMP")
                MainFragmentDirections.actionOverviewFragmentToNavCar()}
            "Dress&\nShoes" -> MainFragmentDirections.actionOverviewFragmentToNavClothesFragment()
            "Music\ninstruments" -> MainFragmentDirections.actionMainFragmentToNavMusicFragment()
            else -> MainFragmentDirections.actionOverviewFragmentToNavCar()
        }
        this.findNavController().navigate(nav)
    }

//    Auto&Boats", "Electric", "Dress&Shoes", "Music instruments

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val bottomView: View =
                layoutInflater.inflate(R.layout.sheet_main_filter, null)

        when (item.itemId) {
            R.id.filter_bottom -> {
                Log.d("MainFragment", "dm:: filter_bottom is clicked")
                activity?.let {
                    val dialog = BottomSheetDialog(it)
                    dialog.setContentView(bottomView)
                    dialog.show()
                }
            }
            R.id.set_location ->{
                showToast("set_location is clicked !")
            }
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)
        val menuItem = menu.findItem(R.id.search_prod);
        val searchView = menuItem.actionView as SearchView
        searchView.queryHint = "Search..."
//        searchView.setBackgroundColor(Color.parseColor("#EFEFF4"))
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                Log.d("OverviewFragment", "dm:: onQueryTextSubmit")
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                Log.d("OverviewFragment", "dm:: onQueryTextChange = $newText")
                viewModel.getListProducts(newText)
                return true
            }

        })
        super.onCreateOptionsMenu(menu, inflater)
    }

}


interface CategoriesListener {
    fun onClickCategory(string: String)
}