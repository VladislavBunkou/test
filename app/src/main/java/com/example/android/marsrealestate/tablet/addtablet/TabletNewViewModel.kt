package com.example.android.marsrealestate.tablet.addtablet

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.di.mv.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class TabletNewViewModel : BaseViewModel<TabletNavigator>() {

    private val TAG = "TabletNewViewModel"

    var modelTablet = ObservableField<String>("") // for field
    var countryTablet = ObservableField<String>("") // for field
    var manufacturer = ObservableField<String>("") // for field
    var screenDiagonal = ObservableField<String>("") // for field
    var screenResolution = ObservableField<String>("") // for field
    var ram = ObservableField<String>("") // for field
    var cameraResolution = ObservableField<String>("") // for field
    var brandTablet = ObservableField<String>("") // for field

    var navigateToTabletCharacteristics = MutableLiveData<Boolean>(false)

    private var manufacturerType: TabletManufacturerType? = null

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)




init {
    getMarsRealEstateProperties(46)// TEmp
}


    private fun getMarsRealEstateProperties(id: Long) {
        coroutineScope.launch {

            //            var getPropertiesDeferred = MarsApi.retrofitService.getProperties(filter.value)
            var getPropertiesDeferred = RetrofitApi.retrofitApiService.getTablet(id)
            try {
//                _status.value = MarsApiStatus.LOADING
                // this will run on a thread managed by Retrofit
                val tabletOne = getPropertiesDeferred.await()

                Log.d("DetailViewModel", "dm-> try tablet = ${tabletOne.toString()}")
                modelTablet.set(tabletOne.model)
                brandTablet.set(tabletOne.manufacturer)
//                _status.value = MarsApiStatus.DONE
//                listResult.id = id
//                _selectedProperty.value = tabletOne
            } catch (e: Exception) {
//                _status.value = MarsApiStatus.ERROR
//                _selectedProperty.value = Prod()
                Log.d("DetailViewModel", "dm-> catch ")
            }
        }
    }








    fun onSendButton() {
        //     val tr = allFilldsIGood()
        if (true) {
            val model = modelTablet.get() ?: ""
            val country = countryTablet.get() ?: ""
            val manufacturer = manufacturer.get() ?: ""
            val productionYear = getNavigator()?.getYear()?.get() ?: ""
            val screenDiagonal = screenDiagonal.get() ?: ""
            val screenResolution = screenResolution.get() ?: ""
            val ram = ram.get() ?: ""
            val cameraResolution = cameraResolution.get() ?: ""
            val seller =  getNavigator()?.getSeller()?.get() ?: ""
            val sim = getNavigator()?.getSIM()?.get() ?: false
            val isNew = getNavigator()?.getIsNew()?.get() ?: false
            val brandTablet = brandTablet.get() ?: ""

            Log.d(TAG, "dm-> model =$model, country = $country, sim = $sim")

            getNavigator()?.showToast(model = model, country = country)  // for easier understanding this code can be deleted

            //   saveTablet(model, country, brandTablet, productionYear, screenDiagonal, screenResolution, ram, sim, cameraResolution, seller, isNew)
            saveTablet(model, country, brandTablet, productionYear, "screenDiagonal", "screenResolution", "ram", sim, "cameraResolution", seller, isNew)
        }else{
            getNavigator()?.showToast(model = "Tablet not Save !!", country = "")
        }
    }

    private fun saveTablet(model: String, manufacturerCountry: String, manufacturer: String, productionYear: String, screenDiagonal: String, screenResolution: String, ram: String, sim: Boolean, cameraResolution: String, seller: String, isNew: Boolean) {

        Log.d("LatheViewModel dm->", "- getLatheProperties")
//            _response.value = "Set response Lathe"

        coroutineScope.launch {
            //                var getPropertiesDeferred = LatheApi.retrofitService.getProperties( )
            var getPropertiesDeferred = RetrofitApi.retrofitApiService.saveTablet(TabletBody(model, manufacturer, manufacturerCountry, productionYear, screenDiagonal, screenResolution, ram, sim, cameraResolution, seller, isNew))
            try {
//                    _status.value = LatheApiStatus.LOADING
                Log.d("LatheViewModel", "dm-> all good")
                var listResult = getPropertiesDeferred.await()
//                    _status.value = LatheApiStatus.DONE
//                _properties.value = listResult
            } catch (e: Exception) {
//                _status.value = LatheApiStatus.ERROR
                Log.d("LatheViewModel", "dm-> I think and shure you do not have and internet connection")
//                _properties.value = ArrayList()
            }
        }


    }

    fun allFilldsIGood(): Boolean {
        var isModelFilled = false
        var isCountryFilled = false
        var isManufacturerFilled = false
        var isProductionYearFilled = false
        var isScreenDiagonalFilled = false
        var isScreenResolutionFilled = false
        var isRamFilled = false
        var isCameraResolutionFilled = false
        var isSellerFilled = false

        if (!modelTablet.get().isNullOrEmpty()) isModelFilled = true
        if (!countryTablet.get().isNullOrEmpty()) isCountryFilled = true
        if (!manufacturer.get().isNullOrEmpty()) isManufacturerFilled = true
        //    if (!productionYear.get().isNullOrEmpty()) isProductionYearFilled = true
        if (!screenDiagonal.get().isNullOrEmpty()) isScreenDiagonalFilled = true
        if (!screenResolution.get().isNullOrEmpty()) isScreenResolutionFilled = true
        if (!ram.get().isNullOrEmpty()) isRamFilled = true
        if (!cameraResolution.get().isNullOrEmpty()) isCameraResolutionFilled = true
        //   if (!seller.get().isNullOrEmpty()) isSellerFilled = true

        if (!isModelFilled || !isCountryFilled || !isManufacturerFilled || !isProductionYearFilled || !isScreenDiagonalFilled || !isScreenResolutionFilled || !isRamFilled || !isCameraResolutionFilled || !isSellerFilled) {
            getNavigator()?.errorMessages(isModelFilled, isCountryFilled, isManufacturerFilled, isProductionYearFilled, isScreenDiagonalFilled, isScreenResolutionFilled, isRamFilled, isCameraResolutionFilled, isSellerFilled)
            return false
        } else {
            return true
        }

    }

    fun updateManufacturer(selectedIndex:  Int){
        manufacturerType = when(selectedIndex.toString()){
            TabletManufacturerType.APPLE.id -> TabletManufacturerType.APPLE
            TabletManufacturerType.SAMSUNG.id -> TabletManufacturerType.SAMSUNG
            TabletManufacturerType.LENOVO.id -> TabletManufacturerType.LENOVO
            else -> TabletManufacturerType.OTHER
        }
//        brandObservable.value = manufacturerType?.getBrand() ?: ""
        brandTablet.set(manufacturerType?.getBrand() ?: "")
    }

    fun onTabletManufacturerClick() {
        getNavigator()?.createSelectManufacturerDialog()
    }

    fun onCharacteristicsClick() {
        displaySetting()
//        getNavigator()?.onTabletCharacteristicsClick()
    }


    fun displaySetting() {
        navigateToTabletCharacteristics.value = true
    }

    fun displaySettingComplete() {
        navigateToTabletCharacteristics.value = false
    }


}