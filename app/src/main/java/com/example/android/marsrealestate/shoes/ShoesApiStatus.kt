package com.example.android.marsrealestate.shoes

enum class ShoesApiStatus{
    LOADING, ERROR, DONE
}