
package com.example.android.marsrealestate.car


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job


enum class MarsApiStatus { LOADING, ERROR, DONE }

class AvtoViewModel : ViewModel() {

    // The internal MutableLiveData that stores the status of the most recent request
    private val _status = MutableLiveData<MarsApiStatus>()

    // The external immutable LiveData for the request status
    val status: LiveData<MarsApiStatus>
        get() = _status

    // Internally, we use a MutableLiveData, because we will be updating the List of MarsProperty
    // with new values
    private val _properties = MutableLiveData<List<Car>>()

    // The external LiveData interface to the property is immutable, so only this class can modify
    val properties: LiveData<List<Car>>
        get() = _properties

    // LiveData to handle navigation to the selected property
    private val _navigateToSelectedProperty = MutableLiveData<Car>()
    val navigateToSelectedProperty: LiveData<Car>
        get() = _navigateToSelectedProperty

    // Create a Coroutine scope using a job to be able to cancel when needed
    private var viewModelJob = Job()

    // the Coroutine runs using the Main (UI) dispatcher
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)



    init {
        getMarsRealEstateProperties( )
    }


    private fun getMarsRealEstateProperties( ) {


        _properties.value = getAvtos()
        _status.value = MarsApiStatus.DONE
//        coroutineScope.launch {
//            // Get the Deferred object for our Retrofit request
//            var getPropertiesDeferred =  RetrofitApi.retrofitApiService.getCars()
//            try {
//                _status.value = MarsApiStatus.LOADING
//                // this will run on a thread managed by Retrofit
//                val listResult = getPropertiesDeferred.await()
//                _status.value = MarsApiStatus.DONE
//                _properties.value = listResult
//            } catch (e: Exception) {
//                _status.value = MarsApiStatus.ERROR
//                _properties.value = ArrayList()
//            }
//        }
    }


    fun getAvtos(): List<Car> {
        // The Coroutine Call Adapter allows us to return a Deferred, a Job with a result
        val imgs: Array<String> = arrayOf("1.jpg","2.jpg","3.jpg","4.jpg","5.jpg")

        val m1 = Car(1,"auto","sell","1000",imgs,"audi","a6",2006,150000,"diesel",2,"manual","wagon","black", "https://www.cstatic-images.com/car-pictures/main/usc80tov111a021001.png")
        val m2 = Car(2,"auto","sell","1000",imgs,"audi","a6",2006,150000,"diesel",2,"manual","wagon","black", "https://www.cstatic-images.com/car-pictures/main/usc90kiv011d021001.png")
        val listAvtos: MutableList<Car> = mutableListOf(m1,m2)
        listAvtos.add(Car(3,"auto","sell","1000",imgs,"audi","a6",2006,150000,"diesel",2,"manual","wagon","black", "https://www.cstatic-images.com/car-pictures/main/usd00fov441a021001.png"))
        listAvtos.add(Car(4,"auto","sell","1000",imgs,"audi","a6",2006,150000,"diesel",2,"manual","wagon","black", "https://www.cstatic-images.com/car-pictures/main/usc90mbv063a021001.png"))

        return  listAvtos

    }

    /**
     * When the property is clicked, set the [_navigateToSelectedProperty] [MutableLiveData]
     * @param marsProperty The [MarsProperty] that was clicked on.
     */
    fun displayPropertyDetails(message: Car) {
        _navigateToSelectedProperty.value = message
    }

    /**
     * After the navigation has taken place, make sure navigateToSelectedProperty is set to null
     */
    fun displayPropertyDetailsComplete() {
        _navigateToSelectedProperty.value = null
    }

}
