package com.example.android.marsrealestate.atv.list

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.atv.Atv
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

enum class AtvApiStatus {
    LOADING, ERROR, DONE
}

class AtvViewModel : ViewModel() {
    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val _atv = MutableLiveData<List<Atv>>()
    val atv: LiveData<List<Atv>>
        get() = _atv

    private val _status = MutableLiveData<AtvApiStatus>()
    val status: LiveData<AtvApiStatus>
        get() = _status

    private val _navigateToSelectedAtv = MutableLiveData<Atv>()
    val navigateToSelectedAtv: LiveData<Atv> get() = _navigateToSelectedAtv

    init {
        getAtvProperties()
    }

    private fun getAtvProperties() {
        coroutineScope.launch {
            val getPropertiesDeferred = RetrofitApi.retrofitApiService.getAtvList()
            try {
                _status.value = AtvApiStatus.LOADING
                val listResult = getPropertiesDeferred.await()
                _status.value = AtvApiStatus.DONE
                _atv.value = listResult
            } catch (e: Exception) {
                _status.value = AtvApiStatus.ERROR
                Log.d("Error dm - > ", "Internet connection not exist or Server is shot down")
                _atv.value = ArrayList()
            }
        }
    }

    fun displayOneAtv(atv: Atv) {
        _navigateToSelectedAtv.value = atv
    }

    fun displayOneAtvComplete() {
        _navigateToSelectedAtv.value = null
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}