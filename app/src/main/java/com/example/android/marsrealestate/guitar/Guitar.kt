package com.example.android.marsrealestate.guitar

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Guitar(
        var model: String,
        var typeOfProduct: String,
        var manufacturer: String,
        var manufacturerCountry: String,
        var url: String,
        var guitarNeck: String,
        var guitarBody: String,
        var guitarScale: String,
        var scaleNumberOfFrets: String,
        var guitarColor: String,
        var productIsNew: Boolean,
        var description: String,
        var userId: String
) : Parcelable