
package com.example.android.marsrealestate.car.detailview

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

import com.example.android.marsrealestate.car.Car


class DetailCarViewModel(message: Car, app: Application) : AndroidViewModel(app) {

    // The internal MutableLiveData for the selected property
    private val _selectedProperty = MutableLiveData<Car>()

    // The external LiveData for the SelectedProperty
    val selectedProperty: LiveData<Car>
        get() = _selectedProperty

    // Initialize the _selectedProperty MutableLiveData
    init {
        _selectedProperty.value = message
    }

}

