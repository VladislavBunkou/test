package com.example.android.marsrealestate.trailer

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.R
import com.example.android.marsrealestate.di.mv.trailer.TrailerApiStatus
import com.example.android.marsrealestate.repository.trailer.TrailerItemListener
import com.example.android.marsrealestate.trailer.model.Trailer
import com.example.android.marsrealestate.ui.adapters.TrailerRecyclerAdapter


@BindingAdapter("trailerApiStatus")
fun bindStatusTrailer(statusImageView: ImageView, status: TrailerApiStatus?){
    when(status){
        TrailerApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        TrailerApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        TrailerApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}

//Binding Trail er recyclerAdapter
@BindingAdapter("listDataTrailer", "listenerTrailers")
fun bindRecyclerViewTrailers(recyclerView: RecyclerView, listDataTrailers: List<Trailer>, listenerTrailer: TrailerItemListener) {
    val adapter = recyclerView.adapter as TrailerRecyclerAdapter
    adapter.addItems(listDataTrailers)
    adapter.addListener(listenerTrailer)
}
