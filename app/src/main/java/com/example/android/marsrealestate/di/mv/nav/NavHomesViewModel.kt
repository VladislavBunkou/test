package com.example.android.marsrealestate.di.mv.nav

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NavHomesViewModel : ViewModel() {

}

interface NavHomesListener {
    fun clickFlat()
}