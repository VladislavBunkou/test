package com.example.android.marsrealestate.guitar.guitar_add.guitar_enum

enum class GuitarBodyEnum (val id: String) {
    OTHER("0"){ override fun get(): String = "Other" },
    DREADNOUGHT("1"){ override fun get(): String = "Dreadnought" },
    PARLOUR("2"){ override fun get(): String = "Parlour" },
    JUMBO("3"){ override fun get(): String = "Jumbo" },
    AUDITORIUM("4"){ override fun get(): String = "Auditorium" },
    CLASSICAL("5"){ override fun get(): String = "Classical" },
    TRAVEL("6"){ override fun get(): String = "Travel" };

    abstract fun get(): String
}