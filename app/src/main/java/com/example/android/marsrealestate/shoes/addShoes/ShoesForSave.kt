package com.example.android.marsrealestate.shoes.addShoes


class ShoesForSave(
        //val id: Long,
        var userId: String="",
        var model: String="",
        var typeOfProduct: String="shoes",
        var manufacturer: String="",
        var manufacturerCountry: String="",
        var productIsNew: Boolean=false,
        var description: String="",
        var city: String="",
        var price: String="",
        var typeOfShoes: String="",
        var seller: String="",
        var productCondition: String="",
        var url: String="",
        var sex: String="",
        var size: String="",
        var color: String="",
        var wordsToSearch: String="",

        var category:String="",

        var latitude:Double=-1.0,
        var longitude:Double=-1.0,

        var winter: Boolean = false,
        var spring: Boolean = false,
        var summer: Boolean = false,
        var fall: Boolean = false,
        var demi: Boolean = false

)