package com.example.android.marsrealestate.di.mv.nav

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class NavViewModel ( app: Application) : AndroidViewModel(app) {
    var id = MutableLiveData<Long>(0)
    var isCreateNew = MutableLiveData<Boolean>(true)
}