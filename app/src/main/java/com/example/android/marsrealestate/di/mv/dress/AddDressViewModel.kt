package com.example.android.marsrealestate.di.mv.dress

import android.util.Log
import androidx.constraintlayout.widget.Constraints.TAG
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.di.mv.BaseViewModel
import com.example.android.marsrealestate.dress.DressRepository
import com.example.android.marsrealestate.dress.addDress.*
import com.example.android.marsrealestate.enums.Status
import com.example.android.marsrealestate.itemInCabinet
import com.example.android.marsrealestate.ui.adapters.dress.DressNavigator
import com.example.android.marsrealestate.ui.authentification.LoginFragment
import com.example.android.marsrealestate.ui.dress.*
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddDressViewModel: BaseViewModel<DressNavigator>() {

    val fragment: AddDressFragment = AddDressFragment()
    var dressRepository: DressRepository

    var latitudeDress = d_latitude
    var longtitudeDress = d_longitude

    var wordsToSearchDress = ObservableField<String>("")
    var manufacturerDress = ObservableField<String>("")
    var modelDress = ObservableField<String>("")
    var sexDress = ObservableField<String>("")
    var sleeveDress = ObservableField<String>("")
    var fabricDress = ObservableField<String>("")
    var silhouetteDress = ObservableField<String>("")
    var sizeDress = ObservableField<String>("")
    var cityDress = ObservableField<String>("")
    var colorDress = ObservableField<String>("")
    var sellerDress = ObservableField<String>("")
    var productConditionDress = ObservableField<String>("")
    var priceDress = ObservableField<String>("")


    var descriptionDress = ObservableField<String>("")

    var dressSaveID = ObservableField<Long>(0)

    val saveDressObservable = MutableLiveData<DressSaveInfo>()

    var navigateToDressCharacteristics = MutableLiveData<Boolean>(false)

    private var sizeType: DressSizeType?=null
    private var sleeveType: DressSleeveType?=null
    private var silhouetteType: DressSilhouetteType?=null

    var PICK_IMAGE_REQUEST: Int = 1

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val saveDressObserver = Observer<DressSaveInfo> {
        when (it.status) {
            Status.INITIAL, Status.LOADING -> {
                setIsLoading(true)
                Log.d("AddDressViewModel", "Status.Loading")
            }
            Status.SUCCESS -> {
                setIsLoading(false)
                Log.d("AddDressViewModel", "Status.SUCCESS")
            }
            Status.ERROR -> {
                setIsLoading(false)
                Log.d("AddDressViewModel", "Status.Error")
            }
            Status.ERROR_CONNECTION -> {
                setIsLoading(false)
            }
            else -> {
            }
        }
    }


    init {
        saveDressObservable.observeForever(saveDressObserver)
        dressRepository = DressRepository(coroutineScope)
/*

       */

        if (itemInCabinet){
            dressRepository.dressID.observeForever(Observer { dressSaveID.set(it) })

            dressRepository.wordsToSearchDressObservable.observeForever(Observer {wordsToSearchDress.set(it) })
            dressRepository.manufacturerDressObservable.observeForever(Observer {manufacturerDress.set(it) })
            dressRepository.modelDressObservable.observeForever(Observer {modelDress.set(it) })
            dressRepository.silhouetteDressObservable.observeForever(Observer {silhouetteDress.set(it) })
            dressRepository.sexDressObservable.observeForever(Observer {sexDress.set(it) })
            dressRepository.fabricDressObservable.observeForever(Observer {fabricDress.set(it) })
            dressRepository.sleeveDressObservable.observeForever(Observer {sleeveDress.set(it) })
            dressRepository.cityDressObservable.observeForever(Observer {cityDress.set(it) })
            dressRepository.sizeDressObservable.observeForever(Observer {sizeDress.set(it) })
            dressRepository.colorDressObservable.observeForever(Observer {colorDress.set(it) })
            dressRepository.sellerDressObservable.observeForever(Observer {sellerDress.set(it) })
            dressRepository.productConditionDressObservable.observeForever(Observer {productConditionDress.set(it) })
            dressRepository.priceDressObservable.observeForever(Observer {priceDress.set(it) })
            dressRepository.descriptionDressObservable.observeForever(Observer {descriptionDress.set(it) })

            Log.d("AddDressViewModel: init","update object")
        }
        else{
            Log.d("AddDressViewModel: init","new object")
        }


    }

    fun receiveDressById(id: Long, isSaveNewDressOnServer: Boolean) {
        dressRepository.receiveDressById(id, isSaveNewDressOnServer)
    }


    fun onSendButton(/*id: Long*/) {

        if (allFilldsIGood()) {
            val idUserFirebase = if (FirebaseAuth.getInstance().currentUser != null) FirebaseAuth.getInstance().currentUser?.uid else "-"
            Log.d(TAG, "dm-> idUserFirebase = $idUserFirebase")

            val dress = DressForSave().apply {
                userId = idUserFirebase ?: ""
                wordsToSearch = wordsToSearchDress.get() ?:""
                manufacturer = manufacturerDress.get() ?: ""
                model = modelDress.get() ?: ""
                silhouette = silhouetteDress.get() ?: ""
                sex = sexDress.get() ?: ""
                fabric = fabricDress.get() ?: ""
                sleeve = sleeveDress.get() ?: ""
                city = cityDress.get() ?: ""
                size = sizeDress.get() ?: ""
                color = colorDress.get() ?: ""
                seller = sellerDress.get() ?: ""
                productCondition = productConditionDress.get() ?: ""
                price = priceDress.get() ?: ""
                description = descriptionDress.get() ?: ""

                url = getNavigator()?.getPhotoId() ?: ""

                oneColor = getNavigator()?.getOneColor()?.get() ?: false
                multiColor = getNavigator()?.getMultiColor()?.get() ?: false
                decoration = getNavigator()?.getDecoration()?.get() ?: false
                print = getNavigator()?.getPrint()?.get() ?: false

                productIsNew = getNavigator()?.getProductIsNew()?.get() ?: false

                Log.d(LoginFragment.TAG,"AddDressViewModel: km->Location  latitude=$d_latitude, lontitude=$d_longitude")

                latitude = d_latitude
                longitude = d_longitude

                Log.d(LoginFragment.TAG,"AddDressViewModel: km->Location  latitude=$latitude, lontitude=$longitude")
            }
            Log.d(TAG,"km->SendButton: ${dressSaveID.get() ?: -1}")
            if (!itemInCabinet){
                Log.d("AddDressViewModel", "new dress $itemInCabinet")
            saveDressWithDeferred(dress)}
            else{
                Log.d("AddDressViewModel", "update $itemInCabinet id: ${dressSaveID.get()}")
                saveDressById(dressSaveID.get() ?: 328, dress)
            }

           // Log.d(TAG,"km->init save ${navViewModel.id.value}")

            getNavigator()?.showToast(model = "Dress ${dress.model} is saved!")

            wordsToSearchDress.set("")
            manufacturerDress.set("")
            modelDress.set("")
            silhouetteDress.set("")
            sexDress.set("")
            fabricDress.set("")
            sleeveDress.set("")
            cityDress.set("")
            sizeDress.set("")
            colorDress.set("")
            productConditionDress.set("")
            priceDress.set("")
            descriptionDress.set("")


        } else {
            getNavigator()?.showToast("Dress not Save !!")
        }


    }

    private fun saveDressWithDeferred(/*id: Long,*/dress: DressForSave) {
        saveDressObservable.postValue(DressSaveInfo(Status.LOADING))
//id получать или генерить проверка на нуль
        //после этого через put, а лучше наверное path отправлять
        Log.d(TAG,"km->init save $dressSaveID")
        //Log.d(TAG,"km->init save ${navViewModel.id.value}")
       // if(id == null){
        coroutineScope.launch {
            var getPropertiesDeferred = RetrofitApi.retrofitApiService.saveDress(dress)
            try {
                Log.d(TAG, "all good")
                var dressResponse = getPropertiesDeferred.await()
                Log.d(TAG, "code ${dressResponse.code()}")
                if (dressResponse.isSuccessful) {
                    Log.d(TAG, "dm-> code ${dressResponse.code()}")
                    saveDressObservable.postValue(DressSaveInfo(Status.SUCCESS))
                } else {
                    saveDressObservable.postValue(DressSaveInfo(Status.ERROR))
                }
            } catch (e: Exception) {
                Log.d(TAG, "Error :: ${e.message}")
                saveDressObservable.postValue(DressSaveInfo(Status.ERROR_CONNECTION))
            }
        }/*} else if (id!=null){
            coroutineScope.launch {
                var getPropertiesDeferred = RetrofitApi.retrofitApiService.saveDressById(id,dress)
                try {
                    Log.d(TAG, "all good")
                    var dressResponse = getPropertiesDeferred.await()
                    Log.d(TAG, "code ${dressResponse.code()}")
                    if (dressResponse.isSuccessful) {
                        Log.d(TAG, "dm-> code ${dressResponse.code()}")
                        saveDressObservable.postValue(DressSaveInfo(Status.SUCCESS))
                    } else {
                        saveDressObservable.postValue(DressSaveInfo(Status.ERROR))
                    }
                } catch (e: Exception) {
                    Log.d(TAG, "Error :: ${e.message}")
                    saveDressObservable.postValue(DressSaveInfo(Status.ERROR_CONNECTION))
                }
            }*/

    }


    private fun saveDressById(id: Long,dress: DressForSave){
        val idUserFirebase = if (FirebaseAuth.getInstance().currentUser != null) FirebaseAuth.getInstance().currentUser?.uid else "-"
        Log.d(TAG, "AddDressViewModel:saveDressById idUserFirebase = $idUserFirebase")

        //Call<DressForSave>
        var call: Call<DressForSave> = RetrofitApi.retrofitApiService.saveDressById(id, dress)
        call.enqueue(object: Callback<DressForSave> {
            override fun onFailure(call: Call<DressForSave>, t: Throwable) {
                Log.d("TEST PUT trailer", t.message)

                saveDressObservable.postValue(DressSaveInfo(Status.ERROR))
            }

            override fun onResponse(call: Call<DressForSave>, response: Response<DressForSave>) {
                Log.d("AddDressViewModel", "Call onResponse: ${response.message()}")
                saveDressObservable.postValue(DressSaveInfo(Status.SUCCESS))
            }

        })
    }


    fun allFilldsIGood(): Boolean {
        var isModelFilled = false
        var isCityFilled = false
        var isFabricFilled = false
        var isPriceFilled = false
        var isSexFilled = false
        var isColorFilled = false
        var isSellerFilled = false
        var isConditionFilled = false
        var isManufacturerFilled = false

        if (!modelDress.get().isNullOrEmpty()) isModelFilled = true
        if (!cityDress.get().isNullOrEmpty()) isCityFilled = true
        if (!fabricDress.get().isNullOrEmpty()) isFabricFilled = true
        if (!priceDress.get().isNullOrEmpty()) isPriceFilled = true
        if (!manufacturerDress.get().isNullOrEmpty())isManufacturerFilled = true
        if (!sexDress.get().isNullOrEmpty()) isSexFilled = true
        if (!colorDress.get().isNullOrEmpty()) isColorFilled = true
        if (!sellerDress.get().isNullOrEmpty()) isSellerFilled = true
        if (!productConditionDress.get().isNullOrEmpty()) isConditionFilled = true

        if (!isModelFilled || !isManufacturerFilled || !isCityFilled || !isPriceFilled||
                !isFabricFilled || !isSexFilled || !isColorFilled || !isSellerFilled ||
                !isConditionFilled) {
            getNavigator()?.errorMessages(isModelFilled, isManufacturerFilled, isFabricFilled,
                    isCityFilled, isPriceFilled, isSexFilled, isColorFilled,
                    isSellerFilled, isConditionFilled)
            return false
        } else {
            return true
        }


    }

    fun updateSize(selectedIndex:  Int){
        sizeType = when(selectedIndex.toString()){
            DressSizeType.XS.id -> DressSizeType.XS
            DressSizeType.S.id -> DressSizeType.S
            DressSizeType.M.id -> DressSizeType.M
            DressSizeType.L.id -> DressSizeType.L
            DressSizeType.XL.id -> DressSizeType.XL
            DressSizeType.ONESIZE.id -> DressSizeType.ONESIZE
            else -> DressSizeType.OTHER
        }
        sizeDress.set(sizeType?.getSizeType()?:"")
    }

    fun updateSleeve(selectedIndex:  Int){
        sleeveType = when(selectedIndex.toString()){
            DressSleeveType.FULL.id -> DressSleeveType.FULL
            DressSleeveType.HALF.id -> DressSleeveType.HALF
            DressSleeveType.THREEQ.id -> DressSleeveType.THREEQ
            DressSleeveType.SHORT.id -> DressSleeveType.SHORT
            DressSleeveType.SLEEVELESS.id -> DressSleeveType.SLEEVELESS
            else -> DressSleeveType.OTHER
        }
        sleeveDress.set(sleeveType?.getSleeveType() ?: "")
    }

    fun updateSilhouette(selectedIndex:  Int){
        silhouetteType = when(selectedIndex.toString()){
            DressSilhouetteType.ALINE.id -> DressSilhouetteType.ALINE
            DressSilhouetteType.ASYMMETRICAL.id -> DressSilhouetteType.ASYMMETRICAL
            DressSilhouetteType.BUD.id -> DressSilhouetteType.BUD
            DressSilhouetteType.MERMAID.id -> DressSilhouetteType.MERMAID
            DressSilhouetteType.SHEATH.id -> DressSilhouetteType.SHEATH
            else -> DressSilhouetteType.OTHER
        }
        silhouetteDress.set(silhouetteType?.getSilhouetteType() ?: "")
    }


    fun onDressSizeClick() {
        getNavigator()?.createSelectSizeDialog()
    }

    fun onDressSleeveClick() {
        getNavigator()?.createSelectSleeveDialog()
    }

    fun onDressSilhouetteClick() {
        getNavigator()?.createSelectSilhouetteDialog()
    }


    fun openFileChooser() = getNavigator()?.openFileChooser()
    fun uploadFile() = getNavigator()?.uploadFile()


    fun displayCharacteristic() {
        navigateToDressCharacteristics.value = true
    }

    fun displayCharacteristicsComplete() {
        navigateToDressCharacteristics.value = false
    }

}