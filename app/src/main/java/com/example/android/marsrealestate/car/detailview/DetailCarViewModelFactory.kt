package com.example.android.marsrealestate.car.detailview

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.android.marsrealestate.car.Car


class DetailCarViewModelFactory(
        private val message: Car,
        private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailCarViewModel::class.java)) {
            return DetailCarViewModel(message, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
