package com.example.android.marsrealestate.computer.addcomputer

/**
 * Интерфейс ComputerNavigator
 * @author Ilya Osipov (mailto:il.osipov.gm@gmail.com)
 * @since 28.04.2020
 * @version $Id$
 */

interface ComputerNavigator {

    fun createSelectManufacturerDialog()
}