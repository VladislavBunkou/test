package com.example.android.marsrealestate.shoes

import com.example.android.marsrealestate.di.mv.BaseViewModel

class ItemShoesViewModel: BaseViewModel<Any>(){

    var listener : ShoesItemListener? = null
    lateinit var shoes: Shoes

    fun bind(shoes: Shoes, listener : ShoesItemListener?){
        this.shoes = shoes
        this.listener = listener

    }

    fun onClickItem(){
         listener?.onSubUserItemClick(shoes)
    }

}