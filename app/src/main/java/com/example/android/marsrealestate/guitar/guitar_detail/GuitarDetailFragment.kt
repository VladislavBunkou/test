package com.example.android.marsrealestate.guitar.guitar_detail

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentGuitarDetailBinding

//fragment that outputs info about one selected guitar
class GuitarDetailFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentGuitarDetailBinding.inflate(inflater)
        binding.lifecycleOwner = this

        val guitar = GuitarDetailFragmentArgs.fromBundle(arguments!!).selectedGuitar
        val viewModel = GuitarDetailViewModel(guitar)

        binding.data = viewModel

        binding.txtvGuitarIsNew.text = if (guitar.productIsNew) "Yes" else "No"

        return binding.root
    }
}
