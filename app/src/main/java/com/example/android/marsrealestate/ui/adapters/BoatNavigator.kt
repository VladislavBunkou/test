package com.example.android.marsrealestate.ui.adapters

import androidx.databinding.ObservableField

interface BoatNavigator{
    fun showToast(model: String, country: String)
    fun errorMessages(isModelFilled : Boolean, isCountryFilled: Boolean, isTitleFilled: Boolean)

    fun getPhotoId(): String
    fun uploadFile()
    fun openFileChooser()

    fun createSelectManufacturerDialog()
}