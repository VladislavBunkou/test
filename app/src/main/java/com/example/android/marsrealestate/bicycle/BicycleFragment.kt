package com.example.android.marsrealestate.bicycle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.navigateUp
import com.akordirect.vmccnc.databinding.FragmentBicycleListBinding
import com.example.android.marsrealestate.ui.phone.ToolbarNavigator

class BicycleFragment: Fragment(), BicycleItemListener {



    private val viewModel by viewModels<BicycleViewModel>()
    lateinit var binding: FragmentBicycleListBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = FragmentBicycleListBinding.inflate(inflater)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.listenerBicycle = this

        binding.lasersGrid.adapter = BicycleRecyclerAdapter()




        return binding.root
    }

    override fun onSubUserItemClick(s: Bicycle) {
        this.findNavController().navigate(BicycleFragmentDirections.actionBicycleFragmentToBicycleDetailFragment(s))
    }


}

