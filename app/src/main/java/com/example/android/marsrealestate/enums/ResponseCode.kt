package com.example.android.marsrealestate.enums

enum class ResponseCode(val value: Int) {
    SUCCESS(200),
    SAVED(201),
    UNAUTHORIZED(401),
    NOT_FOUND(404),
    NO_INTERNET_CONNECTION(450),
    UNKNOWN(451),
    INCORRECT_DATA(452);
}


enum class Status {
    INITIAL, NODATA, LOADING, SUCCESS, CACHED, ERROR, ERROR_CONNECTION
}
