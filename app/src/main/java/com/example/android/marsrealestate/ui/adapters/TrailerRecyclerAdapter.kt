package com.example.android.marsrealestate.ui.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.GridViewItemTrailerBinding
import com.example.android.marsrealestate.di.mv.trailer.ItemTrailerViewModel
import com.example.android.marsrealestate.repository.trailer.TrailerItemListener
import com.example.android.marsrealestate.trailer.model.Trailer
import java.util.ArrayList



class TrailerRecyclerAdapter() : RecyclerView.Adapter<TrailerRecyclerAdapter.TrailerPropertyviewHolder>() {

    private val trailersList: ArrayList<Trailer> = arrayListOf()
    private lateinit var listener: TrailerItemListener

    override fun getItemCount(): Int = trailersList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrailerPropertyviewHolder {
        return TrailerPropertyviewHolder(GridViewItemTrailerBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: TrailerPropertyviewHolder, position: Int) {
        val itemTrailerViewModel = ItemTrailerViewModel()
        itemTrailerViewModel.bind(trailersList[position], listener)
        holder.bind(itemTrailerViewModel)

    }

    fun addItems(list: List<Trailer>) {
        this.trailersList.clear()
        this.trailersList.addAll(list)
        notifyDataSetChanged()
    }

    fun addListener(listener: TrailerItemListener){
        Log.d("TrailersRecyclerAdapter", "dm-> Listener is add  = $listener")
        this.listener = listener
    }

    class TrailerPropertyviewHolder(private var binding: GridViewItemTrailerBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(itemTrailerViewModel: ItemTrailerViewModel) {
            binding.mv = itemTrailerViewModel
            binding.executePendingBindings()
        }

    }
}



