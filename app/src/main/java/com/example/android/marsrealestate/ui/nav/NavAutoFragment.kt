package com.example.android.marsrealestate.ui.nav

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.databinding.FragmentNavAutoBinding
import com.example.android.marsrealestate.di.mv.nav.NavAutoListener

class NavAutoFragment : Fragment(), NavAutoListener {
    val TAG = "NavAutoFragment"
//    private val viewModel: NavAutoViewModel by lazy { ViewModelProviders.of(this).get(NavAutoViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentNavAutoBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.listener = this
        return binding.root
    }

    override fun clickAvto() {
        this.findNavController().navigate(NavAutoFragmentDirections.actionNavAutoFragmentToAtvFragment())
    }

    override fun clickBoat() {
        this.findNavController().navigate(NavAutoFragmentDirections.actionNavAutoFragmentToBoatFragment())
    }

    override fun clickAtv() {
        this.findNavController().navigate(NavAutoFragmentDirections.actionNavAutoFragmentToAtvFragment())
    }

    override fun clickTrailer() {
        this.findNavController().navigate(NavAutoFragmentDirections.actionNavAutoFragmentToTrailerst())
    }

    override fun clickMotorbike() {
        this.findNavController().navigate(NavAutoFragmentDirections.actionNavAutoFragmentToMotorbike1Fragment())
    }

    override fun clickBicycle() {
        this.findNavController().navigate(NavAutoFragmentDirections.actionTestFragmentToBicycleFragment())
    }
}