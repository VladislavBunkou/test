package com.example.android.marsrealestate.ui.adapters.phone

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.ItemPhoneFileBinding
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.module.AppGlideModule
import com.example.android.marsrealestate.di.mv.phone.PhoneFile
import com.example.android.marsrealestate.di.mv.phone.PhoneFileItemVM
import com.example.android.marsrealestate.di.mv.phone.PhoneFileType
import java.io.File


class PhoneFileRecyclerAdapter(private val phoneFileList: MutableList<PhoneFile> = ArrayList()) : RecyclerView.Adapter<PhoneFilesViewHolder>(), PhoneFileItemVM.PhoneFileItemVMListener {

    private var listener: PhoneFilesRecyclerAdapterListener? = null

    fun setPhoneFileRecyclerAdapterListener(mListener: PhoneFilesRecyclerAdapterListener?) {
        this.listener = mListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhoneFilesViewHolder {
        val itemPhoneFileBinding = ItemPhoneFileBinding.inflate(LayoutInflater.from(parent.context))
        return PhoneFilesViewHolder(itemPhoneFileBinding)
    }

    override fun onBindViewHolder(profileFilesViewHolder: PhoneFilesViewHolder, position: Int) {
        profileFilesViewHolder.onBind(PhoneFileItemVM(phoneFileList[position], this))
    }

    fun addItems(ticketList: List<PhoneFile>) {
        phoneFileList.clear()
        phoneFileList.addAll(ticketList)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = phoneFileList.size

    override fun onAddPhoneFile() {
        listener?.onAddFile()
    }

    override fun onDeletePhoneFile(profileFile: PhoneFile) {
        val position = phoneFileList.indexOf(profileFile)
        phoneFileList.removeAt(position)
        notifyItemRemoved(position)
        listener?.onRemoveFile(profileFile)
    }

    interface PhoneFilesRecyclerAdapterListener {
        fun onAddFile()
        fun onRemoveFile(ticketFile: PhoneFile)
    }
}


class PhoneFilesViewHolder(private val vBinding: ItemPhoneFileBinding) : RecyclerView.ViewHolder(vBinding.root) {
    fun onBind(vm: PhoneFileItemVM) {
        vBinding.viewModel = vm
        if (vm.item.profileFileType === PhoneFileType.ADD_BTN_TYPE) {
            vBinding.ticketsFilePdf.visibility = View.GONE
            vBinding.deletePhoneFileIcon.visibility = View.GONE
            vBinding.addPhoneFile.visibility = View.VISIBLE
        } else {
            vBinding.ticketsFilePdf.visibility = View.VISIBLE
            vBinding.profileImage.visibility = View.VISIBLE


//              BindingUtils.setImageFromUri(vBinding.profileImage, vm.item.item) // !!

            vBinding.deletePhoneFileIcon.visibility = View.VISIBLE
            vBinding.addPhoneFile.visibility = View.GONE
        }
    }


}

