package com.example.android.marsrealestate

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.ActivityMainBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

class MainActivity : AppCompatActivity() {

    lateinit var fusedLocationClient: FusedLocationProviderClient// = LocationServices.getFusedLocationProviderClient(activity)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        @Suppress("UNUSED_VARIABLE")
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
//        drawerLayout = binding.drawerLayout
        val navController = this.findNavController(R.id.myNavHostFragment)

//        NavigationUI.setupActionBarWithNavController(this, navController)
        NavigationUI.setupWithNavController(binding.navView, navController) // to display the navigation drawer.
        NavigationUI.setupWithNavController(binding.bottomMenuNavigation, navController)

        val appBarConfiguration = AppBarConfiguration(navController.graph) // For back buttons
        NavigationUI.setupActionBarWithNavController(this , navController, appBarConfiguration)

        //getSupportFragmentManager().addOnBackStackChangedListener(onBackPressed());
        //Handle when activity is recreated like on orientation Change
    }

    override fun onSupportNavigateUp(): Boolean { //This method is called when the up button is pressed. Just the pop back stack.
        supportFragmentManager.popBackStack()
        return true
    }
}
