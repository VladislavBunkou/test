package com.example.android.marsrealestate.computer

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.ComputerItemBinding
import com.example.android.marsrealestate.computer.model.Computer

/**
 * Класс ComputerRecyclerAdapter
 * @author Ilya Osipov (mailto:il.osipov.gm@gmail.com)
 * @since 27.04.2020
 * @version $Id$
 */

class ComputerRecyclerAdapter(private val onClickListenerComputer: OnClickListenerComputer) :
        ListAdapter<Computer, ComputerRecyclerAdapter.ComputerPropertyViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : ComputerPropertyViewHolder {
        return ComputerPropertyViewHolder(ComputerItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: ComputerPropertyViewHolder, position: Int) {
        val computerProperty = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListenerComputer.onClick(computerProperty)
        }
        holder.bind(computerProperty)
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Computer>() {
        override fun areItemsTheSame(oldItem: Computer, newItem: Computer) : Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Computer, newItem: Computer): Boolean {
            return oldItem.id == newItem.id
        }
    }

    class ComputerPropertyViewHolder(private var binding: ComputerItemBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(computer: Computer) {
            binding.property = computer
            binding.executePendingBindings()
        }
    }

    class OnClickListenerComputer(val clickListener: (computer: Computer) -> Unit) {
        fun onClick(computer: Computer) = clickListener(computer)
    }
}