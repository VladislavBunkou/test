package com.example.android.marsrealestate.dress.addDress

enum class DressSilhouetteType(val id: String) {
    ALINE("1"){ override fun getSilhouetteType(): String = "A-Line" },
    ASYMMETRICAL("2"){ override fun getSilhouetteType(): String = "Asymmetrical" },
    BUD("3"){ override fun getSilhouetteType(): String = "Bud" },
    SHEATH("4"){ override fun getSilhouetteType(): String = "Sheath" },
    MERMAID("5"){ override fun getSilhouetteType(): String = "Mermaid" },
    OTHER("6"){ override fun getSilhouetteType(): String = "Other" };

    abstract fun getSilhouetteType(): String
}