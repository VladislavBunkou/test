package com.example.android.marsrealestate.guitar

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.R
import com.example.android.marsrealestate.guitar.guitar_list.GuitarApiStatus
import com.example.android.marsrealestate.guitar.guitar_list.GuitarRecyclerAdapter

//bind guitar list to RecyclerView
@BindingAdapter("listDataGuitars")
fun bindRecyclerViewTablets(recyclerView: RecyclerView, data: List<Guitar>?) {
    val adapter = recyclerView.adapter as GuitarRecyclerAdapter
    adapter.submitList(data)
}

//use status image due to api status
@BindingAdapter("guitarApiStatus")
fun bindStatusGuitar(statusImageView: ImageView, status: GuitarApiStatus?) {
    when (status) {
        GuitarApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        GuitarApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        GuitarApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}