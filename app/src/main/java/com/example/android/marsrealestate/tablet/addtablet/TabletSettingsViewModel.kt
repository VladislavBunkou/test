package com.example.android.marsrealestate.tablet.addtablet

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TabletSettingsViewModel: ViewModel() {
    var isSimSelected = MutableLiveData<Boolean>()
    var isNewSelected = MutableLiveData<Boolean>()


    var sellerSelected = MutableLiveData<String>("")    //not working without "multiDexEnabled true" in build.gradle
    var yearSelected = MutableLiveData<String>("")    //not working without "multiDexEnabled true" in build.gradle
}