package com.example.android.marsrealestate.ui.boat

import android.Manifest
import android.app.ProgressDialog.show
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentBoatBinding
import com.example.android.marsrealestate.boat.model.Boat
import com.example.android.marsrealestate.di.mv.boat.BoatViewModel
import com.example.android.marsrealestate.globalLatitude
import com.example.android.marsrealestate.globalLongitude
import com.example.android.marsrealestate.repository.boat.BoatItemListener
import com.example.android.marsrealestate.showDistance
import com.example.android.marsrealestate.ui.adapters.BoatRecyclerAdapter
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.example.android.marsrealestate.ui.boat.boat_latitude
import com.example.android.marsrealestate.ui.boat.boat_longitude
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
class BoatFragment: Fragment(), BoatItemListener {

    private val viewModel by viewModels<BoatViewModel>()
    lateinit var fusedLocationClient:FusedLocationProviderClient

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = FragmentBoatBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.listenerBoat = this
        binding.viewModel = viewModel

        binding.lasersGrid.adapter = BoatRecyclerAdapter()

        binding.fabMap.setOnClickListener {
            this.findNavController().navigate(BoatFragmentDirections.actionBoatFragmentToBoatMapFragment())
        }

        fusedLocationClient  = LocationServices.getFusedLocationProviderClient(requireActivity())

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onSubUserItemClick(boat: Boat) {
        this.findNavController().navigate(BoatFragmentDirections.actionBoatFragmentToBoatOneFragment(boat))
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)

        val menuItem = menu.findItem(R.id.search_prod)
        val searchView = menuItem.actionView as SearchView
        searchView.queryHint = "Search..."
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                Log.d("BoatFragment", "dm:: onQueryTextSubmit")
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                Log.d("BoatFragment", "dm:: onQueryTextChange = $newText")
                viewModel.getBoatList(newText)
                return true
            }

        })
        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.set_location -> {
            setLocationWithPermissionCheck()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    fun setLocation(){
        val task: Task<Location> = fusedLocationClient.lastLocation
        task.addOnSuccessListener(OnSuccessListener {
            if (it != null) {
                showDistance = true
                boat_longitude = it.longitude
                boat_latitude = it.latitude
                globalLongitude = it.longitude
                globalLatitude = it.latitude
                Toast.makeText(context, "Your's latitude = $globalLatitude / longitude = $globalLongitude    ", Toast.LENGTH_SHORT).show()
                Log.d("LOCATION", "dm::  latitude = $globalLatitude / longitude = $globalLongitude")
                Log.d("LOCATION", "dm::  boat_latitude = $boat_latitude / boat_longitude = $boat_longitude")
                viewModel.getBoatList("")
            }
        }).addOnCanceledListener {
            Toast.makeText(context, "Canceled location", Toast.LENGTH_SHORT).show()
        }.addOnFailureListener {
            Toast.makeText(context, "Something wrong with location", Toast.LENGTH_SHORT).show()
            Log.d("LOCATION", "error: ${it.message}")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        showDistance = false
    }

}