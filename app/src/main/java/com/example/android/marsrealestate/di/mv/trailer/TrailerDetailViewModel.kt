package com.example.android.marsrealestate.di.mv.trailer

import android.app.Application
import androidx.lifecycle.*
import com.example.android.marsrealestate.trailer.model.Trailer


class TrailerDetailViewModel(grindingCylindrical: Trailer, app: Application) : AndroidViewModel(app){

    private val _selectedProperty = MutableLiveData<Trailer>()
    val selectedProperty: LiveData<Trailer>
        get() = _selectedProperty

    init {
        _selectedProperty.value = grindingCylindrical
    }
}



class TrailerViewModelFactory(
        private val grilling: Trailer,
        private val application: Application) : ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TrailerDetailViewModel::class.java)) {
            return TrailerDetailViewModel(grilling, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}
