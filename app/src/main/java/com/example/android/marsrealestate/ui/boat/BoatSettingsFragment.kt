package com.example.android.marsrealestate.ui.boat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentBoatSettingsBinding
import com.example.android.marsrealestate.di.mv.boat.BoatInfoViewModel
import com.example.android.marsrealestate.di.mv.boat.BoatSettingsViewModel

class BoatSettingsFragment: Fragment() {

    lateinit var binding: FragmentBoatSettingsBinding

    private val viewModel by viewModels<BoatSettingsViewModel>()

    // for saving data
    private lateinit var boatInfoViewModel: BoatInfoViewModel



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val application = requireNotNull(activity).application

        binding = FragmentBoatSettingsBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel


        // activity?.let {
        /*     boatInfoViewModel = ViewModelProviders.of(it).get(BoatInfoViewModel::class.java)

             viewModel.is3GSelected.value = boatInfoViewModel.isBoatHas3G.get()

             viewModel.is3GSelected.observe(viewLifecycleOwner, Observer {
                 boatInfoViewModel.isBoatHas3G.set(it)
                 Log.d(TAG, "dm-> 3G is $it ")
             })

         }*/



        return binding.root
    }
}