package com.example.android.marsrealestate.computer

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.computer.model.Computer
import com.example.android.marsrealestate.network.RetrofitApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

/**
 * ComputerViewModel
 * @author Ilya Osipov (mailto:il.osipov.gm@gmail.com)
 * @since 27.04.2020
 * @version $Id$
 */
enum class ComputerApiStatus {
    LOADING, ERROR, DONE
}

class ComputerViewModel : ViewModel() {

    val computers = MutableLiveData<List<Computer>>()
    val status = MutableLiveData<ComputerApiStatus>()
    val navigateToSelectedComputer = MutableLiveData<Computer>()

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
        getComputerProperties()
    }

    private fun getComputerProperties() {
        coroutineScope.launch {
            var getPropertiesDeferred = RetrofitApi.retrofitApiService.getComputerList()

            try {
                status.value = ComputerApiStatus.LOADING
                var listResult = getPropertiesDeferred.await()
                status.value = ComputerApiStatus.DONE
                computers.value = listResult
            } catch (error: Exception) {
                status.value = ComputerApiStatus.ERROR
                Log.d("Error dm - > ", "Internet connection not exist or Server is shot down")
                computers.value = ArrayList()
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun displayOneLathe(computer: Computer) {
        navigateToSelectedComputer.value = computer
    }

    fun displayOneLatheComplete() {
        navigateToSelectedComputer.value = null
    }
}