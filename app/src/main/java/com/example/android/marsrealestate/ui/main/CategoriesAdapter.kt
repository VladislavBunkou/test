package com.example.android.marsrealestate.ui.main

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.ItemViewCategoryBinding
import com.example.android.marsrealestate.di.mv.BaseViewModel

class CategoriesAdapter(var listener: CategoriesListener,
                        private val list: MutableList<String> = ArrayList()) : RecyclerView.Adapter<CategoryViewHolder>(),
                        CategoryItemVM.CategoryItemListener {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val itemPhoneFileBinding = ItemViewCategoryBinding.inflate(LayoutInflater.from(parent.context))
        return CategoryViewHolder(itemPhoneFileBinding)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.onBind(CategoryItemVM(list[position], this))
    }

    fun addItems(categoriesList: List<String>) {
        list.clear()
        list.addAll(categoriesList)
        notifyDataSetChanged()
    }

    override fun onClickCategory(nameCategory: String) {
        Log.d("CategoriesAdapter", "dm:: CategoriesAdapter - Click")
        listener.onClickCategory(nameCategory)
    }
}

class CategoryViewHolder(private val vBinding: ItemViewCategoryBinding) : RecyclerView.ViewHolder(vBinding.root) {
    fun onBind(vm: CategoryItemVM) {
        vBinding.vm = vm
    }
}


class CategoryItemVM(
        val categoryName: String, val itemListener: CategoryItemListener) : BaseViewModel<Any>() {
    fun onClickCategory() = itemListener.onClickCategory(categoryName)

    interface CategoryItemListener {
        fun onClickCategory(string: String)
    }

}