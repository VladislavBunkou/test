package com.example.android.marsrealestate.network
import com.example.android.marsrealestate.atv.Atv
import com.example.android.marsrealestate.atv.AtvBody
import com.example.android.marsrealestate.guitar.Guitar
import com.example.android.marsrealestate.bicycle.Bicycle
import com.example.android.marsrealestate.bicycle.addNewBicycle.ForSaveBicycle
import com.example.android.marsrealestate.boat.addboat.BoatBody
import com.example.android.marsrealestate.boat.model.Boat
import com.example.android.marsrealestate.boat.model.BoatOne
import com.example.android.marsrealestate.car.Car
import com.example.android.marsrealestate.computer.addcomputer.ComputerBody
import com.example.android.marsrealestate.computer.model.Computer
import com.example.android.marsrealestate.computer.model.ComputerOne
import com.example.android.marsrealestate.dress.Dress
import com.example.android.marsrealestate.dress.DressOne
import com.example.android.marsrealestate.dress.addDress.DressForSave
import com.example.android.marsrealestate.motorbike.Motorbike
import com.example.android.marsrealestate.model.Prod
import com.example.android.marsrealestate.model.ProdOne
import com.example.android.marsrealestate.repository.body.PhoneBody
import com.example.android.marsrealestate.model.Phone
import com.example.android.marsrealestate.model.PhoneOne
import com.example.android.marsrealestate.tablet.addtablet.TabletBody
import com.example.android.marsrealestate.tablet.model.Tablet
import com.example.android.marsrealestate.shoes.Shoes
import com.example.android.marsrealestate.shoes.addShoes.ShoesForSave
import com.example.android.marsrealestate.tablet.model.TabletOne
import com.example.android.marsrealestate.trailer.addtrailer.TrailerBody
import com.example.android.marsrealestate.trailer.model.Trailer
import com.example.android.marsrealestate.trailer.model.TrailerOne
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit


private const val BASE_URL = "http://q11.jvmhost.net/"
private val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

object RetrofitApi {
    private val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL) // 1 URI
            .addConverterFactory(MoshiConverterFactory.create(moshi)) // 2 converter factory
            .addCallAdapterFactory(CoroutineCallAdapterFactory()) // !! CoroutineCallAdapterFactory allows us returns  Deferred object  .
            .build()

    val retrofitApiService: RetrofitApiService by lazy {
        retrofit.create(RetrofitApiService::class.java)
    }
}


interface RetrofitApiService {

    // Prod
    @GET("prod/{id}")
    fun getProd(@Path("id") id: Long): Deferred<ProdOne>

    @GET("prod_json")
    fun getListProd(): Deferred<List<Prod>>

    @GET("prods_select_by_model_or_manufacturer/{str}")
    fun getListProd(@Path("str") str: String): Deferred<List<Prod>>

    @GET("prods_by_userid/{idStr}")
    fun getListProdById(@Path("idStr") idStr: String): Deferred<List<Prod>>


    //Phone
    @GET("phone/{id}")
    fun getPhoneOne(@Path("id") id: Long): Deferred<PhoneOne>

    @GET("phone_json")
    fun getPhoneList(): Deferred<List<Phone>>

    @POST("phone")
    fun savePhone(@Body phoneBody: PhoneBody): Deferred<Response<Unit>>


    //Tablet
    @GET("tablet/{id}")
    fun getTablet(@Path("id") id: Long): Deferred<TabletOne>

    @GET("tablet_json")
    fun getTabletList(): Deferred<List<Tablet>>

    @POST("tablet")
    fun saveTablet(@Body tabletBody: TabletBody): Deferred<String>

    // Car
    @GET("car_json")
    fun getCars(): Deferred<List<Car>>



    // Boat
    @GET("boat_json")
    fun getBoatList(): Deferred<List<Boat>>

    @GET("boats_select_by_model_or_manufacturer/{str}")
    fun getBoatList(@Path("str") str: String): Deferred<List<Boat>>

    @POST("boat")
    fun saveBoat(@Body boatBody: BoatBody): Deferred<Response<Unit>>

    @GET("boat/{id}")
    fun getBoatOne(@Path("id") id: Long): Deferred<BoatOne>

    @PUT("boat/{id}")
    fun updateBoat(
            @Path("id") id: Long,
            @Body boatBody: BoatBody
    ): Call<BoatBody>



    // Guitar
    @GET("guitar_json")
    fun getGuitarList(): Deferred<List<Guitar>>

    @POST("guitar")
    fun addGuitar(@Body guitar: Guitar): Call<Guitar>


    // Quadrocycle
    @GET("quadrocycle_json")
    fun getAtvList(): Deferred<List<Atv>>

    @POST("quadrocycle")
    fun addAtv(@Body atvBody: AtvBody): Call<AtvBody>


    //Bicycle
    @GET("bicycle_json")
    fun getBicycleList(): Deferred<List<Bicycle>>

    @POST("bicycle")
    fun saveBicycle(@Body addBicycleDataClass: ForSaveBicycle): Deferred<Response<Unit>>

    @GET("bicycle_by_id/{id}")
    fun getBicycleById(@Path("id") id: Long): Deferred<Bicycle>

    /*
    @PUT("bicycle/{id}")
    fun saveBicycleById(@Path("id") id: Long,@Body bicycle: ForSaveBicycle): Call<ForSaveBicycle>
    */

    //Dress
    @GET("dress_json")
    fun getDressList(): Deferred<List<Dress>>

    @POST("dress")
    fun saveDress(@Body dressForSave: DressForSave): Deferred<Response<Unit>>

    @GET("dress_by_id/{id}")
    fun getDressById(@Path("id") id: Long): Deferred<Dress>

    @PUT("dress/{id}")
    fun saveDressById(@Path("id") id: Long,@Body dressForSave: DressForSave): Call<DressForSave>

    @GET("dress_select_by_model_or_manufacturer/{str}")
    fun getDressList(@Path("str") str: String): Deferred<List<Dress>>

    //Shoes
    @GET("shoes_json")
    fun getShoesList(): Deferred<List<Shoes>>

    @GET("shoes_by_id/{id}")
    fun getShoesById(@Path("id") id: Long): Deferred<Shoes>

    @POST("shoes")
    fun saveShoes(@Body shoesForSave: ShoesForSave): Deferred<Response<Unit>>


    // Trailer
    @GET("motorbike_json")
    fun getMotorbikeList(): Deferred<List<Motorbike>>

    @GET("trailer_json")
    fun getTrailerList(): Deferred<List<Trailer>>

    @POST("trailer")
    fun saveTrailer(@Body trailerBody: TrailerBody): Deferred<Response<Unit>>

    @GET("trailer/{id}")
    fun getTrailerOne(@Path("id") id: Long): Deferred<TrailerOne>

    @GET("trailers_select_by_model_or_manufacturer/{str}")
    fun getTrailersList(@Path("str") str: String): Deferred<List<Trailer>>

    @PUT("trailer/{id}")
    fun updateTrailer(
            @Path("id") id: Long,
            @Body trailerBody: TrailerBody
    ): Call<TrailerBody>

    //Computer
    @GET("computer/{id}")
    fun getComputer(@Path("id") id: Long): Deferred<ComputerOne>

    @GET("computer_json")
    fun getComputerList(): Deferred<List<Computer>>

    @GET("computer")
    fun saveComputer(@Body computerBody: ComputerBody): Deferred<String>
}