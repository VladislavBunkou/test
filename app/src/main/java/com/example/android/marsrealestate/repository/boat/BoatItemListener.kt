package com.example.android.marsrealestate.repository.boat

import com.example.android.marsrealestate.boat.model.Boat


interface BoatItemListener {
    fun onSubUserItemClick(s: Boat)
}