package com.example.android.marsrealestate.ui.nav

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.databinding.FragmentNavAutoBinding
import com.akordirect.vmccnc.databinding.FragmentNavHomesBinding
import com.example.android.marsrealestate.di.mv.nav.NavAutoListener
import com.example.android.marsrealestate.di.mv.nav.NavAutoViewModel
import com.example.android.marsrealestate.di.mv.nav.NavHomesListener
import com.example.android.marsrealestate.di.mv.nav.NavHomesViewModel

class NavHomesFragment : Fragment(), NavHomesListener {
    val TAG = "NavAutoFragment"

    private val viewModel: NavHomesViewModel by lazy { ViewModelProviders.of(this).get(NavHomesViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentNavHomesBinding.inflate(inflater)
        binding.lifecycleOwner = this
         binding.listener = this
        return binding.root
    }

    override fun clickFlat()  {
//        this.findNavController().navigate(NavAutoFragmentDirections.actionNavAutoFragmentToAtvFragment())

    }

}