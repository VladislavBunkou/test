package com.example.android.marsrealestate.guitar.guitar_add

import androidx.databinding.ObservableField
import com.example.android.marsrealestate.guitar.Guitar

interface GuitarAddNavigator {
    fun showToast(guitar: Guitar)
    fun showErrors(isModelCorrect: Boolean, isTypeOfProductCorrect: Boolean,
            isManufacturerCorrect: Boolean, isManufacturerCountryCorrect: Boolean,
            isGuitarNeckCorrect: Boolean, isGuitarBodyCorrect: Boolean,
            isGuitarScaleCorrect: Boolean, isScaleNumberOfFretsCorrect: Boolean,
            isGuitarColorCorrect: Boolean, isDescriptionCorrect: Boolean, isUriCorrect: Boolean)

//    fun get3G() : ObservableField<Boolean>
//    fun get4G() : ObservableField<Boolean>

    fun createSelectTypeOfProductDialog()
    fun createSelectManufacturerDialog()
    fun createSelectManufacturerCountryDialog()
    fun createSelectGuitarNeckDialog()
    fun createSelectGuitarBodyDialog()
    fun createSelectNumberOfFretsDialog()

    fun onProductIsNewSwitchClick()

    fun openFileChooser()
    fun uploadFile()
    fun getPhotoId(): String
}