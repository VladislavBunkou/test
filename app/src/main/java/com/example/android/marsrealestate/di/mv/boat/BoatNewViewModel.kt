package com.example.android.marsrealestate.di.mv.boat

import android.app.Activity
import android.location.Location
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.boat.addboat.BoatBody
import com.example.android.marsrealestate.boat.addboat.BoatManufacturerType
import com.example.android.marsrealestate.ui.adapters.BoatNavigator
import com.example.android.marsrealestate.di.mv.BaseViewModel
import com.example.android.marsrealestate.enums.Status
import com.example.android.marsrealestate.globalLatitude
import com.example.android.marsrealestate.globalLongitude
import com.example.android.marsrealestate.itemInCabinet
import com.example.android.marsrealestate.repository.boat.BoatRepository
import com.example.android.marsrealestate.repository.boat.BoatSaveInfo
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.URL



class BoatNewViewModel : BaseViewModel<BoatNavigator>() {

    var boat_latitude: Double = 0.0
    var boat_longitude: Double = 0.0

    private val TAG = "BoatNewViewModel"

    lateinit var boatRepository: BoatRepository

    var boatSavedId = ObservableField<Long>()
    var wordsToSearchBoat = ObservableField<String>("")
    var modelBoat = ObservableField<String>("") // for field
    var countryBoat = ObservableField<String>("") // for field
    var brandBoat = ObservableField<String>("") // for field
    var descriptionBoat = ObservableField<String>("")
    var cityBoat = ObservableField<String>("")
    var priceBoat = ObservableField<String>("")
    var productConditionBoat = ObservableField<String>("")
    var imageUrlBoat = ObservableField<String>()



    var PICK_IMAGE_REQUEST = 1

    val saveBoatObservable = MutableLiveData<BoatSaveInfo>()

    var navigateToBoatCharacteristics = MutableLiveData<Boolean>(false)
    var navigateToGoogleMap = MutableLiveData<Boolean>(false)

    private var manufacturerType: BoatManufacturerType? = null

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val saveBoatObserver = Observer<BoatSaveInfo> {
        when (it.status) {
            Status.INITIAL, Status.LOADING -> {
                setIsLoading(true)
                Log.d("PhoneNewViewModel", "dm-> Status.Loading")
            }
            Status.SUCCESS -> {
                setIsLoading(false)
                Log.d("PhoneNewViewModel", "dm-> Status.SUCCESS")
            }
            Status.ERROR -> {
                setIsLoading(false)
                Log.d("PhoneNewViewModel", "dm-> Status.Error")
            }
            Status.ERROR_CONNECTION -> {
                setIsLoading(false)
            }
            else -> {
            }
        }
    }

    init {
        saveBoatObservable.observeForever(saveBoatObserver)
        boatRepository = BoatRepository(coroutineScope)

        if(itemInCabinet) {
            boatRepository.idBoat.observeForever(Observer { boatSavedId.set(it) })
            boatRepository.modelBoatObservable.observeForever(Observer { modelBoat.set(it) })
            boatRepository.brandBoatObservable.observeForever(Observer { brandBoat.set(it) })
            boatRepository.countryBoatObservable.observeForever(Observer { countryBoat.set(it) })
            boatRepository.cityBoatObservable.observeForever(Observer { cityBoat.set(it) })
            boatRepository.descriptionBoatObservable.observeForever(Observer { descriptionBoat.set(it) })
            boatRepository.priceBoatObservable.observeForever(Observer { priceBoat.set(it) })
            boatRepository.productConditionBoatObservable.observeForever(Observer { productConditionBoat.set(it) })
            boatRepository.imageUrlBoatObservable.observeForever(Observer { imageUrlBoat.set(it) })
        } else {
            Log.d("observerForever","new boat")
        }
    }


    fun receiveBoatById(id: Long, isSaveNewBoatOnServer: Boolean) {
        boatRepository.receiveBoatById(id, isSaveNewBoatOnServer)
    }


    fun saveButton(){
        if(allFilldsIGood()) {
            saveBoatObservable.postValue(BoatSaveInfo(Status.LOADING))
            uploadFile()
        } else {
            getNavigator()?.showToast(model = "Boat not saved", country = "")
        }
    }

    fun onSendButton() {

            val idUserFirebase = if (FirebaseAuth.getInstance().currentUser != null) FirebaseAuth.getInstance().currentUser?.uid else "-"
            Log.d(TAG, "dm-> idUserFirebase = $idUserFirebase")


        val boat = BoatBody().apply {
                wordsToSearch = wordsToSearchBoat.get() ?: ""
                userId = idUserFirebase ?: ""
                model = modelBoat.get() ?: ""
                manufacturer = brandBoat.get() ?: ""
                manufacturerCountry = countryBoat.get() ?: ""
                price = priceBoat.get() ?: ""
                productCondition = productConditionBoat.get() ?: ""

                longitude = boat_longitude
                latitude = boat_latitude

                city = cityBoat.get() ?: ""
                description = descriptionBoat.get() ?: ""
                url = getNavigator()?.getPhotoId() ?: ""
            }

            getNavigator()?.showToast(model = "Boat ${boat.manufacturer} is saved!", country = boat.manufacturerCountry)  // for easier understanding this code can be deleted

        if (!itemInCabinet){
            Log.d("GridItem", "new boat $itemInCabinet")
            saveBoatWithDeferred(boat)
        }else {
            Log.d("GridItem", "update $itemInCabinet id: ${boatSavedId.get()}")
            updateBoat(boatSavedId.get() ?: 328, boat)
        }

            wordsToSearchBoat.set("")
            modelBoat.set("")
            countryBoat.set("")
            brandBoat.set("")
            priceBoat.set("")
            descriptionBoat.set("")
            cityBoat.set("")
            productConditionBoat.set("")

    }

    private fun updateBoat(id: Long, boat:   BoatBody) {
        val idUserFirebase = if (FirebaseAuth.getInstance().currentUser != null) FirebaseAuth.getInstance().currentUser?.uid else "-"
        Log.d(TAG, "dm-> idUserFirebase = $idUserFirebase")

        //Call<Boat>
        var call: Call<BoatBody> = RetrofitApi.retrofitApiService.updateBoat(id, boat)
        call.enqueue(object: Callback<BoatBody> {
            override fun onFailure(call: Call<BoatBody>, t: Throwable) {
                Log.d("TEST PUT trailer", t.message)

                saveBoatObservable.postValue(BoatSaveInfo(Status.ERROR))
            }

            override fun onResponse(call: Call<BoatBody>, response: Response<BoatBody>) {
                Log.d("TEST PUT boat", response.message())
                saveBoatObservable.postValue(BoatSaveInfo(Status.SUCCESS))
            }

        })
    }

    private fun saveBoatWithDeferred(boat: BoatBody) {
        saveBoatObservable.postValue(BoatSaveInfo(Status.LOADING))

        coroutineScope.launch {
            //                var getPropertiesDeferred = LatheApi.retrofitService.getProperties( )
            var getPropertiesDeferred = RetrofitApi.retrofitApiService.saveBoat(boat)
            try {
                //                    _status.value = LatheApiStatus.LOADING
                Log.d(TAG, "dm-> all good")
                val boatResponse = getPropertiesDeferred.await()
                Log.d(TAG, "dm-> ${boatResponse.code()}")

                if (boatResponse.isSuccessful) {
                    Log.d(TAG, "dm-> code ${boatResponse.code()}")
//                    var resposne = phoneResponse.body().toString()
//                    Log.d(TAG, "dm-> body ${phoneResponse.body().toString() }")
                    saveBoatObservable.postValue(BoatSaveInfo(Status.SUCCESS))
                } else {
                    saveBoatObservable.postValue(BoatSaveInfo(Status.ERROR))
                }

            } catch (e: Exception) {
                //                _status.value = LatheApiStatus.ERROR
                Log.d(TAG, "dm-> Error :: ${e.message}")
                saveBoatObservable.postValue(BoatSaveInfo(Status.ERROR_CONNECTION))
                //                _properties.value = ArrayList()
            }
        }
    }


    fun allFilldsIGood(): Boolean {
        var isTitleFilled = false
        var isModelFilled = false
        var isCountryFilled = false
        var isBrandFilled = false

        if (!wordsToSearchBoat.get().isNullOrEmpty()) isTitleFilled = true
        if (!modelBoat.get().isNullOrEmpty()) isModelFilled = true
        if (!countryBoat.get().isNullOrEmpty()) isCountryFilled = true
        if (!brandBoat.get().isNullOrEmpty()) isBrandFilled = true

        Log.d(TAG, "dm-> brand: ${brandBoat.get()}")

        if (!isModelFilled || !isCountryFilled || !isBrandFilled || !isTitleFilled) {
            getNavigator()?.errorMessages(isModelFilled, isCountryFilled, isTitleFilled)
            return false
        } else {
            return true
        }

    }

    fun updateManufacturer(selectedIndex:  Int){
        manufacturerType = when(selectedIndex.toString()){
            BoatManufacturerType.BIGBOAT.id -> BoatManufacturerType.BIGBOAT
            BoatManufacturerType.BRIG.id -> BoatManufacturerType.BRIG
            BoatManufacturerType.LINDER.id -> BoatManufacturerType.LINDER
            else -> BoatManufacturerType.WALKERBAY
        }
//        brandObservable.value = manufacturerType?.getBrand() ?: ""
        brandBoat.set(manufacturerType?.getBrand() ?: "")
    }

    fun onBoatManufacturerClick() = getNavigator()?.createSelectManufacturerDialog()

    fun onCharacteristicsClick() {
        displaySetting()
//        getNavigator()?.onBoatCharacteristicsClick()
    }

    fun onAddLocationClick(){
        displayGoogleMap()
    }


    fun displaySetting() {
        navigateToBoatCharacteristics.value = true
    }

    fun displaySettingComplete() {
        navigateToBoatCharacteristics.value = false
    }


    fun displayGoogleMap() {
        navigateToGoogleMap.value = true
    }

    fun displayGoogleMapComplete() {
        navigateToGoogleMap.value = false
    }


    fun openFileChooser() = getNavigator()?.openFileChooser()
    fun uploadFile() = getNavigator()?.uploadFile()


}