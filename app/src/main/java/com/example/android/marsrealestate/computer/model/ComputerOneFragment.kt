package com.example.android.marsrealestate.computer.model

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentComputerDetailBinding

/**
 * Класс ComputerOneFragment
 * @author Ilya Osipov (mailto:il.osipov.gm@gmail.com)
 * @since 27.04.2020
 * @version $Id$
 */

class ComputerOneFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?) : View? {
        val application = requireNotNull(activity).application
        val binding = FragmentComputerDetailBinding.inflate(inflater)
        binding.lifecycleOwner = this

        val computerProperty = ComputerOneFragmentArgs.fromBundle(arguments!!).selectedComputerProperty
        val viewModelFactory = ComputerOneViewModelFactory(computerProperty, application)
        binding.viewModel = ViewModelProviders.of(
                this, viewModelFactory).get(ComputerOneViewModel::class.java)

        return binding.root
    }
}