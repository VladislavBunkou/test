package com.example.android.marsrealestate.repository.trailer

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.enums.Status
import com.example.android.marsrealestate.trailer.model.Trailer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch


class  TrailerRepository(val coroutineScope: CoroutineScope) {

    val trailers = MutableLiveData<List<Trailer>>()
    val saveTrailerObservable = MutableLiveData<TrailerSaveInfo>()
    val receiveTrailerObservable = MutableLiveData<TrailerGetInfo>() // replace to Repository
    val idTrailer = MutableLiveData<Long>()
    val modelTrailerObservable = MutableLiveData<String>()
    val brandTrailerObservable = MutableLiveData<String>()
    val countryTrailerObservable = MutableLiveData<String>()




    fun getTrailerList() {
        receiveTrailerObservable.postValue(TrailerGetInfo(Status.LOADING))
        Log.d("TrailerRepository", "dm->  1")
        coroutineScope.launch {
            Log.d("TrailerRepository", "dm-> 2")
            var getPropertiesDeferred = RetrofitApi.retrofitApiService.getTrailerList()

            try {
//                _status.value = PhoneApiStatus.LOADING
                var listResult = getPropertiesDeferred.await()
//                _status.value = PhoneApiStatus.DONE
                trailers.value = listResult
                Log.d("TrailerRepository", "dm-> 11 / trailers.value = ${trailers.value?.size}" )
                receiveTrailerObservable.postValue(TrailerGetInfo(Status.SUCCESS))
//                receivePhoneObservable.postValue(PhoneGetInfo(Status.ERROR)
            } catch (e: Exception) {
//                _status.value = PhoneApiStatus.ERROR
                Log.d("TrailerRepository", "dm-> Internet connection not exist or Server is shot down")
                trailers.value = ArrayList()
                receiveTrailerObservable.postValue(TrailerGetInfo(Status.ERROR_CONNECTION))
            }

        }
    }


    fun receiveTrailerById(id: Long, isSaveNewTrailerOnServer: Boolean) {
        receiveTrailerObservable.postValue(TrailerGetInfo(Status.LOADING)) // change

        Log.d("TrailerRepository", "HEAR before idTrailer.postValue($id)")

        if (isSaveNewTrailerOnServer) {
            receiveTrailerObservable.postValue(TrailerGetInfo(Status.SUCCESS))
            Log.d("TrailerRepository", "HEARa getMarsRealEstateProperties / id = ${id} - means new Trailer")
        } else
        {
            Log.d("TrailerRespository", "check0")
            coroutineScope.launch {
                idTrailer.postValue(id)
                Log.d("TrailerRepository", "in Coroutine dm->  getMarsRealEstateProperties / id = ${id}")
                //            var getPropertiesDeferred = MarsApi.retrofitService.getProperties(filter.value)
                var getPropertiesDeferred = RetrofitApi.retrofitApiService.getTrailerOne(id)
                Log.d("TrailerRepository", "in Coroutine after getProperties dm->  getMarsRealEstateProperties / id = ${id}")

                try {
//                _status.value = MarsApiStatus.LOADING
                    // this will run on a thread managed by Retrofit
                    Log.d("TrailerRespository", "check1")
                    val trailerOne = getPropertiesDeferred.await()
                    Log.d("TrailerRespository", "in Coroutine 2 dm-> try trailer = ${trailerOne.toString()}")
                    modelTrailerObservable.postValue(trailerOne.model)
                    brandTrailerObservable.postValue(trailerOne.manufacturer)
                    countryTrailerObservable.postValue(trailerOne.manufacturerCountry)
                    receiveTrailerObservable.postValue(TrailerGetInfo(Status.SUCCESS))
                } catch (e: Exception) {
                    Log.d("TrailerRepository", "dm-> catch $e")
                    receiveTrailerObservable.postValue(TrailerGetInfo(Status.ERROR_CONNECTION))
                }
                }
             }
    }

}