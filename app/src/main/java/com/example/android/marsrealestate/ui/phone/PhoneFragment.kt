package com.example.android.marsrealestate.ui.phone

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentPhone2Binding
import com.example.android.marsrealestate.di.mv.phone.PhoneDetailViewModel
import com.example.android.marsrealestate.di.mv.phone.PhoneDetailViewModelFactory


class PhoneFragment : Fragment() {

//    companion object { // Ex2
//        const val PHONE_ID = "phone_id"
//    }
//
//    private var selectedPhoneId: Int? = null // Ex2
//
//    override fun onCreate(savedInstanceState: Bundle?) { // Ex2
//        super.onCreate(savedInstanceState)
//        selectedPhoneId = arguments?.getInt(PHONE_ID)
////        Log.d(TAG, "dm::shop  selectedOfferId = $selectedOfferId")
//    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val application = requireNotNull(activity).application // Ex1
        val binding = FragmentPhone2Binding.inflate(inflater)
        binding.lifecycleOwner = this
        val phone = PhoneFragmentArgs.fromBundle(arguments!!).selectedPhone // // Ex1
        val viewModelFactory = PhoneDetailViewModelFactory(phone, application) // Ex1
        binding.viewModel = ViewModelProviders.of(this, viewModelFactory).get(PhoneDetailViewModel::class.java)
        return binding.root
    }
}