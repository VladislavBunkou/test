package com.example.android.marsrealestate.bicycle.addNewBicycle

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class BicycleCharacteristicViewModel: ViewModel() {
    val foldingSelected = MutableLiveData<Boolean>(false)
    val mountainSelected = MutableLiveData<Boolean>(false)
    val roadSelected = MutableLiveData<Boolean>(false)
    val citybikeSelected = MutableLiveData<Boolean>(false)
    val hibridSelected = MutableLiveData<Boolean>(false)

    val productIsNewSelected = MutableLiveData<Boolean>(false)
}