package com.example.android.marsrealestate.shoes

interface ShoesItemListener {
    fun onSubUserItemClick(s: Shoes)
}