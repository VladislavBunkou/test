package com.example.android.marsrealestate.shoes.addShoes

import android.app.Activity
import android.content.ContentResolver
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentAddShoesBinding
import com.example.android.marsrealestate.di.mv.nav.NavViewModel
import com.example.android.marsrealestate.ui.authentification.LoginFragment.Companion.TAG
import com.example.android.marsrealestate.ui.dress.d_latitude
import com.example.android.marsrealestate.ui.dress.d_longitude
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
class AddShoesFragment : Fragment(), ShoesNavigator{

    lateinit var binding: FragmentAddShoesBinding

    private val viewModel: AddShoesViewModel by lazy {
       ViewModelProviders.of(this).get(AddShoesViewModel::class.java)
   }

    //LOCATION
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private lateinit var shoesInfoViewModel: ShoesInfoViewModel
    lateinit var navViewModel: NavViewModel

    //For upload image
    lateinit var storage: FirebaseStorage
    lateinit var storageRef: StorageReference

    lateinit var imageView: ImageView
    lateinit var textResult: TextView

    private var imageUri: Uri = Uri.EMPTY // for saving image
    private var uploadedImageUri: Uri = Uri.EMPTY // for saving image
    private var fileIsSelected = false // true when we choose image

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val application = requireNotNull(activity).application

        binding = FragmentAddShoesBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        viewModel.setNavigator(this)


        //LOCATION
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.requireActivity())
        //getLastLocation()

        this.fusedLocationClient.lastLocation
                .addOnSuccessListener { location->
                    if (location != null) {
                        d_latitude =  location.latitude
                        d_longitude = location.longitude
                        Log.d(TAG,"AddShoesFragment: km->Location  latitude=$d_latitude, longitude=$d_longitude")
                    }
                    else if (location == null){
                        Log.d(TAG,"AddShoesFragment: km->Location IS NULL latitude=$d_latitude, longitude=$d_longitude")
                    }

                }


        imageView = binding.imageView
        textResult = binding.textViewShowUploads
        storage = FirebaseStorage.getInstance()
        storageRef = storage.reference

        activity?.let {
            shoesInfoViewModel = ViewModelProviders.of(it).get(ShoesInfoViewModel::class.java)
            navViewModel = ViewModelProviders.of(it).get(NavViewModel::class.java)
            Log.d("AddShoesFragment", "dm->  navViewModel.id.value = ${navViewModel.id.value}}")
        }
        navViewModel.id.observe(this, Observer {
            if (navViewModel.isCreateNew.value == true)
                viewModel.receiveShoesById(navViewModel.id.value ?: 1, true)

            else  {
                viewModel.receiveShoesById(navViewModel.id.value ?: 1, false)
                navViewModel.isCreateNew.postValue(true)
            }
            Log.d("AddShoesFragment", "navViewModel.id.value = ${navViewModel.id.value}}")
        })
        viewModel.navigateToShoesCharacteristics.observe(this, Observer {
            if (it) {
                this.findNavController().navigate(
                        AddShoesFragmentDirections.actionAddShoesFragmentToShoesCharateristicFragment())
                viewModel.displayCharacteristicsComplete()
            }
        })

        binding.buttonChooseFile.setOnClickListener {
            preOpenFileChooserWithPermissionCheck() // // step 3 -add this fun (it is genegated!!)
            //odtieneLocationWithPermissionCheck()
        }

        return binding.root
    }

    @NeedsPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            /*android.Manifest.permission.LOCATION_HARDWARE*/)
    fun preOpenFileChooser(){
        openFileChooser()
    }

    // step 4
    @OnPermissionDenied(android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            /*android.Manifest.permission.LOCATION_HARDWARE*/)
    fun permissionDenied() {
        Log.d("AddDressFragment", "dm:: OnPermissionDenied")
    }
    // step 4
    @OnNeverAskAgain(android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            /*android.Manifest.permission.LOCATION_HARDWARE*/)
    fun neverAskAgain() {
        Log.d("AddDressFragment", "dm:: OnNeverAskAgain")
    }



    override fun showToast(model: String) {
        Toast.makeText(context, "model: $model", Toast.LENGTH_LONG).show()
    }

    override fun errorMassages(isModelFilled: Boolean, isBrandFilled: Boolean,
                               isManufacturerCountryFilled: Boolean, isCityFilled: Boolean,
                               isPriceFilled: Boolean, isSellerFilled: Boolean,
                               isProductConditionFilled: Boolean, isSexFilled: Boolean,
                               isTitleFilled: Boolean, isColorFilled: Boolean) {
        if (!isModelFilled) {
            binding.shoesModelLayout.isErrorEnabled = true
            binding.shoesModelLayout.error = "Enter model"
        } else {
            binding.shoesModelLayout.isErrorEnabled = false
        }

        if (!isBrandFilled) {
            binding.shoesManufacturerLayout.isErrorEnabled = true
            binding.shoesManufacturerLayout.error = "Enter manufacturer"
        } else {
            binding.shoesManufacturerLayout.isErrorEnabled = false
        }

        if (!isManufacturerCountryFilled) {
            binding.shoesManufacturerCountryLayout.isErrorEnabled = true
            binding.shoesManufacturerCountryLayout.error = "Enter country"
        } else {
            binding.shoesManufacturerCountryLayout.isErrorEnabled = false
        }

        if (!isCityFilled) {
            binding.shoesCityLayout.isErrorEnabled = true
            binding.shoesCityLayout.error = "Enter city"
        } else {
            binding.shoesCityLayout.isErrorEnabled = false
        }

        if (!isPriceFilled) {
            binding.shoesPriceLayout.isErrorEnabled = true
            binding.shoesPriceLayout.error = "Enter price"
        } else {
            binding.shoesPriceLayout.isErrorEnabled = false
        }

        if (!isSellerFilled) {
            binding.shoesSellerLayout.isErrorEnabled = true
            binding.shoesSellerLayout.error = "Enter seller"
        } else {
            binding.shoesSellerLayout.isErrorEnabled = false
        }

        if (!isProductConditionFilled) {
            binding.shoesProductConditionLayout.isErrorEnabled = true
            binding.shoesProductConditionLayout.error = "Enter condition"
        } else {
            binding.shoesProductConditionLayout.isErrorEnabled = false
        }

        if (!isSexFilled) {
            binding.shoesSexLayout.isErrorEnabled = true
            binding.shoesSexLayout.error = "Enter Width"
        } else {
            binding.shoesSexLayout.isErrorEnabled = false
        }

        if (!isTitleFilled) {
            binding.shoesWordsToSearchLayout.isErrorEnabled = true
            binding.shoesWordsToSearchLayout.error = "Enter Weight"
        } else {
            binding.shoesWordsToSearchLayout.isErrorEnabled = false
        }

        if (!isColorFilled) {
            binding.shoesColorLayout.isErrorEnabled = true
            binding.shoesColorLayout.error = "Enter Length"
        } else {
            binding.shoesColorLayout.isErrorEnabled = false
        }
    }

    override fun getWinter(): ObservableField<Boolean> = shoesInfoViewModel.winter

    override fun getSpring(): ObservableField<Boolean> = shoesInfoViewModel.spring

    override fun getFall(): ObservableField<Boolean> = shoesInfoViewModel.fall

    override fun getSummer(): ObservableField<Boolean> = shoesInfoViewModel.summer

    override fun getDemi(): ObservableField<Boolean> = shoesInfoViewModel.demi

    override fun getProductIsNew(): ObservableField<Boolean> = shoesInfoViewModel.productIsNew

    private var currentSelectedSizeIndex = -1

    override fun createSelectSizeDialog() {
        val listCategory = listOf("36", "37", "38", "39", "40", "41","42","43","Other")

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setSingleChoiceItems(listCategory.toTypedArray(),
                        currentSelectedSizeIndex) { dialog, which ->
                    currentSelectedSizeIndex = which
                    viewModel.updateSize(which + 1)
                    dialog.dismiss()
                }
                setTitle("Shoes size")
                setNegativeButton("Cancel") { _, _ ->
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }


    override fun openFileChooser() {
        textResult.text = "_"

        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, viewModel.PICK_IMAGE_REQUEST)
        fileIsSelected = true
    }

    override fun uploadFile() {
        if (imageUri != null) {
            val fileReference: StorageReference = storageRef.child(

                    "image/shoes/${System.currentTimeMillis()}.${getFileExtension(imageUri)}"
            )

            val uploadTask = fileReference.putFile(imageUri)
                    .addOnCompleteListener() {
                        val handler = Handler()
                        handler.postDelayed({ binding.progressBar.progress = 0 }, 500)
                        Toast.makeText(context, "Upload successful", Toast.LENGTH_LONG).show()

                        Picasso.get().load(R.drawable.ic_image_is_uploded).into(imageView)

                        textResult.text = "OK!"
                        textResult.setTextColor(Color.GREEN)
                    }

                    .addOnFailureListener { e ->
                        Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
                    }

                    .addOnProgressListener {
                        val progress = 100.0 * it.bytesTransferred / it.totalByteCount
                        binding.progressBar.setProgress(progress.toInt())
                    }

            val urlTask = uploadTask.continueWithTask {
                        if (!it.isSuccessful) {
                            it.exception?.let { ex ->
                                throw ex
                            }
                        }
                        fileReference.downloadUrl
                    }
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            uploadedImageUri = it.result ?: Uri.EMPTY
                            Toast.makeText(context, "Url: $uploadedImageUri", Toast.LENGTH_SHORT).show()
                            Log.d("km->", "Url: $uploadedImageUri")
                            textResult.text = "1) Image is uploaded!"
                        } else {
                            // Handle failures
                            Toast.makeText(context, "something wrong!!!", Toast.LENGTH_SHORT).show()
                        }
                    }

        } else {
            Toast.makeText(context, "No file selected", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getPhotoId() = uploadedImageUri.toString()

    private fun getFileExtension(uri: Uri): String? {
        val cR: ContentResolver = activity!!.contentResolver
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(cR.getType(uri))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == viewModel.PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK
                && data?.data != null) {
            imageUri = data.data!!
            Picasso.get().load(imageUri).into(imageView)
        }
    }
}