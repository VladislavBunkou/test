package com.example.android.marsrealestate.tablet.addtablet

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentTabletSettingsBinding

class TabletSettingsFragment : Fragment() {

    private val TAG : String = "TabletSettingsFragment"

    lateinit var binding: FragmentTabletSettingsBinding

    private val viewModel: TabletSettingsViewModel by lazy {
        ViewModelProviders.of(this).get(TabletSettingsViewModel::class.java)
    }

    // for saving data
    private lateinit var tabletInfoViewModel: TabletInfoViewModel



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val application = requireNotNull(activity).application

        binding = FragmentTabletSettingsBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel


        activity?.let {
            tabletInfoViewModel = ViewModelProviders.of(it).get(TabletInfoViewModel::class.java)

            viewModel.isSimSelected.value = tabletInfoViewModel.isTabletHasSIM.get()
            viewModel.isNewSelected.value = tabletInfoViewModel.isNew.get()
            viewModel.yearSelected.value = tabletInfoViewModel.year.get()
            viewModel.sellerSelected.value = tabletInfoViewModel.seller.get()

            viewModel.isSimSelected.observe(viewLifecycleOwner, Observer {
                tabletInfoViewModel.isTabletHasSIM.set(it)
            })

            viewModel.isNewSelected.observe(viewLifecycleOwner, Observer {
                tabletInfoViewModel.isNew.set(it)
            })

               viewModel.sellerSelected.observe(viewLifecycleOwner, Observer {
                   tabletInfoViewModel.seller.set(it)
               })

               viewModel.yearSelected.observe(viewLifecycleOwner, Observer {
                   tabletInfoViewModel.year.set(it)
               })
        }



        return binding.root
    }

}