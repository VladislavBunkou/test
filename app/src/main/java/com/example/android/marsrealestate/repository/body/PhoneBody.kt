package com.example.android.marsrealestate.repository.body

import com.google.gson.annotations.SerializedName

data class PhoneBody(
        var userId: String = "",
        var model: String = "",
        var manufacturer: String = "",
        var country: String = "",
        var has3g: Boolean = false,
        var has4g: Boolean = false,
        var url: String = "https://cdn.mos.cms.futurecdn.net/J8DirbjgmENqft2LGkuMBZ-650-80.jpg",
        var typeOfProduct: String = "phone"
)
