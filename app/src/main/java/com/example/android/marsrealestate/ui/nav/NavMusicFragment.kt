package com.example.android.marsrealestate.ui.nav

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.databinding.FragmentNavMusicBinding

class NavMusicFragment : Fragment(), NavMusicListener {
    private val TAG = "NavMusicFragment"


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentNavMusicBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.navMusicListener  = this
        return binding.root
    }

    override fun onClickGuitars() {
        this.findNavController().navigate(NavMusicFragmentDirections.actionNavMusicToGuitars())
    }
}


interface NavMusicListener {
    fun onClickGuitars()
}