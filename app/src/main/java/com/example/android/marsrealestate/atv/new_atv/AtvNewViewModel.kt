package com.example.android.marsrealestate.atv.new_atv

import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.atv.AtvBody
import com.example.android.marsrealestate.atv.new_atv.atw_enum.AtvManufacturerCountryEnum
import com.example.android.marsrealestate.atv.new_atv.atw_enum.AtvManufacturerEnum
import com.example.android.marsrealestate.atv.new_atv.atw_enum.AtvTypeOfProductEnum
import com.example.android.marsrealestate.di.mv.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.*

class AtvNewViewModel: BaseViewModel<AtvNewNavigator>() {

    var model = ObservableField("")
    var productIsNew = ObservableBoolean()
    var typeOfProduct = ObservableField("")
    var manufacturer = ObservableField("")
    var manufacturerCountry = ObservableField("")
    var description = ObservableField("")
    var mileage = ObservableField("")
    var firstRegistration = ObservableField("")
    var cubicCapacity = ObservableField("")
    var powerSupply = ObservableField("")
    var gearBox = ObservableField("")

    private var typeOfProductEnum: AtvTypeOfProductEnum? = null
    private var manufacturerEnum: AtvManufacturerEnum? = null
    private var manufacturerCountryEnum: AtvManufacturerCountryEnum? = null

    //val id: Long
    //val userId: Long
    //val city: String,
    //val price: Int,
    //val category: String,
    //val seller: String,
    //val productCondition: String

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    var PICK_IMAGE_REQUEST: Int = 1

    fun onSendButton() {
        if (allFieldsGood()) {
            val imageId = getNavigator()?.getPhotoId() ?: ""

            val currentAtv = AtvBody(
                    model = model.get()!!,
                    typeOfProduct = typeOfProductEnum!!.get(),
                    manufacturer = manufacturerEnum!!.get(),
                    manufacturerCountry = manufacturerCountryEnum!!.get(),
                    productIsNew = productIsNew.get(),
                    description = description.get()!!,
                    url = imageId,
                    mileage = mileage.get()!!.toInt(),
                    firstRegistration = firstRegistration.get()!!,
                    cubicCapacity = cubicCapacity.get()!!.toInt(),
                    powerSupply = powerSupply.get()!!,
                    gearBox = gearBox.get()!!
            )

            getNavigator()?.showToast(currentAtv)  // for easier understanding this code can be deleted

            saveAtv(currentAtv)

            model.set("")
            typeOfProductEnum = null
            manufacturerEnum = null
            manufacturerCountryEnum = null
            typeOfProduct.set("")
            manufacturer.set("")
            manufacturerCountry.set("")
            productIsNew.set(false)
            description.set("")
            mileage.set("")
            firstRegistration.set("")
            cubicCapacity.set("")
            powerSupply.set("")
            gearBox.set("")
        }
    }

    private fun saveAtv(atv: AtvBody) {
        coroutineScope.launch {
            RetrofitApi.retrofitApiService
                    .addAtv(atv).enqueue(object : Callback<AtvBody>  {
                        override fun onResponse(call: Call<AtvBody>, response: Response<AtvBody>) {
                            Log.d("AtvNewViewModel", "ATV added successfully")
                        }

                        override fun onFailure(call: Call<AtvBody>, t: Throwable) {
                            Log.d("AtvNewViewModel", "Error occurred while adding ATV")
                        }
                    })
        }
    }

    private fun allFieldsGood(): Boolean {
        var isModelFilled = false
        var isManufacturerFilled = false
        var isManufacturerCountryFilled = false
        var isTypeOfProductFilled = false
        var isDescriptionFilled = false
        var isMileageFilled = false
        var isFirstRegistrationFilled = false
        var isCubicCapacityFilled = false
        var isPowerSupplyFilled = false
        var isGearBoxFilled = false
        var isUriFilled = false

        if (!model.get().isNullOrEmpty()) isModelFilled = true
        if (!manufacturer.get().isNullOrEmpty()) isManufacturerFilled = true
        if (!manufacturerCountry.get().isNullOrEmpty()) isManufacturerCountryFilled = true
        if (!typeOfProduct.get().isNullOrEmpty()) isTypeOfProductFilled = true
        if (!description.get().isNullOrEmpty()) isDescriptionFilled = true
        if (!mileage.get().isNullOrEmpty()) isMileageFilled = true
        if (!firstRegistration.get().isNullOrEmpty()) isFirstRegistrationFilled = true
        if (!cubicCapacity.get().isNullOrEmpty()) isCubicCapacityFilled = true
        if (!powerSupply.get().isNullOrEmpty()) isPowerSupplyFilled = true
        if (!gearBox.get().isNullOrEmpty()) isGearBoxFilled = true
        if (!getNavigator()?.getPhotoId().isNullOrEmpty()) isUriFilled = true

        return if (isModelFilled && isManufacturerFilled &&
                isManufacturerCountryFilled && isTypeOfProductFilled &&
                isDescriptionFilled && isMileageFilled &&
                isFirstRegistrationFilled && isCubicCapacityFilled &&
                isPowerSupplyFilled && isGearBoxFilled && isUriFilled) {
            true
        } else {
            getNavigator()?.showErrors(isModelFilled, isManufacturerFilled,
                    isManufacturerCountryFilled, isTypeOfProductFilled,
                    isDescriptionFilled, isMileageFilled,
                    isFirstRegistrationFilled, isCubicCapacityFilled,
                    isPowerSupplyFilled, isGearBoxFilled, isUriFilled)
            false
        }
    }

    fun updateTypeOfProduct(selectedIndex: Int){
        typeOfProductEnum = when (selectedIndex.toString()){
            AtvTypeOfProductEnum.CHILDREN.id -> AtvTypeOfProductEnum.CHILDREN
            AtvTypeOfProductEnum.RHINO.id -> AtvTypeOfProductEnum.RHINO
            AtvTypeOfProductEnum.SPORT.id -> AtvTypeOfProductEnum.SPORT
            else -> AtvTypeOfProductEnum.UTILITY
        }
        typeOfProduct.set(typeOfProductEnum!!.get())
    }

    fun updateManufacturer(selectedIndex: Int){
        manufacturerEnum = when (selectedIndex.toString()){
            AtvManufacturerEnum.ARCTIC_CAT.id -> AtvManufacturerEnum.ARCTIC_CAT
            AtvManufacturerEnum.HONDA.id -> AtvManufacturerEnum.HONDA
            AtvManufacturerEnum.KAWASAKI.id -> AtvManufacturerEnum.KAWASAKI
            AtvManufacturerEnum.POLARIS.id -> AtvManufacturerEnum.POLARIS
            AtvManufacturerEnum.SUZUKI.id -> AtvManufacturerEnum.SUZUKI
            AtvManufacturerEnum.YAMAHA.id -> AtvManufacturerEnum.YAMAHA
            else -> AtvManufacturerEnum.OTHER
        }
        manufacturer.set(manufacturerEnum!!.get())
    }

    fun updateManufacturerCountry(selectedIndex: Int){
        manufacturerCountryEnum = when(selectedIndex.toString()){
            AtvManufacturerCountryEnum.JAPAN.id -> AtvManufacturerCountryEnum.JAPAN
            AtvManufacturerCountryEnum.CHINA.id -> AtvManufacturerCountryEnum.CHINA
            AtvManufacturerCountryEnum.INDONESIA.id -> AtvManufacturerCountryEnum.INDONESIA
            AtvManufacturerCountryEnum.MALAYSIA.id -> AtvManufacturerCountryEnum.MALAYSIA
            else -> AtvManufacturerCountryEnum.OTHER
        }
        manufacturerCountry.set(manufacturerCountryEnum!!.get())
    }

    fun onManufacturerClick() = getNavigator()?.createSelectManufacturerDialog()
    fun onManufacturerCountryClick() = getNavigator()?.createSelectManufacturerCountryDialog()
    fun onTypeOfProductClick() = getNavigator()?.createSelectTypeOfProductDialog()
    fun openFileChooser() = getNavigator()?.openFileChooser()
    fun uploadFile() = getNavigator()?.uploadFile()
    //fun onProductIsNewSwitchClick() = getNavigator()?.onProductIsNewSwitchClick()


}