package com.example.android.marsrealestate.di.mv.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.model.Prod
import kotlinx.coroutines.*

enum class MainApiStatus { LOADING, ERROR, DONE }


class MainViewModel : ViewModel() {
    private val TAG = "OverviewViewModel"
    val status = MutableLiveData<MainApiStatus>()
    val properties = MutableLiveData<List<Prod>>()
    val categories = MutableLiveData<List<String>>()
    val navigateToSelectedProperty = MutableLiveData<Prod>()


    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)


    init {
        getListProducts()
        categories.value = listOf("Auto&\nBoats", "Electric", "Dress&\nShoes", "Houses", "Music\ninstruments")
    }


    fun getListProducts(str: String = "") {
        coroutineScope.launch {
            var getPropertiesDeferred = if (str.isEmpty()) RetrofitApi.retrofitApiService.getListProd()
            else RetrofitApi.retrofitApiService.getListProd(str)
            try {
                status.value = MainApiStatus.LOADING
                val listResult = getPropertiesDeferred.await()
                status.value = MainApiStatus.DONE
                properties.value = listResult
            } catch (e: Exception) {
                status.value = MainApiStatus.ERROR
                properties.value = listOf()
            }
        }
    }


    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}
