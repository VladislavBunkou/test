package com.example.android.marsrealestate.tablet.addtablet

import androidx.databinding.ObservableField

interface TabletNavigator{
    fun showToast(model: String, country: String)
    fun errorMessages(isModelFilled: Boolean, isCountryFilled: Boolean, isManufacturerFilled: Boolean, isProductionYearFilled: Boolean, isScreenDiagonalFilled: Boolean, isScreenResolutionFilled: Boolean, isRamFilled: Boolean, isCameraResolutionFilled: Boolean, isSellerFilled: Boolean)

    fun getSIM(): ObservableField<Boolean>
    fun getIsNew(): ObservableField<Boolean>
     fun getSeller(): ObservableField<String>
     fun getYear(): ObservableField<String>

    fun createSelectManufacturerDialog()
}