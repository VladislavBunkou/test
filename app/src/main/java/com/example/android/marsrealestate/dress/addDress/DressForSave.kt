package com.example.android.marsrealestate.dress.addDress

class DressForSave (
    //val id: Long,
    var innerId: String = "",
    var userId: String= "",
    var model: String= "",
    var article: String= "",
    var sleeve: String= "",
    var fabric: String= "",
    var silhouette: String= "",
    var size: String= "",
    var height: String= "",
    var typeOfProduct: String= "dress",
    var typeOfCollection: String= "",
    var manufacturer: String= "",
    var manufacturerCountry: String= "",

    var productIsNew: Boolean= false,

    var description: String= "",
    var url: String= "",

    var oneColor: Boolean = false,
    var multiColor: Boolean = false,
    var decoration: Boolean = false,
    var print: Boolean = false,


    var latitude: Double = -1.0,
    var longitude: Double = -1.0,

    var price: String="",
    var category: String = "",
    var seller: String = "",
    var productCondition: String = "",
    var wordsToSearch: String = "",
    var sex: String = "",
    var color: String = "",
    var city: String = ""

)