package com.example.android.marsrealestate.di.mv

import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel
import java.lang.ref.WeakReference

open class BaseViewModel<N> : ViewModel() {
    private val mIsLoading = ObservableBoolean(false)
    private var mNavigator: WeakReference<N>? = null

    fun getIsLoading(): ObservableBoolean = mIsLoading
    fun getNavigator(): N? = mNavigator?.get()
    fun setIsLoading(isLoading: Boolean) =  mIsLoading.set(isLoading)

    fun setNavigator(navigator: N) {
        clearNavigator()
        mNavigator = WeakReference<N>(navigator)
    }

    private fun clearNavigator() = mNavigator?.clear()

    override fun onCleared() {
        super.onCleared()
        clearNavigator()
    }
}