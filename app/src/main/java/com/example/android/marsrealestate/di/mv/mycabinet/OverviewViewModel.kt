
package com.example.android.marsrealestate.di.mv.mycabinet

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.model.Prod
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.*

enum class MarsApiStatus { LOADING, ERROR, DONE }


class OverviewViewModel : ViewModel() {

    private val TAG = "OverviewViewModel"

    private val _status = MutableLiveData<MarsApiStatus>()
    val status: LiveData<MarsApiStatus>
        get() = _status

    //    val properties: LiveData<List<MarsProperty>> get() = _properties
//    private val _properties = MutableLiveData<List<MarsProperty>>()
    val properties: LiveData<List<Prod>> get() = _properties
    private val _properties = MutableLiveData<List<Prod>>()

    private val _navigateToSelectedProperty = MutableLiveData<Prod>()
    val navigateToSelectedProperty: LiveData<Prod>
        get() = _navigateToSelectedProperty


    val navigateToAutosAndBoats = MutableLiveData<Boolean>(false)
    val navigateToElectronik = MutableLiveData<Boolean>(false)
    val navigateToClothes = MutableLiveData<Boolean>(false)


    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)


    init {
        getMarsRealEstateProperties( )
    }


    private fun getMarsRealEstateProperties( ) {
        val userId = FirebaseAuth.getInstance().currentUser?.uid ?: ""
        Log.d(TAG, "dm-> userId = $userId")
        coroutineScope.launch {
            var getPropertiesDeferred = RetrofitApi.retrofitApiService.getListProdById(userId)

            try {
                _status.value = MarsApiStatus.LOADING
                // this will run on a thread managed by Retrofit
                val listResult = getPropertiesDeferred.await()
                _status.value = MarsApiStatus.DONE
                _properties.value = listResult
            } catch (e: Exception) {
                _status.value = MarsApiStatus.ERROR
                _properties.value = ArrayList()
            }
        }
    }


    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

//    fun updateFilter(filter: MarsApiFilter) = getMarsRealEstateProperties(filter)

    fun displayPropertyDetails(prod: Prod) {
        _navigateToSelectedProperty.value = prod
    }


    fun displayPropertyDetailsComplete() {
        _navigateToSelectedProperty.value = null
    }


    fun onClickCreatePhone() {

        Log.d(TAG, "dm-> onAutosClick")

    }


    // Auto
    fun onAutosClick() {
        Log.d(TAG, "dm-> onAutosClick")
        navigateToAutosAndBoats.value = true
    }

    fun navigateToAutoComplited() {
        navigateToAutosAndBoats.value = false
    }


    // onElectronikClick
    fun onElectronikClick() {
        navigateToElectronik.value = true
    }

    fun navigateToElectronikComplited() {
        navigateToElectronik.value = false
    }

    //onClothesClick
    fun onClothesClick() {
        navigateToClothes.value = true
    }

    fun navigateToClothesComplited() {
        navigateToClothes.value = false
    }
}
