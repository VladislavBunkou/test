package com.example.android.marsrealestate.tablet

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.TabletItemBinding
import com.example.android.marsrealestate.tablet.model.Tablet

class TabletRecyclerAdapter (private val onClickListenerTablet: OnClickListenerTablet) : ListAdapter<Tablet, TabletRecyclerAdapter.TabletPropertyviewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TabletPropertyviewHolder {
        return TabletPropertyviewHolder(TabletItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: TabletPropertyviewHolder, position: Int) {
        val tabletProperty = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListenerTablet.onClick(tabletProperty)
        }
        holder.bind(tabletProperty)
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Tablet>() {
        override fun areItemsTheSame(oldItem: Tablet, newItem: Tablet): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Tablet, newItem: Tablet): Boolean {
            return oldItem.id == newItem.id
        }
    }

    class TabletPropertyviewHolder(private var binding: TabletItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(tablet: Tablet) {
            binding.property = tablet
            binding.executePendingBindings()
        }

    }

    class OnClickListenerTablet(val clickListener: (tablet: Tablet) -> Unit) {
        fun onClick(tablet: Tablet) = clickListener(tablet)
    }
}