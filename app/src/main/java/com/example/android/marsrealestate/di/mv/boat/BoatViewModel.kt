package com.example.android.marsrealestate.di.mv.boat

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.android.marsrealestate.boat.model.Boat
import com.example.android.marsrealestate.di.mv.BaseViewModel
import com.example.android.marsrealestate.dress.DressRepository
import com.example.android.marsrealestate.enums.Status
import com.example.android.marsrealestate.repository.boat.BoatGetInfo
import com.example.android.marsrealestate.repository.boat.BoatRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

enum class BoatApiStatus {
    LOADING, ERROR, DONE
}

class BoatViewModel : BaseViewModel<Any>() {

    val boatsListObservable = ObservableArrayList<Boat>()

    val status: LiveData<BoatApiStatus>
        get() = _status
    private val _status = MutableLiveData<BoatApiStatus>()

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    lateinit var boatRepository: BoatRepository // Added


    val dress: LiveData<List<Boat>> get() = _boat
    private val _boat = MutableLiveData<List<Boat>>()

    private val allBoatObserver = Observer<List<Boat>> {list ->
        list?.let {
            boatsListObservable.run {
                Log.d("BoatViewModel", "dm-> 22 this.size = ${this.size}")
                clear()
                addAll(list)
            }
        }
    }

    private val receiveBoatObserver = Observer<BoatGetInfo> {
        when (it.status) {
            Status.INITIAL, Status.LOADING -> {
                setIsLoading(true)
                Log.d("BoatNewViewModel", "dm-> Status.Loading")
            }
            Status.SUCCESS -> {
                setIsLoading(false)
                Log.d("BoatNewViewModel", "dm-> Status.SUCCESS")
            }
            Status.ERROR -> {
                setIsLoading(false)
                Log.d("BoatNewViewModel", "dm-> Status.Error")
            }
            Status.ERROR_CONNECTION -> {
                setIsLoading(false)
            }
            else -> {
            }
        }
    }


    init {


        Log.d("BoatViewModel", "dm-> 2")
        boatRepository = BoatRepository(coroutineScope)
        boatRepository.boats.observeForever(allBoatObserver)
        boatRepository.receiveBoatObservable.observeForever(receiveBoatObserver)
        boatRepository.getBoatsList()
//        getPhoneProperties()
    }



    fun testRefresh(){
        Log.d("BoatViewModel", "dm-> ttt REFRESH")
    }


    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel() // !! Important
    }

    fun getBoatList(str: String) { // Added
        boatRepository.getBoatsList(str)
    }

}