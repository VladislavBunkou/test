package com.example.android.marsrealestate.trailer.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class Trailer(
        val id: Long,
        val model: String,
        val typeOfProduct: String,
        val manufacturer: String,
        val manufacturerCountry: String,
        val url: String) : Parcelable


@Parcelize
class TrailerOne(
        //val id: Long,
        val model: String,
        val typeOfProduct: String,
        val manufacturer: String,
        val manufacturerCountry: String,
        val url: String
) : Parcelable