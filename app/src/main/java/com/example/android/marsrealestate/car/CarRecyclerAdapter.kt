package   com.example.android.marsrealestate.car

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.GridViewItemCarBinding


class CarRecyclerAdapter(private val onClickListener: OnClickListener) :
    ListAdapter<Car, CarRecyclerAdapter.CarViewHolder>(DiffCallback) {

    class CarViewHolder(private var binding: GridViewItemCarBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(message: Car) {
            binding.property = message

            binding.executePendingBindings()
        }

    }

    companion object DiffCallback : DiffUtil.ItemCallback<Car>() {

        override fun areItemsTheSame(oldItem: Car, newItem: Car): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Car, newItem: Car): Boolean {

            return oldItem.id == newItem.id
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CarViewHolder {
        return CarViewHolder(GridViewItemCarBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        val message = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(message)
        }
        holder.bind(message)
    }


    class OnClickListener(val clickListener: (message: Car) -> Unit) {
        fun onClick(message: Car) = clickListener(message)
    }
}

