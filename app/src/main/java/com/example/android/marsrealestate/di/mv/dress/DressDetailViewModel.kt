package com.example.android.marsrealestate.di.mv.dress

import android.app.Application
import androidx.lifecycle.*
import com.example.android.marsrealestate.dress.Dress


class DressDetailViewModel(dress: Dress, app: Application) : AndroidViewModel(app) {

    private val _selectedProperty = MutableLiveData<Dress>()
    val selectedProperty: LiveData<Dress>
        get() = _selectedProperty

    init {
        _selectedProperty.value = dress
    }

}

class DressDetailViewModelFactory(private val grilling: Dress, private val application: Application) :
        ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DressDetailViewModel::class.java)) {
            return DressDetailViewModel(grilling, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}