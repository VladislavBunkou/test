package com.example.android.marsrealestate.di.mv.dress

import android.location.Location
import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.android.marsrealestate.di.mv.BaseViewModel
import com.example.android.marsrealestate.dress.Dress
import com.example.android.marsrealestate.dress.DressApiStatus
import com.example.android.marsrealestate.dress.DressRepository
import com.example.android.marsrealestate.dress.addDress.DressGetInfo
import com.example.android.marsrealestate.enums.Status
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.ui.dress.d_latitude
import com.example.android.marsrealestate.ui.dress.d_longitude
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.Collections.addAll
import kotlin.math.roundToInt

class DressViewModel : BaseViewModel<Any>(){

    val dressListObservable = ObservableArrayList<Dress>()

    //status
    val status: LiveData<DressApiStatus>
        get() = _status
    private val _status = MutableLiveData<DressApiStatus>()

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main )

    val dress: LiveData<List<Dress>> get() = _dress
    private val _dress = MutableLiveData<List<Dress>>()

    lateinit var dressRepository: DressRepository // Added


    private val allDressObserver = Observer<List<Dress>> { list ->
        list?.let {
            dressListObservable.run {
                Log.d("DressViewModel", "22 this.size = ${this.size}")
                clear()
                addAll(list)
            }
        }
    }

    private val receiveDressObserver = Observer<DressGetInfo> {
        when (it.status) {
            Status.INITIAL, Status.LOADING -> {
                setIsLoading(true)
                Log.d("DressViewModel", "Status.Loading")
            }
            Status.SUCCESS -> {
                setIsLoading(false)
                Log.d("DressViewModel", "Status.SUCCESS")
            }
            Status.ERROR -> {
                setIsLoading(false)
                Log.d("DressViewModel", "Status.Error")
            }
            Status.ERROR_CONNECTION -> {
                setIsLoading(false)
            }
            else -> {
            }
        }
    }


    init {
        dressRepository = DressRepository(coroutineScope)
        dressRepository.dress.observeForever(allDressObserver)
        dressRepository.receiveDressObservable.observeForever(receiveDressObserver)
        dressRepository.getDressList()

    }

    fun getDressList(str: String) { // Added
        dressRepository.getDressList(str)
    }

    fun getSortingDressList(str: String){
        dressRepository.getSortingDressList(str)
    }
/*
    fun sorting(list: List<Dress>, latitude: Double, longitude: Double):List<Dress>{
        dressRepository.sortingDressList(dress, d_latitude, d_longitude)
    }
*/

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun testRefresh() {
        Log.d("DressViewModel", "REFRESH")
    }



}
