package com.example.android.marsrealestate.ui.prod

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentDetailBinding
import com.example.android.marsrealestate.di.mv.prod.ProdDetailViewModel
import com.example.android.marsrealestate.di.mv.prod.ProdDetailViewModelFactory


class ProdDetailFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val application = requireNotNull(activity).application
        val binding = FragmentDetailBinding.inflate(inflater)
        binding.lifecycleOwner = this
        val marsProperty = ProdDetailFragmentArgs.fromBundle(arguments!!).selectedProperty
        val viewModelFactory = ProdDetailViewModelFactory(marsProperty, application)
        binding.viewModel = ViewModelProviders.of(this, viewModelFactory).get(ProdDetailViewModel::class.java)
        return binding.root
    }
}