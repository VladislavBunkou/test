package com.example.android.marsrealestate.tablet.model

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentTabletDetailBinding

class TabletOneFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val application = requireNotNull(activity).application
        val binding =  FragmentTabletDetailBinding.inflate(inflater)
        binding.lifecycleOwner = this

        val tabletProperty = TabletOneFragmentArgs.fromBundle(arguments!!).selectedTabletProperty
        val viewModelFactory = TabletOneViewModelFactory(tabletProperty, application)
        binding.viewModel = ViewModelProviders.of(
                this, viewModelFactory).get(TabletOneViewModel::class.java)

        return binding.root
    }
}