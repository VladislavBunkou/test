package com.example.android.marsrealestate.shoes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.databinding.FragmentShoesBinding

class ShoesFragment : Fragment(), ShoesItemListener {

    private val viewModel: ShoesViewModel by lazy {
        ViewModelProviders.of(this).get(ShoesViewModel::class.java)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val binding = FragmentShoesBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.listenerShoes = this

        binding.lasersGrid.adapter = ShoesRecyclerAdapter()


        return binding.root
    }

    override fun onSubUserItemClick(s: Shoes) {
        this.findNavController().navigate(ShoesFragmentDirections.actionShoesFragmentToShoesDetailFragment(s))
    }


}

