package com.example.android.marsrealestate.guitar.guitar_add.guitar_enum

enum class GuitarManufacturerCountryEnum (val id: String) {
    OTHER("0"){ override fun get(): String = "Other" },
    JAPAN("1"){ override fun get(): String = "Japan" },
    CHINA("2"){ override fun get(): String = "China" },
    INDONESIA("3"){ override fun get(): String = "Indonesia" },
    KOREA("4"){ override fun get(): String = "Korea" },
    AUSTRALIA("5"){ override fun get(): String = "Australia" },
    MEXICO("6"){ override fun get(): String = "Mexico" },
    USA("7"){ override fun get(): String = "USA" },
    UK("8"){ override fun get(): String = "UK" },
    FRANCE("9"){ override fun get(): String = "France" },
    GERMANY("10"){ override fun get(): String = "Germany" };

    abstract fun get(): String
}