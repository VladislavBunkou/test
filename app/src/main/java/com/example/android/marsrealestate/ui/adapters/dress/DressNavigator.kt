package com.example.android.marsrealestate.ui.adapters.dress

import androidx.databinding.ObservableField

interface DressNavigator {
    fun showToast(model: String)

    fun errorMessages(
            isModelFilled: Boolean,
            isCityFilled: Boolean,
            isFabricFilled: Boolean,
            isPriceFilled: Boolean,
            isSexFilled: Boolean,
            isColorFilled: Boolean,
            isSellerFilled: Boolean,
            isConditionFilled: Boolean,
            isManufacturerFilled: Boolean

    )

    fun getOneColor() : ObservableField<Boolean>
    fun getMultiColor() : ObservableField<Boolean>
    fun getDecoration() : ObservableField<Boolean>
    fun getPrint() : ObservableField<Boolean>

    fun getProductIsNew() : ObservableField<Boolean>

    fun createSelectSizeDialog()
    fun createSelectSleeveDialog()
    fun createSelectSilhouetteDialog()

    fun openFileChooser()
    fun uploadFile()
    fun getPhotoId(): String
}