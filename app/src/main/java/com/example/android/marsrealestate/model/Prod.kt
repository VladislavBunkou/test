package com.example.android.marsrealestate.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Prod(
        val id: Long,
        val model: String,
        val typeOfProduct: String,
        val manufacturer: String,
        val latitude: Double,
        val longitude: Double,


        val manufacturerCountry: String,
        val productIsNew: Boolean,
        val url: String)
    : Parcelable{
    val produceInGemrany: Boolean
            get() =  manufacturer=="Germany"
}


@Parcelize
data class ProdOne(
        val model: String,
        val typeOfProduct: String,
        val manufacturer: String,
        val manufacturerCountry: String,
        val url: String)
    : Parcelable