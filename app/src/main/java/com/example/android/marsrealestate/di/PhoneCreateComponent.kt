package com.example.android.marsrealestate.di


import com.example.android.marsrealestate.ui.phone.PhoneCreateActivity
import com.example.android.marsrealestate.ui.phone.PhoneCreateFragment
import dagger.Subcomponent

@ActivityScope
@Subcomponent
interface PhoneCreateComponent {

    @Subcomponent.Factory
    interface Factory{
        fun create(): PhoneCreateComponent
    }

    fun inject(activity: PhoneCreateActivity)
    fun inject(fragment: PhoneCreateFragment)
}