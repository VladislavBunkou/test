package com.example.android.marsrealestate.motorbike

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentMotorbike1Binding
import com.akordirect.vmccnc.databinding.GridViewItemMotorbikeBinding
import com.example.android.marsrealestate.network.RetrofitApi
import kotlinx.android.parcel.Parcelize
import kotlinx.coroutines.*
import java.lang.Exception

@Parcelize
class Motorbike(

        val id: Long,
        val model: String,
        val typeOfProduct: String,
        val manufacturer: String,
        val manufacturerCountry: String,
        val url: String) : Parcelable


enum class MotorbikeApiStatus {
    LOADING, ERROR, DONE
}

@BindingAdapter("apiStatusMotorbike") //
fun bindStatusMotorbike(statusImageView: ImageView, status: MotorbikeApiStatus?) {
    when (status) {
        MotorbikeApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        MotorbikeApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        MotorbikeApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}



@BindingAdapter("listDataMotorbike")
fun bindRecyclerViewMotorbike(recyclerView: RecyclerView, data: List<Motorbike>?) {
    val adapter = recyclerView.adapter as MotorbikeGridAdapter
    adapter.submitList(data)
}




class Motorbike1Fragment : Fragment() {

    private val viewModel: Motorbike1ViewModel by lazy {
        ViewModelProviders.of(this).get(Motorbike1ViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentMotorbike1Binding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel



        binding.drillingGrid.adapter = MotorbikeGridAdapter(MotorbikeGridAdapter.OnClickListenerMotorbike {
            viewModel.displayOneMotorbike(it)
            Toast.makeText(context, "Motorbikeed ${it.model}", Toast.LENGTH_SHORT).show()
        })

        viewModel.navigateToSelectedMotorbike.observe(this, Observer {
            it?.let {
                this.findNavController()
                    .navigate(Motorbike1FragmentDirections.actionPress1FragmentToPress2Fragment(it))
                viewModel.displayOneMotorbikeComplete()
            }
        })

        return binding.root
    }
}

class Motorbike1ViewModel : ViewModel() {

    val properties: LiveData<List<Motorbike>> get() = _properties
    private val _properties = MutableLiveData<List<Motorbike>>()

    val status: LiveData<MotorbikeApiStatus> get() = _status
    private val _status = MutableLiveData<MotorbikeApiStatus>()

    val navigateToSelectedMotorbike: LiveData<Motorbike> get() = _navigateToSelectedMotorbike
    private val _navigateToSelectedMotorbike = MutableLiveData<Motorbike>()

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)


    init {
        getListMotorbikeFromServer()
    }

    private fun getListMotorbikeFromServer() {

        coroutineScope.launch {
            //            var jobMy = RetrofitApi.retrofitApiService.getListVmcs(axes = "all", condition = "all")
            var jobMotorbike: Deferred<List<Motorbike>> = RetrofitApi.retrofitApiService.getMotorbikeList()
            try {
                _status.value = MotorbikeApiStatus.LOADING
                val listMotorbike: List<Motorbike> = jobMotorbike.await()
                _status.value = MotorbikeApiStatus.DONE
                _properties.value = listMotorbike

            } catch (e: Exception) {
                _status.value = MotorbikeApiStatus.ERROR
                _properties.value = listOf()
            }
        }

    }

    fun displayOneMotorbike(lathe: Motorbike) {
        _navigateToSelectedMotorbike.value = lathe
    }

    fun displayOneMotorbikeComplete() {
        _navigateToSelectedMotorbike.value = null
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }


}


class MotorbikeGridAdapter(private val onClickListenerMotorbike: OnClickListenerMotorbike) :
    ListAdapter<Motorbike, MotorbikeGridAdapter.MotorbikeViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MotorbikeViewHolder {
        return MotorbikeViewHolder(GridViewItemMotorbikeBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: MotorbikeViewHolder, position: Int) {
        val press = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListenerMotorbike.onClick(press)
        }
        holder.bind(press)
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Motorbike>() {
        override fun areItemsTheSame(oldItem: Motorbike, newItem: Motorbike): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Motorbike, newItem: Motorbike): Boolean {
            return oldItem.id == newItem.id
        }
    }

    class MotorbikeViewHolder(private var binding: GridViewItemMotorbikeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(press: Motorbike) {
            binding.property = press
            binding.executePendingBindings()
        }
    }

    class OnClickListenerMotorbike(val clickListener: (lathe: Motorbike) -> Unit) {
        fun onClick(lathe: Motorbike) = clickListener(lathe)
    }
}