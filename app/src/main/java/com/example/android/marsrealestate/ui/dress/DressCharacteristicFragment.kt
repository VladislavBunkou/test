package com.example.android.marsrealestate.ui.dress

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.Constraints.TAG
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentDressCharacteristicBinding
import com.example.android.marsrealestate.di.mv.dress.DressCharacteristicViewModel
import com.example.android.marsrealestate.di.mv.dress.DressInfoViewModel

class DressCharacteristicFragment: Fragment(){

    lateinit var binding: FragmentDressCharacteristicBinding

    private val viewModel: DressCharacteristicViewModel by lazy {
        ViewModelProviders.of(this).get(DressCharacteristicViewModel::class.java)
    }

    private lateinit var dressInfoViewModel: DressInfoViewModel

    @SuppressLint("LongLogTag")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val application = requireNotNull(activity).application

        binding = FragmentDressCharacteristicBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        activity?.let {
            dressInfoViewModel = ViewModelProviders.of(it).get(DressInfoViewModel::class.java)

            viewModel.oneColorSelected.value = dressInfoViewModel.oneColor.get()
            viewModel.multiColorSelected.value = dressInfoViewModel.multiColor.get()
            viewModel.decorationSelected.value = dressInfoViewModel.decoration.get()
            viewModel.printSelected.value = dressInfoViewModel.print.get()

            viewModel.productIsNewSelected.value = dressInfoViewModel.productIsNew.get()

            viewModel.oneColorSelected.observe(viewLifecycleOwner, Observer {
                dressInfoViewModel.oneColor.set(it)
                Log.d(TAG, "one color $it ")
            })

            viewModel.multiColorSelected.observe(viewLifecycleOwner, Observer {
                dressInfoViewModel.multiColor.set(it)
                Log.d(TAG, "multi color $it ")
            })

            viewModel.decorationSelected.observe(viewLifecycleOwner, Observer {
                dressInfoViewModel.decoration.set(it)
                Log.d(TAG, "decoration $it ")
            })

            viewModel.printSelected.observe(viewLifecycleOwner, Observer {
                dressInfoViewModel.print.set(it)
                Log.d(TAG, "print $it ")
            })

            viewModel.productIsNewSelected.observe(viewLifecycleOwner, Observer {
                dressInfoViewModel.productIsNew.set(it)
                Log.d(TAG, "product is new $it ")
            })
    }
        return binding.root
}


}