package com.example.android.marsrealestate.dress

interface DressItemListener {
    fun onSubUserItemClick(s: Dress)
}