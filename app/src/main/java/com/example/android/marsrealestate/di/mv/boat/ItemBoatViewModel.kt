package com.example.android.marsrealestate.di.mv.boat

import android.location.Location
import android.util.Log
import com.example.android.marsrealestate.boat.model.Boat
import com.example.android.marsrealestate.di.mv.BaseViewModel
import com.example.android.marsrealestate.globalLatitude
import com.example.android.marsrealestate.globalLongitude
import com.example.android.marsrealestate.repository.boat.BoatItemListener


class ItemBoatViewModel : BaseViewModel<Any>(){

    var listener : BoatItemListener? = null
    lateinit var boat: Boat
    var distance: String = ""
    var distanceShow: Boolean = false

    fun bind(boat: Boat, listener : BoatItemListener){
        this.boat = boat
        this.listener = listener
        //Location.distanceBetween(globalLatitude, globalLongitude, boat.latitude, boat.longitude)
        Log.d("DISTANCE", "${boat.longitude} - $globalLongitude + ${boat.latitude} - $globalLongitude")

        val loc1 = Location("")
        loc1.latitude = globalLatitude
        loc1.longitude = globalLongitude
        Log.d("DISTANCE", "yours $globalLongitude  $globalLatitude")

        val loc2 = Location("")
        loc2.latitude = boat.latitude
        loc2.longitude = boat.longitude
        Log.d("DISTANCE", "product ${boat.longitude} ${boat.latitude}")

        distance = String.format("%.3f",(loc1.distanceTo(loc2))/1000)+" km"
        Log.d("DISTANCE", "distance = $distance")
        //distance = kotlin.math.sqrt((boat.longitude - globalLongitude).pow(2) + (boat.latitude - globalLongitude).pow(2))
    }

    fun onClickItem(){
        Log.d("BOATCHECK", "dm-> Is clicked boat = ${boat.model}")
        listener?.onSubUserItemClick(boat)
    }

}