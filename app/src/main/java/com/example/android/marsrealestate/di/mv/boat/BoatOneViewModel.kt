package com.example.android.marsrealestate.di.mv.boat

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.android.marsrealestate.boat.model.Boat

class BoatOneViewModel(boat: Boat, app: Application): AndroidViewModel(app) {
    val selectedProperty: LiveData<Boat> get() = _selectedProperty
    private val _selectedProperty = MutableLiveData<Boat>()


    init {
        _selectedProperty.value = boat
    }
}