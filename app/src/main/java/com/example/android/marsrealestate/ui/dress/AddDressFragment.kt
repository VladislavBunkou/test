package com.example.android.marsrealestate.ui.dress

import android.app.Activity
import android.content.ContentResolver
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.webkit.MimeTypeMap
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentAddDressBinding
import com.example.android.marsrealestate.di.mv.dress.AddDressViewModel
import com.example.android.marsrealestate.di.mv.dress.DressInfoViewModel
import com.example.android.marsrealestate.di.mv.nav.NavViewModel
import com.example.android.marsrealestate.itemInCabinet
import com.example.android.marsrealestate.ui.adapters.dress.DressNavigator
import com.example.android.marsrealestate.ui.authentification.LoginFragment.Companion.TAG
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions


var d_latitude = 0.0
var d_longitude = 0.0

@RuntimePermissions
class AddDressFragment: Fragment(), DressNavigator {

lateinit var binding: FragmentAddDressBinding

    private val viewModel by viewModels<AddDressViewModel>()

    //LOCATION
    private lateinit var fusedLocationClient: FusedLocationProviderClient



    private lateinit var dressInfoViewModel: DressInfoViewModel
    lateinit var navViewModel: NavViewModel

    //For upload image
    lateinit var storage: FirebaseStorage
    lateinit var storageRef: StorageReference

    lateinit var imageView: ImageView
    lateinit var textResult: TextView

    private var imageUri: Uri = Uri.EMPTY // for saving image
    private var uploadedImageUri: Uri = Uri.EMPTY // for saving image
    private var fileIsSelected = false // true when we choose image

    //@NeedsPermission(android.Manifest.permission.LOCATION_HARDWARE)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val application = requireNotNull(activity).application

        binding = FragmentAddDressBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        viewModel.setNavigator(this)

        //LOCATION
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.requireActivity())
        //getLastLocation()

        this.fusedLocationClient.lastLocation
                .addOnSuccessListener { location->
                    if (location != null) {
                        d_latitude =  location.latitude
                        d_longitude = location.longitude
                        Log.d(TAG,"AddDressFragment: km->Location  latitude=$d_latitude, longitude=$d_longitude")
                    }
                    else if (location == null){
                        Log.d(TAG,"AddDressFragment: km->Location IS NULL latitude=$d_latitude, longitude=$d_longitude")
                    }

                }

        imageView = binding.imageView
        textResult = binding.textViewShowUploads
        storage = FirebaseStorage.getInstance()
        storageRef = storage.reference

        activity?.let {
            dressInfoViewModel = ViewModelProviders.of(it).get(DressInfoViewModel::class.java)
            navViewModel = ViewModelProviders.of(it).get(NavViewModel::class.java)
            Log.d("AddDressFragment", "dm->  navViewModel.id.value = ${navViewModel.id.value}}")
        }
        navViewModel.id.observe(this, Observer {
            if (navViewModel.isCreateNew.value == true)
                viewModel.receiveDressById(navViewModel.id.value ?: 1, true)

            else  {
                viewModel.receiveDressById(navViewModel.id.value ?: 1, false)
                navViewModel.isCreateNew.postValue(true)
            }
            Log.d("AddDressFragment", "navViewModel.id.value = ${navViewModel.id.value}}")
        })
      viewModel.navigateToDressCharacteristics.observe(this, Observer {
            if (it) {
                this.findNavController().navigate(
                        AddDressFragmentDirections.actionAddDressFragmentToDressCharacteristicFragment())
                viewModel.displayCharacteristicsComplete()
            }
        })


        binding.buttonChooseFile.setOnClickListener {
            preOpenFileChooserWithPermissionCheck() // // step 3 -add this fun (it is genegated!!)
            //odtieneLocationWithPermissionCheck()
        }

        return binding.root
    }

    @NeedsPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            /*android.Manifest.permission.LOCATION_HARDWARE*/)
    fun preOpenFileChooser(){
        openFileChooser()
    }

    // step 4
    @OnPermissionDenied(android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            /*android.Manifest.permission.LOCATION_HARDWARE*/)
    fun permissionDenied() {
        Log.d("AddDressFragment", "dm:: OnPermissionDenied")
    }
    // step 4
    @OnNeverAskAgain(android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            /*android.Manifest.permission.LOCATION_HARDWARE*/)
    fun neverAskAgain() {
        Log.d("AddDressFragment", "dm:: OnNeverAskAgain")
    }

    override fun showToast(model: String) {
        Toast.makeText(context, "model: $model", Toast.LENGTH_LONG).show()
    }


    override fun errorMessages(isModelFilled: Boolean, isCityFilled: Boolean, isFabricFilled: Boolean,
                               isPriceFilled: Boolean, isSexFilled: Boolean, isColorFilled: Boolean,
                               isSellerFilled: Boolean, isConditionFilled: Boolean, isManufacturerFilled: Boolean) {
        if (!isConditionFilled) {
            binding.dressConditionLayout.isErrorEnabled = true
            binding.dressConditionLayout.error = "Enter Condition"
        } else {
            binding.dressConditionLayout.isErrorEnabled = false
        }

        if (!isSellerFilled) {
            binding.dressSellerLayout.isErrorEnabled = true
            binding.dressSellerLayout.error = "Enter Seller"
        } else {
            binding.dressSellerLayout.isErrorEnabled = false
        }

        if (!isColorFilled) {
            binding.dressColorLayout.isErrorEnabled = true
            binding.dressColorLayout.error = "Enter Color"
        } else {
            binding.dressColorLayout.isErrorEnabled = false
        }

        if (!isSexFilled) {
            binding.dressSexLayout.isErrorEnabled = true
            binding.dressSexLayout.error = "Enter Sex"
        } else {
            binding.dressSexLayout.isErrorEnabled = false
        }

        if (!isPriceFilled) {
            binding.dressPriceLayout.isErrorEnabled = true
            binding.dressPriceLayout.error = "Enter Price"
        } else {
            binding.dressPriceLayout.isErrorEnabled = false
        }

        if (!isModelFilled) {
            binding.dressModelLayout.isErrorEnabled = true
            binding.dressModelLayout.error = "Enter model"
        } else {
            binding.dressModelLayout.isErrorEnabled = false
        }
        if (!isCityFilled) {
            binding.dressCityLayout.isErrorEnabled = true
            binding.dressCityLayout.error = "Enter city"
        } else {
            binding.dressCityLayout.isErrorEnabled = false
        }

        if (!isFabricFilled) {
            binding.dressFabricLayout.isErrorEnabled = true
            binding.dressFabricLayout.error = "Enter fabric"
        } else {
            binding.dressFabricLayout.isErrorEnabled = false
        }

        if (!isManufacturerFilled) {
            binding.dressManufacturerLayout.isErrorEnabled = true
            binding.dressManufacturerLayout.error = "Enter manufacturer"
        } else {
            binding.dressManufacturerLayout.isErrorEnabled = false
        }
    }


    override fun getOneColor(): ObservableField<Boolean> = dressInfoViewModel.oneColor

    override fun getMultiColor(): ObservableField<Boolean> = dressInfoViewModel.multiColor

    override fun getDecoration(): ObservableField<Boolean> = dressInfoViewModel.decoration

    override fun getPrint(): ObservableField<Boolean> = dressInfoViewModel.print

    override fun getProductIsNew(): ObservableField<Boolean> = dressInfoViewModel.productIsNew


    private var currentSelectedSizeIndex = -1
    override fun createSelectSizeDialog() {
        val listCategory = listOf("XS", "S", "M", "L", "XL", "One size")

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setSingleChoiceItems(listCategory.toTypedArray(),
                        currentSelectedSizeIndex) { dialog, which ->
                    currentSelectedSizeIndex = which
                    viewModel.updateSize(which + 1)
                    dialog.dismiss()
                }
                setTitle("Dress size")
                setNegativeButton("Cancel") { _, _ ->
                }
            }
            builder.create()
        }
        alertDialog?.show()    }


    private var currentSelectedSleeveIndex = -1
    override fun createSelectSleeveDialog() {
        val listCategory = listOf("Full", "1/2", "3/4", "Short", "Sleeveless")

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setSingleChoiceItems(listCategory.toTypedArray(),
                        currentSelectedSleeveIndex) { dialog, which ->
                    currentSelectedSleeveIndex = which
                    viewModel.updateSleeve(which + 1)
                    dialog.dismiss()
                }
                setTitle("Dress sleeve")
                setNegativeButton("Cancel") { _, _ ->
                }
            }
            builder.create()
        }
        alertDialog?.show()    }


    private var currentSelectedSilhouetteIndex = -1
    override fun createSelectSilhouetteDialog() {
        val listCategory = listOf("A-Line", "Asymmetrical", "Bud", "Sheath", "Mermaid")

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setSingleChoiceItems(listCategory.toTypedArray(),
                        currentSelectedSilhouetteIndex) { dialog, which ->
                    currentSelectedSilhouetteIndex = which
                    viewModel.updateSilhouette(which + 1)
                    dialog.dismiss()
                }
                setTitle("Dress silhouette")
                setNegativeButton("Cancel") { _, _ ->
                }
            }
            builder.create()
        }
        alertDialog?.show()    }

    override fun openFileChooser() {
        textResult.text = "_"

        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, viewModel.PICK_IMAGE_REQUEST)
        fileIsSelected = true
    }

    override fun uploadFile() {
        if (imageUri != null) {
            val fileReference: StorageReference = storageRef.child(

                    "image/dress/${System.currentTimeMillis()}.${getFileExtension(imageUri)}"
            )
                    //ValidationUtils.isFileHasGoodSize()
            val uploadTask = fileReference.putFile(imageUri)
                    .addOnCompleteListener() {
                        val handler = Handler()
                        handler.postDelayed({ binding.progressBar.progress = 0 }, 500)
                        Toast.makeText(context, "Upload successful", Toast.LENGTH_LONG).show()

                        Picasso.get().load(R.drawable.ic_image_is_uploded).into(imageView)

                        textResult.text = "OK!"
                        textResult.setTextColor(Color.GREEN)
                    }

                    .addOnFailureListener { e ->
                        Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
                    }

                    .addOnProgressListener {
                        val progress = 100.0 * it.bytesTransferred / it.totalByteCount
                        binding.progressBar.setProgress(progress.toInt())
                    }

            val urlTask = uploadTask.continueWithTask {
                        if (!it.isSuccessful) {
                            it.exception?.let { ex ->
                                throw ex
                            }
                        }
                        fileReference.downloadUrl
                    }
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            uploadedImageUri = it.result ?: Uri.EMPTY
                            Toast.makeText(context, "Url: $uploadedImageUri", Toast.LENGTH_SHORT).show()
                            Log.d("km->", "Url: $uploadedImageUri")
                            textResult.text = "1) Image is uploaded!"
                        } else {
                            // Handle failures
                            Toast.makeText(context, "something wrong!!!", Toast.LENGTH_SHORT).show()
                        }
                    }

        } else {
            Toast.makeText(context, "No file selected", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getPhotoId() = uploadedImageUri.toString()

    private fun getFileExtension(uri: Uri): String? {
        val cR: ContentResolver = activity!!.contentResolver
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(cR.getType(uri))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == viewModel.PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK
                && data?.data != null) {
            imageUri = data.data!!
            Picasso.get().load(imageUri).into(imageView)
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        itemInCabinet = false
        Log.d("GridItem", "close new trailer fragment $itemInCabinet")
    }

}