package com.example.android.marsrealestate.dress

import android.location.Location
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.dress.addDress.DressGetInfo
import com.example.android.marsrealestate.dress.addDress.DressSaveInfo
import com.example.android.marsrealestate.enums.Status
import com.example.android.marsrealestate.ui.dress.d_latitude
import com.example.android.marsrealestate.ui.dress.d_longitude
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlin.math.abs
import kotlin.math.roundToInt



class DressRepository(val coroutineScope: CoroutineScope) {

    val dressID =MutableLiveData<Long>()
    val dress = MutableLiveData<List<Dress>>()
    val saveDressObservable = MutableLiveData<DressSaveInfo>()
    val receiveDressObservable = MutableLiveData<DressGetInfo>()

    //var distance: Int = 0

    val wordsToSearchDressObservable = MutableLiveData<String>()
    val manufacturerDressObservable = MutableLiveData<String>()
    val modelDressObservable = MutableLiveData<String>()
    val silhouetteDressObservable = MutableLiveData<String>()
    val sexDressObservable = MutableLiveData<String>()
    val fabricDressObservable = MutableLiveData<String>()
    val sleeveDressObservable = MutableLiveData<String>()
    val cityDressObservable = MutableLiveData<String>()
    val sizeDressObservable = MutableLiveData<String>()
    val colorDressObservable = MutableLiveData<String>()
    val sellerDressObservable = MutableLiveData<String>()
    val productConditionDressObservable = MutableLiveData<String>()
    val priceDressObservable = MutableLiveData<String>()
    val descriptionDressObservable = MutableLiveData<String>()


    fun getDressList(str: String = "") {
        receiveDressObservable.postValue(DressGetInfo(Status.LOADING))
        Log.d("DressRepository", "  1")
        coroutineScope.launch {
            Log.d("DressRepository", " 2")
            var getPropertiesDeferred = if (str.isEmpty()) RetrofitApi.retrofitApiService.getDressList()
            else RetrofitApi.retrofitApiService.getDressList(str)
            Log.d("DressRepository", " $getPropertiesDeferred" )
            try {
                var listResult = getPropertiesDeferred.await()
                Log.d("DressRepository", " $listResult" )
                dress.value = /*sortingDressList(*/listResult/*, d_latitude, d_longitude)*/
                Log.d("DressRepository", "11 / dress.value = ${dress.value?.size}" )
                Log.d("DressRepository", " ${dress.value}" )
                receiveDressObservable.postValue(DressGetInfo(Status.SUCCESS))
            } catch (e: Exception) {
                Log.d("DressRepository", "$e")
                dress.value = ArrayList()
                receiveDressObservable.postValue(DressGetInfo(Status.ERROR_CONNECTION))
            }

        }
    }

    fun getSortingDressList(str: String = ""){
        receiveDressObservable.postValue(DressGetInfo(Status.LOADING))
        Log.d("DressRepository", "SORTED LIST")
        coroutineScope.launch {
             var getPropertiesDeferred = if (str.isEmpty()) RetrofitApi.retrofitApiService.getDressList()
            else RetrofitApi.retrofitApiService.getDressList(str)
            Log.d("DressRepository", " $getPropertiesDeferred" )
            try {
                var listResult = getPropertiesDeferred.await()
                Log.d("DressRepository", " $listResult" )
                dress.value = sortingDressList(listResult, d_latitude, d_longitude)
                Log.d("DressRepository", "11 / dress.value = ${dress.value?.size}" )
                Log.d("DressRepository", " ${dress.value}" )
                receiveDressObservable.postValue(DressGetInfo(Status.SUCCESS))
            } catch (e: Exception) {
                Log.d("DressRepository", "$e")
                dress.value = ArrayList()
                receiveDressObservable.postValue(DressGetInfo(Status.ERROR_CONNECTION))
            }

        }
    }

    private fun sortingDressList(list: List<Dress>, latitude: Double, longitude: Double): List<Dress> {
        return list.sortedWith(Comparator { o1, o2 ->
            var distance1 = FloatArray(1)
            var distance2 = FloatArray(1)
            Location.distanceBetween(
                    latitude,
                    longitude,
                    o1?.latitude ?: 0.0,
                    o1?.longitude ?: 0.0,
                    distance1
            )
            Location.distanceBetween(
                    latitude,
                    longitude,
                    o2?.latitude ?: 0.0,
                    o2?.longitude ?: 0.0,
                    distance2
            )
            val difference = (distance1[0] - distance2[0]).roundToInt()
            Log.d("DressRepository", "$difference")
            //distance = abs(difference)/1000
            //Log.d("DressRepository", "$distance")
            difference
        })
    }





    fun receiveDressById(id: Long, isSaveNewDressOnServer: Boolean) {
        receiveDressObservable.postValue(DressGetInfo(Status.LOADING))
        dressID.postValue(id)
        // change
        if (isSaveNewDressOnServer) {
            receiveDressObservable.postValue(DressGetInfo(Status.SUCCESS))
            Log.d("DressRepository", "getMarsRealEstateProperties / id = ${id} - means new Dress")

        } else{
            coroutineScope.launch {
                dressID.postValue(id)
                Log.d("DressRepository", "getMarsRealEstateProperties / id = ${id}")
                var getPropertiesDeferred = RetrofitApi.retrofitApiService.getDressById(id)
                try {
                    val dressOne = getPropertiesDeferred.await()
                    Log.d("DressRepository", "dm-> try dress = $dressOne")
                    //userIdDressObservable.postValue(dressOne.userId)
                    wordsToSearchDressObservable
                    manufacturerDressObservable.postValue(dressOne.manufacturer)
                    modelDressObservable.postValue(dressOne.model)
                    silhouetteDressObservable.postValue(dressOne.silhouette)
                    sexDressObservable.postValue(dressOne.sex)
                    fabricDressObservable.postValue(dressOne.fabric)
                    sleeveDressObservable.postValue(dressOne.sleeve)
                    cityDressObservable.postValue(dressOne.city)
                    sizeDressObservable.postValue(dressOne.size)
                    colorDressObservable.postValue(dressOne.color)
                    sellerDressObservable.postValue(dressOne.seller)
                    productConditionDressObservable.postValue(dressOne.productCondition)
                    priceDressObservable.postValue(dressOne.price)
                    descriptionDressObservable.postValue(dressOne.description)

                    receiveDressObservable.postValue(DressGetInfo(Status.SUCCESS))
                } catch (e: Exception) {
                    Log.d("DressRepository", "catch : ${e.message}")
                    receiveDressObservable.postValue(DressGetInfo(Status.ERROR_CONNECTION))
                }
            }}
    }

}


