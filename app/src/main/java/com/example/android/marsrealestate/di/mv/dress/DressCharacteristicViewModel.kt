package com.example.android.marsrealestate.di.mv.dress

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DressCharacteristicViewModel: ViewModel(){
    val oneColorSelected = MutableLiveData<Boolean>(false)
    val multiColorSelected = MutableLiveData<Boolean>(false)
    val decorationSelected = MutableLiveData<Boolean>(false)
    val printSelected = MutableLiveData<Boolean>(false)

    val productIsNewSelected = MutableLiveData<Boolean>(false)
}