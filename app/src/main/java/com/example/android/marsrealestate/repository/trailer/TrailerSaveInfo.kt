package com.example.android.marsrealestate.repository.trailer

import com.example.android.marsrealestate.enums.ResponseCode
import com.example.android.marsrealestate.enums.Status


class TrailerSaveInfo(
//        protected
        var status: Status,
        val code: ResponseCode = ResponseCode.SUCCESS,
        val message: String = "")


class TrailerGetInfo(
//        protected
        var status: Status,
        val code: ResponseCode = ResponseCode.SUCCESS,
        val message: String = "")