package com.example.android.marsrealestate.guitar.guitar_add.guitar_enum

enum class GuitarManufacturerEnum (val id: String) {
    OTHER("0"){ override fun get(): String = "Other" },
    YAMAHA("1"){ override fun get(): String = "Yamaha" },
    GIBSON("2"){ override fun get(): String = "Gibson" },
    FENDER("3"){ override fun get(): String = "Fender" },
    IBANEZ("4"){ override fun get(): String = "Ibanez" },
    WASHBURN("5"){ override fun get(): String = "Washburn" },
    BEHRINGER("6"){ override fun get(): String = "Behringer" },
    CORT("7"){ override fun get(): String = "Cort" };

    abstract fun get(): String
}