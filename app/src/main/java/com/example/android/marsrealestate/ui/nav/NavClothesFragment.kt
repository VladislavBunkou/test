package com.example.android.marsrealestate.ui.nav

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.databinding.FragmentNavClothesBinding
import com.example.android.marsrealestate.di.mv.nav.NavClothesViewModel

class NavClothesFragment : Fragment() {

    val TAG = "NavClothesFragment"

    private val viewModel: NavClothesViewModel by lazy { ViewModelProviders.of(this).get(NavClothesViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentNavClothesBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        viewModel.navigateToDress.observe(this, Observer {
            if (it) {
                this.findNavController().navigate(NavClothesFragmentDirections.actionNavClothesFragmentToDressFragment())
                viewModel.navigationDressFinished()
            }
        })

        viewModel.navigateToShoes.observe(this, Observer {
            if (it) {
                this.findNavController().navigate(NavClothesFragmentDirections.actionNavClothesFragmentToShoesFragment())
                viewModel.navigationShoesFinished()
            }
        })

        return binding.root
    }
}