package com.example.android.marsrealestate.computer.addcomputer

import com.google.gson.annotations.SerializedName

/**
 * Класс ComputerBody
 * @author Ilya Osipov (mailto:il.osipov.gm@gmail.com)
 * @since 28.04.2020
 * @version $Id$
 */

class ComputerBody (
        @SerializedName("id")
        var id: Long = 0,

        @SerializedName("userId")
        var userId: String = "",

        @SerializedName("model")
        var model: String = "",

        @SerializedName("manufacturer")
        var manufacturer: String = "",

        @SerializedName("manufacturerCountry")
        val manufacturerCountry: String = "",

        @SerializedName("productIsNew")
        val productIsNew: Boolean = false,

        @SerializedName("description")
        val description: String = "",

        @SerializedName("city")
        val city: String = "",

        @SerializedName("latitude")
        val latitude: Double = 0.0,

        @SerializedName("longitude")
        val longitude: Double = 0.0,

        @SerializedName("price")
        val price: Int = 0,

        @SerializedName("category")
        val category: String = "",

        @SerializedName("seller")
        val seller: String,

        @SerializedName("productCondition")
        val productCondition: String = "",

        @SerializedName("url")
        val url: String = "",

        @SerializedName("wordsToSearch")
        val wordsToSearch: String = ""
)