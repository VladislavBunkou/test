package com.example.android.marsrealestate.guitar.guitar_add.guitar_enum

enum class GuitarNumberOfFretsEnum (val id: String) {
    OTHER("0"){ override fun get(): String = "Other" },
    NINETEEN("1"){ override fun get(): String = "19" },
    TWENTY("2"){ override fun get(): String = "20" },
    TWENTYONE("3"){ override fun get(): String = "21" },
    TWENTYFOUR("4"){ override fun get(): String = "24" },
    TWENTYSEVEN("5"){ override fun get(): String = "27" };

    abstract fun get(): String
}