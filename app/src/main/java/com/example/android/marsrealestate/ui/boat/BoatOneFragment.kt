package com.example.android.marsrealestate.ui.boat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentBoatDetailBinding
import com.example.android.marsrealestate.di.mv.boat.BoatOneViewModel
import com.example.android.marsrealestate.di.mv.boat.BoatOneViewModelFactory

class BoatOneFragment: Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val application = requireNotNull(activity).application
        val binding =  FragmentBoatDetailBinding.inflate(inflater)
        binding.lifecycleOwner = this
        val boatProperty = BoatOneFragmentArgs.fromBundle(arguments!!).selectedBoatProperty
        val viewModelFactory = BoatOneViewModelFactory(boatProperty, application)
        binding.viewModel = ViewModelProviders.of(
                this, viewModelFactory).get(BoatOneViewModel::class.java)
        return binding.root
    }
}