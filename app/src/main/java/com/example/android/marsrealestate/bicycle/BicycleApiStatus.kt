package com.example.android.marsrealestate.bicycle

enum class BicycleApiStatus {
        LOADING, ERROR, DONE
}