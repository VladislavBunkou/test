package com.example.android.marsrealestate.ui.adapters.phone

import androidx.databinding.ObservableField


interface PhoneNavigator{
    fun showToast(str: String)
    fun errorMessages(isModelFilled : Boolean, isCountryFilled: Boolean)

    fun get3G() : ObservableField<Boolean>
    fun get4G() : ObservableField<Boolean>

    fun createSelectManufacturerDialog()

    fun openFileChooser()
    fun uploadFile()
    fun getPhotoId(): String

    fun backOnMyCabinet()
}