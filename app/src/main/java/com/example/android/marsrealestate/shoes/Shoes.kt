package com.example.android.marsrealestate.shoes

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Shoes(
        val id: Long,
        val userId: String,
        val wordsToSearch: String,
        val manufacturer: String,
        val model: String,
        val city: String,
        val category: String,
        val manufacturerCountry: String,
        val sex: String,
        val typeOfShoes: String,
        val size: String,
        val color: String,
        val productCondition: String,
        val seller: String,
        val productIsNew: Boolean,
        val price: String,
        val description: String,

        val typeOfProduct: String,

        val url: String,

        val latitude: Double,
        val longitude: Double
        
        //val length: String,
        //val width: String,
        //val weight: String
):Parcelable