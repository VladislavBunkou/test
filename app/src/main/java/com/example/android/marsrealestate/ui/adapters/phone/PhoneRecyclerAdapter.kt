package com.example.android.marsrealestate.ui.adapters.phone

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.PhoneItemBinding
import com.example.android.marsrealestate.di.mv.phone.ItemPhoneViewModel
import com.example.android.marsrealestate.model.Phone
import com.example.android.marsrealestate.repository.phone.PhoneItemListener

class PhoneRecyclerAdapter() : RecyclerView.Adapter<PhoneRecyclerAdapter.PhonePropertyviewHolder>() {

    private val phonesList: ArrayList<Phone> = arrayListOf()
    private lateinit var listener: PhoneItemListener

    override fun getItemCount(): Int = phonesList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhonePropertyviewHolder {
        return PhonePropertyviewHolder(PhoneItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: PhonePropertyviewHolder, position: Int) {
        val itemPhoneViewModel = ItemPhoneViewModel()
        itemPhoneViewModel.bind(phonesList[position], listener)
        holder.bind(itemPhoneViewModel)

    }

    fun addItems(list: List<Phone>) {
        this.phonesList.clear()
        this.phonesList.addAll(list)
        notifyDataSetChanged()
    }

    fun addListener(listener: PhoneItemListener){
        this.listener = listener
    }

    class PhonePropertyviewHolder(private var binding: PhoneItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(itemPhoneViewModel: ItemPhoneViewModel) {
            binding.mv = itemPhoneViewModel
            binding.executePendingBindings()
        }

    }
}
