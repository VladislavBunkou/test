package com.example.android.marsrealestate.tablet

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.tablet.model.Tablet
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

enum class TabletApiStatus {
    LOADING, ERROR, DONE
}

class TabletViewModel : ViewModel() {

    val tablets = MutableLiveData<List<Tablet>>()
    val status = MutableLiveData<TabletApiStatus>()
    val navigateToSelectedTablet = MutableLiveData<Tablet>()

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
        getTabletProperties()
    }

    private fun getTabletProperties() {

        coroutineScope.launch {
            var getPropertiesDeferred = RetrofitApi.retrofitApiService.getTabletList()

            try {
                status.value = TabletApiStatus.LOADING
                var listResult = getPropertiesDeferred.await()
                status.value = TabletApiStatus.DONE
                tablets.value = listResult
            } catch (e: Exception) {
                status.value = TabletApiStatus.ERROR
                Log.d("Error dm - > ", "Internet connection not exist or Server is shot down")
                tablets.value = ArrayList()
            }

        }

    }


    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel() // !! Important
    }

    fun displayOneLathe(tablet: Tablet) {
        navigateToSelectedTablet.value = tablet
    }

    fun displayOneLatheComplete() {
        navigateToSelectedTablet.value = null
    }


}