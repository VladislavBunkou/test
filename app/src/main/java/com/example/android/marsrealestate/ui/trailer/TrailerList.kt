package com.example.android.marsrealestate.ui.trailer

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import com.akordirect.vmccnc.databinding.FragmentTrailerListBinding
import com.example.android.marsrealestate.di.mv.trailer.TrailerViewModel
import com.example.android.marsrealestate.repository.trailer.TrailerItemListener
import com.example.android.marsrealestate.trailer.model.Trailer
import com.example.android.marsrealestate.ui.adapters.TrailerRecyclerAdapter



class TrailerList : Fragment(), TrailerItemListener {



    private val viewModel: TrailerViewModel by lazy {
        ViewModelProviders.of(this).get(TrailerViewModel::class.java)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        val binding = FragmentTrailerListBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.listenerTrailer = this
        binding.viewModel = viewModel


        binding.drillingGrid.adapter = TrailerRecyclerAdapter()


        return binding.root
    }

    fun moveToAddTrailer(view: View){
        this.findNavController().navigate(TrailerListDirections.actionTrailerListToNewTrailer())
    }

    override fun onSubUserItemClick(trailer: Trailer) {
        this.findNavController().navigate(TrailerListDirections.actionTrailerListToTrailerDetailFragment(trailer))
    }
}


