package com.example.android.marsrealestate.atv.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.atv.Atv

class AtvOneViewModel(atv: Atv) : ViewModel() {
    private val _selectedProperty = MutableLiveData<Atv>()
    val selectedProperty: LiveData<Atv>
        get() = _selectedProperty

    init {
        _selectedProperty.value = atv
    }
}
