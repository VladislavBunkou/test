package com.example.android.marsrealestate.car

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView


@BindingAdapter("listDataCar")
fun bindRecyclerViewCar(recyclerView: RecyclerView, data: List<Car>?) {
    val adapter = recyclerView.adapter as CarRecyclerAdapter
    adapter.submitList(data)
}
