package com.example.android.marsrealestate.ui.adapters

import androidx.databinding.ObservableField

interface TrailerNavigator{
    fun showToast(model: String)
    fun errorMessage(isModelFilled: Boolean, isCountryFilled: Boolean)

    fun createSelectManufacturerDialog()

    fun openFileChooser()
    fun uploadFile()
    fun getPhotoId(): String

    fun onProductIsNew()
}