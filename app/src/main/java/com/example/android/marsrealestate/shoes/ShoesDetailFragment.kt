package com.example.android.marsrealestate.shoes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentShoesDetailBinding

class ShoesDetailFragment : Fragment(){

    private val viewModel: ShoesDetailViewModel by lazy {
        ViewModelProviders.of(this).get(ShoesDetailViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val application = requireNotNull(activity).application
        val binding =  FragmentShoesDetailBinding.inflate(inflater)

        binding.lifecycleOwner = this


        val shoes = ShoesDetailFragmentArgs.fromBundle(arguments!!).selectedShoesProperty

        val viewModelFactory = ShoesDetailViewModelFactory(shoes, application)

        binding.viewModel = ViewModelProviders.of(this, viewModelFactory).get(ShoesDetailViewModel::class.java)

        return binding.root
    }
}