package com.example.android.marsrealestate.shoes.addShoes

enum class ShoesSizeType (val id: String){
    ONE("1") {override fun getSizeType(): String  = "36"},
    TWO("2"){override fun getSizeType(): String = "37"},
    THREE("3"){override fun getSizeType(): String= "38"},
    FOUR("4"){override fun getSizeType(): String = "39"},
    FIVE("5"){override fun getSizeType(): String= "40"},
    SIX("6"){override fun getSizeType(): String= "41"},
    SEVEN("7"){override fun getSizeType(): String= "42"},
    EIGHT("8"){override fun getSizeType(): String= "43"},

    OTHER("9"){override fun getSizeType(): String  = "OTHER"};

    abstract fun getSizeType(): String
}