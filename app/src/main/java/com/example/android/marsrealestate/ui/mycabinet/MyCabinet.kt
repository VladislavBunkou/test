package com.example.android.marsrealestate.ui.mycabinet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.databinding.FragmentMyCabinetBinding

class MyCabinetFragment : Fragment(), MyCabinetNavigator {

    lateinit var binding: FragmentMyCabinetBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = FragmentMyCabinetBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.listener = this

        return binding.root
    }

    override fun clikToCreatePhone() {
        this.findNavController().navigate(MyCabinetFragmentDirections.actionMyCabinetFragmentToPhoneCreateFragment1())
    }


}


interface MyCabinetNavigator {
    fun clikToCreatePhone()
}