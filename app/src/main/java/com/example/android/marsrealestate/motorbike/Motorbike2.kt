package com.example.android.marsrealestate.motorbike

import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.*
import com.akordirect.vmccnc.databinding.FragmentMotorbike2Binding


class Motorbike2Fragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?  ): View? {

        val application = requireNotNull(activity).application
        val binding = FragmentMotorbike2Binding.inflate(inflater)
        binding.lifecycleOwner = this

        val press = Motorbike2FragmentArgs.fromBundle(arguments!!).selectedElement


        val viewModelFactory = Motorbike2ViewModelFactory(press, application)

        binding.viewModel =
            ViewModelProviders.of( this, viewModelFactory).get(Motorbike2ViewModel::class.java)


        return binding.root
    }
}


// ---- Model ---

class Motorbike2ViewModel(grindingCylindrical: Motorbike, app: Application) : AndroidViewModel(app) {

    private val _selectedProperty = MutableLiveData<Motorbike>()
    val selectedProperty: LiveData<Motorbike>
        get() = _selectedProperty

    init {
        _selectedProperty.value = grindingCylindrical
    }


}



class Motorbike2ViewModelFactory(

        private val grilling: Motorbike,
        private val application: Application ) : ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(Motorbike2ViewModel::class.java)) {
            return Motorbike2ViewModel(grilling, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}
