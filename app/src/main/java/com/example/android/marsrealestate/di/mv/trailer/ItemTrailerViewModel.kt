package com.example.android.marsrealestate.di.mv.trailer

import android.util.Log
import com.example.android.marsrealestate.di.mv.BaseViewModel
import com.example.android.marsrealestate.repository.trailer.TrailerItemListener
import com.example.android.marsrealestate.trailer.model.Trailer


class ItemTrailerViewModel : BaseViewModel<Any>(){

    var listener : TrailerItemListener? = null
    lateinit var trailer: Trailer

    fun bind(trailer: Trailer, listener : TrailerItemListener){
        this.trailer = trailer
        this.listener = listener

    }

    fun onClickItem(){
        Log.d("", "dm-> Is clicked trailer = ${trailer.model}")
        listener?.onSubUserItemClick(trailer)
    }

}