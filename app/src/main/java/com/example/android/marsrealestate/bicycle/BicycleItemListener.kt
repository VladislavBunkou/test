package com.example.android.marsrealestate.bicycle

interface BicycleItemListener{
    fun onSubUserItemClick(s: Bicycle)
}