package com.example.android.marsrealestate.shoes.addShoes

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.Constraints
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentShoesCharacteristicBinding

class ShoesCharateristicFragment : Fragment(){

    lateinit var binding: FragmentShoesCharacteristicBinding

    private val viewModel by viewModels<ShoesCharacteristicViewModel>()

    private lateinit var shoesInfoViewModel: ShoesInfoViewModel

    @SuppressLint("LongLogTag")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val application = requireNotNull(activity).application

        binding = FragmentShoesCharacteristicBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        activity?.let {
            shoesInfoViewModel = ViewModelProviders.of(it).get(ShoesInfoViewModel::class.java)

            viewModel.winterSelected.value = shoesInfoViewModel.winter.get()
            viewModel.springSelected.value = shoesInfoViewModel.spring.get()
            viewModel.fallSelected.value = shoesInfoViewModel.fall.get()
            viewModel.summerSelected.value = shoesInfoViewModel.summer.get()
            viewModel.demiSelected.value = shoesInfoViewModel.demi.get()

            viewModel.productIsNewSelected.value = shoesInfoViewModel.productIsNew.get()

            viewModel.winterSelected.observe(viewLifecycleOwner, Observer {
                shoesInfoViewModel.winter.set(it)
                Log.d(Constraints.TAG, "winter $it ")
            })

            viewModel.springSelected.observe(viewLifecycleOwner, Observer {
                shoesInfoViewModel.spring.set(it)
                Log.d(Constraints.TAG, "spring $it ")
            })

            viewModel.fallSelected.observe(viewLifecycleOwner, Observer {
                shoesInfoViewModel.fall.set(it)
                Log.d(Constraints.TAG, "fall $it ")
            })

            viewModel.summerSelected.observe(viewLifecycleOwner, Observer {
                shoesInfoViewModel.summer.set(it)
                Log.d(Constraints.TAG, "summer $it ")
            })

            viewModel.productIsNewSelected.observe(viewLifecycleOwner, Observer {
                shoesInfoViewModel.productIsNew.set(it)
                Log.d(Constraints.TAG, "product is NEW $it ")
            })


        }
        return binding.root
    }
}