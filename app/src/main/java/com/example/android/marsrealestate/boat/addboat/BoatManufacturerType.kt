package com.example.android.marsrealestate.boat.addboat

enum class BoatManufacturerType (val id: String){
    BIGBOAT("1"){ override fun getBrand(): String = "BigBoat" },
    BRIG("2"){ override fun getBrand(): String = "BRIG" },
    LINDER("3"){ override fun getBrand(): String = "Linder" },
    WALKERBAY("4"){ override fun getBrand(): String = "Walker Bay" };

    abstract fun getBrand(): String
}