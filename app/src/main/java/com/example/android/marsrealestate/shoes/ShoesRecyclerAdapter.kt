package com.example.android.marsrealestate.shoes

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.ItemShoesBinding

class ShoesRecyclerAdapter : RecyclerView.Adapter<ShoesRecyclerAdapter.ShoesPropertyviewHolder>() {

    private val shoesList: ArrayList<Shoes> = arrayListOf()

    private lateinit var listener: ShoesItemListener

    val list:List<Int> = listOf(1,2,3)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShoesPropertyviewHolder {
        return ShoesPropertyviewHolder(ItemShoesBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun getItemCount(): Int = shoesList.size

    override fun onBindViewHolder(holder: ShoesPropertyviewHolder, position: Int) {
        val itemShoesViewModel = ItemShoesViewModel()
        itemShoesViewModel.bind(shoesList[position], listener)
        holder.bind(itemShoesViewModel)
    }

    fun addItems(list: List<Shoes>) {
        this.shoesList.clear()
        this.shoesList.addAll(list)
        notifyDataSetChanged()
    }

    fun addListener(listener: ShoesItemListener){
        this.listener = listener
    }

    class ShoesPropertyviewHolder(private var binding: ItemShoesBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(itemShoesViewModel: ItemShoesViewModel) {
            binding.mv = itemShoesViewModel
            binding.executePendingBindings()
        }

    }
}