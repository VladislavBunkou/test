package com.example.android.marsrealestate.computer.addcomputer

import com.example.android.marsrealestate.di.mv.BaseViewModel

/**
 * Класс ComputerNewViewModel
 * @author Ilya Osipov (mailto:il.osipov.gm@gmail.com)
 * @since 28.04.2020
 * @version $Id$
 */

class ComputerNewViewModel : BaseViewModel<ComputerNavigator>() {
    private val TAG = "ComputerNewViewModel"


}