package com.example.android.marsrealestate.guitar.guitar_list

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.guitar.Guitar
import kotlinx.coroutines.*

enum class GuitarApiStatus {
    LOADING, ERROR, DONE
}

class GuitarListViewModel : ViewModel() {
    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    //current guitar list
    private val _guitars = MutableLiveData<List<Guitar>>()
    val guitars: LiveData<List<Guitar>>
        get() = _guitars

    //current api status (loading, error, done)
    private val _status = MutableLiveData<GuitarApiStatus>()
    val status: LiveData<GuitarApiStatus>
        get() = _status

    //navigation to guitar item
    private val _navigateToSelectedGuitar = MutableLiveData<Guitar>()
    val navigateToSelectedGuitar: LiveData<Guitar>
        get() = _navigateToSelectedGuitar

    init {
        initializeGuitars()
    }

    //get guitar properties
    private fun initializeGuitars() {
        coroutineScope.launch {
            val getPropertiesDeferred = RetrofitApi.retrofitApiService.getGuitarList()
            try {
                _status.value = GuitarApiStatus.LOADING
                val listResult = getPropertiesDeferred.await()
                _status.value = GuitarApiStatus.DONE
                _guitars.value = listResult
            } catch (e: Exception) {
                _status.value = GuitarApiStatus.ERROR
                Log.d("Error dm - > ", "Internet connection not exist or Server is shot down")
                _guitars.value = ArrayList()
            }
        }
    }

    //navigate to guitar
    fun displayDetailGuitar(guitar: Guitar) {
        _navigateToSelectedGuitar.value = guitar
    }

    //reset variable that triggers navigation
    fun displayDetailGuitarComplete() {
        _navigateToSelectedGuitar.value = null
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}