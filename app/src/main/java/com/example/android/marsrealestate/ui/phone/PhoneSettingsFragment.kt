package com.example.android.marsrealestate.ui.phone

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentPhoneSettingsBinding
import com.example.android.marsrealestate.di.mv.phone.PhoneSettingsViewModel
import com.example.android.marsrealestate.di.mv.phone.PhoneInfoViewModel

class PhoneSettingsFragment : Fragment() {
    lateinit var binding: FragmentPhoneSettingsBinding
    private val viewModel by viewModels<PhoneSettingsViewModel>()
    private lateinit var phoneInfoViewModel: PhoneInfoViewModel // for saving data

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentPhoneSettingsBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        activity?.let {
            phoneInfoViewModel = ViewModelProviders.of(it).get(PhoneInfoViewModel::class.java)

            viewModel.is3GSelected.value = phoneInfoViewModel.isPhoneHas3G.get()
            viewModel.is4GSelected.value = phoneInfoViewModel.isPhoneHas4G.get()

            viewModel.is3GSelected.observe(viewLifecycleOwner, Observer {
                phoneInfoViewModel.isPhoneHas3G.set(it)
            })

            viewModel.is4GSelected.observe(viewLifecycleOwner, Observer {
                phoneInfoViewModel.isPhoneHas4G.set(it)
            })
        }

        return binding.root
    }
}