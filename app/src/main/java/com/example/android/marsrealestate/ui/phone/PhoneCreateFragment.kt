package com.example.android.marsrealestate.ui.phone

import android.Manifest
import android.app.Activity
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentPhoneNewBinding
import com.example.android.marsrealestate.di.mv.nav.NavViewModel
import com.example.android.marsrealestate.di.mv.phone.PhoneCreateViewModel
import com.example.android.marsrealestate.di.mv.phone.PhoneFile
import com.example.android.marsrealestate.di.mv.phone.PhoneInfoViewModel
import com.example.android.marsrealestate.ui.adapters.phone.PhoneFileRecyclerAdapter
import com.example.android.marsrealestate.ui.adapters.phone.PhoneNavigator
import com.example.android.marsrealestate.ui.map.MapsActivity
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions
import javax.inject.Inject


@BindingAdapter("profileFilesListBind")
fun addProfileFilesAdapter(recyclerView: RecyclerView, phoneFiles: List<PhoneFile>) {
    val adapter: PhoneFileRecyclerAdapter? = recyclerView.adapter as PhoneFileRecyclerAdapter?
    adapter?.addItems(phoneFiles)
}

@RuntimePermissions // step1
class PhoneCreateFragment : Fragment(), PhoneNavigator , PhoneCreateListener, PhoneFileRecyclerAdapter.PhoneFilesRecyclerAdapterListener{

    lateinit var binding: FragmentPhoneNewBinding

    @Inject
    lateinit var viewModel: PhoneCreateViewModel // Old:   private val viewModel by viewModels<PhoneCreateViewModel>()

    private var profileFilesRecyclerAdapter: PhoneFileRecyclerAdapter = PhoneFileRecyclerAdapter()

    // Where we save data about phone
    private lateinit var phoneInfoViewModel: PhoneInfoViewModel
    private lateinit var navViewModel: NavViewModel

    //For upload image
    lateinit var storage: FirebaseStorage
    lateinit var storageRef: StorageReference
    //    lateinit var mountainsRef: StorageReference
    lateinit var imageView: ImageView
    lateinit var textResult: TextView
//    lateinit var progressBar: ProgressBar


    private var imageUri: Uri = Uri.EMPTY // for saving image
    private var uploadedImageUri: Uri = Uri.EMPTY // for saving image
    private var fileIsSelected = false // true when we choose image


    private lateinit var locationClient: FusedLocationProviderClient
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0

    override fun onAttach(context: Context) {
        super.onAttach(context)
//        (requireActivity() as MyApplication).appComponent.inject(this)
        (activity as PhoneCreateActivity).registrationComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val application = requireNotNull(activity).application

        binding = FragmentPhoneNewBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.listener = this
        viewModel.setNavigator(this) // for easier understanding this code can be deleted

        // // for saving image
        imageView = binding.imageView
        textResult = binding.textViewShowUploads
        storage = FirebaseStorage.getInstance()
        storageRef = storage.reference

        // for save inf from second window
        activity?.let {
            phoneInfoViewModel = ViewModelProviders.of(it).get(PhoneInfoViewModel::class.java)
            navViewModel = ViewModelProviders.of(it).get(NavViewModel::class.java)
            Log.d("PhoneNewFragment", "dm->  navViewModel.id.value = ${navViewModel.id.value}}")
        }

        navViewModel.id.observe(viewLifecycleOwner, Observer {
            if (navViewModel.isCreateNew.value == true)
            viewModel.receivePhoneById(navViewModel.id.value ?: 1, true)
            else  {
                viewModel.receivePhoneById(navViewModel.id.value ?: 1, false)
                navViewModel.isCreateNew.postValue(true)
            }
            Log.d("PhoneNewFragment", "dm->  navViewModel.id.value = ${navViewModel.id.value}}")
        })

//        viewModel.navigateToPhoneCharacteristics.observe(this, Observer {
//            if (it) {
//                this.findNavController().navigate(
//                        PhoneCreateFragmentDirections.actionPhoneCreateFragmentToPhoneSettingsFragment2())
//                viewModel.displaySettingComplete()
//            }
//        })

        locationClient = LocationServices.getFusedLocationProviderClient(this.requireActivity())
        locationClient.lastLocation.addOnSuccessListener {
            this.latitude = latitude
            this.longitude = longitude
        }
        Log.d("PhoneNewFragment", "dm->  latitude = $latitude / longitude = $longitude")

        binding.buttonChooseFile.setOnClickListener {
            preOpenFileChooserWithPermissionCheck() // // step 3 -add this fun (it is genegated!!)
        }

        setAdapter()

        return binding.root
    }


    // step 1
    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
   fun preOpenFileChooser(){
        openFileChooser()
   }

     // step 2  - click 'Make project' (Ctrl+F9)

    // step 4
    @OnPermissionDenied(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun permissionDenied() {
        Log.d("PhoneNewFragment", "dm:: OnPermissionDenied")
    }
    // step 4
    @OnNeverAskAgain(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun neverAskAgain() {
        Log.d("PhoneNewFragment", "dm:: OnNeverAskAgain")
    }



    // for saving image
    override fun openFileChooser() {
        textResult.text = "_"
//        buttonUpload.visibility = View.VISIBLE

        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, viewModel.PICK_IMAGE_REQUEST)
        fileIsSelected = true
//        viewModel.addFileItem(ProfileFile(ProfileFileType.JPEG_TYPE, null, photoPaths[0]))
    }

    // for saving image
    private fun getFileExtension(uri: Uri): String? {
        val cR: ContentResolver = activity!!.contentResolver
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(cR.getType(uri))
    }

    // for saving image
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == viewModel.PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK
                && data?.data != null) {
            imageUri = data.data!!
            Picasso.get().load(imageUri).into(imageView)
        }
    }

    // for saving image
    override fun uploadFile() {
        if (imageUri != null) {
            val fileReference: StorageReference = storageRef.child(
//            У каждого этот путь будет разный
                    "image/phone/${System.currentTimeMillis()}.${getFileExtension(imageUri)}"
//              Например: "image/bicycle/${System.currentTimeMillis()}.${getFileExtension(imageUri)}"
//              Например: "image/tablet/${System.currentTimeMillis()}.${getFileExtension(imageUri)}"
//              Например: "image/guitar/${System.currentTimeMillis()}.${getFileExtension(imageUri)}"
//              Например: "image/atv/${System.currentTimeMillis()}.${getFileExtension(imageUri)}"
//              Например: "image/car/${System.currentTimeMillis()}.${getFileExtension(imageUri)}"
//              Например: "image/motorbike/${System.currentTimeMillis()}.${getFileExtension(imageUri)}"
            )

            val uploadTask = fileReference.putFile(imageUri)

//                .addOnSuccessListener {
                    .addOnCompleteListener() {
                        val handler = Handler()
                        handler.postDelayed({ binding.progressBar.progress = 0 }, 500)
                        Toast.makeText(context, "Upload successful", Toast.LENGTH_LONG).show()
//                    val upload = UploadImage(
//                        editText.text.toString().trim(),
//                        it.result.toString()
//                    )

                        Picasso.get().load(R.drawable.ic_image_is_uploded).into(imageView)
//                    buttonUpload.visibility = View.INVISIBLE
                        textResult.text = "OK!"
                        textResult.setTextColor(Color.GREEN)
//                    val uploadId: String = storageRef
//                    storageRef.child(uploadId).setValue(upload)
                    }

                    .addOnFailureListener { e ->
                        Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
                    }

                    .addOnProgressListener {
                        val progress = 100.0 * it.bytesTransferred / it.totalByteCount
                        binding.progressBar.setProgress(progress.toInt())
                    }

            val urlTask = uploadTask.continueWithTask {
                if (!it.isSuccessful) {
                    it.exception?.let { ex ->
                        throw ex
                    }
                }
                fileReference.downloadUrl
            }
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            uploadedImageUri = it.result ?: Uri.EMPTY
                            Toast.makeText(context, "Url: $uploadedImageUri", Toast.LENGTH_SHORT).show()
                            Log.d("dm->2", "Url: $uploadedImageUri")
                            textResult.text = "1) Image is uploaded!"
                        } else {
                            // Handle failures
                            Toast.makeText(context, "something wrong!!!", Toast.LENGTH_SHORT).show()
                        }
                    }

        } else {
            Toast.makeText(context, "No file selected", Toast.LENGTH_SHORT).show()
        }
    } // End uploadFile


    override fun showToast(str: String) {  // for easier understanding this code can be deleted
        Toast.makeText(context, "model: $str, country: $str", Toast.LENGTH_LONG).show()
    }

    override fun errorMessages(isModelFilled: Boolean, isCountryFilled: Boolean) {

        if (!isModelFilled) {
            binding.phoneModelLayout.isErrorEnabled = true
            binding.phoneModelLayout.error = "Enter model"
        } else {
            binding.phoneModelLayout.isErrorEnabled = false
        }

        if (!isCountryFilled) {
            binding.phoneCountryLayout.isErrorEnabled = true
            binding.phoneCountryLayout.error = "Enter country"
        } else {
            binding.phoneCountryLayout.isErrorEnabled = false
        }

    }


    override fun get3G() = phoneInfoViewModel.isPhoneHas3G
    override fun get4G() = phoneInfoViewModel.isPhoneHas4G
    override fun getPhotoId() = uploadedImageUri.toString()
    override fun backOnMyCabinet() {
        Toast.makeText(context, "should back On My Cabinet", Toast.LENGTH_SHORT).show()
//        this.findNavController().navigate(PhoneCreateFragmentDirections.actionPhoneCreateFragment1ToMyCabinetFragment())
}

    private var currentSelectedManufacturerIndex = -1

    override fun createSelectManufacturerDialog() {
        val listCategory = listOf("Apple", "Xiomi", "HTC", "Other")

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setSingleChoiceItems(listCategory.toTypedArray(),
                        currentSelectedManufacturerIndex) { dialog, which ->
                    currentSelectedManufacturerIndex = which
                    viewModel.updateManufacturer(which + 1)
                    dialog.dismiss()
                }
                setTitle("Phone producer")
                setNegativeButton("Cancel") { _, _ ->
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }

    override fun goToGoogleMap() {
        Log.d("PhoneCreateFragment", "dm:: onClickCreatePhone")
        val intent = Intent(activity, MapsActivity::class.java)
        startActivity(intent)
    }


// ---------------- /// -----------------
    private fun setAdapter() {
        profileFilesRecyclerAdapter.setPhoneFileRecyclerAdapterListener(this)
//        binding.profileFilesRecycler.layoutManager = FlexboxLayoutManager(context)
        binding.profileFilesRecycler.layoutManager = LinearLayoutManager(context)
        binding.profileFilesRecycler.adapter = profileFilesRecyclerAdapter
    }


    override fun onAddFile() {
//        if (viewModel.phoneFilesList.size < (2)) openChooseFileDialogWithPermissionCheck() // Note: the first one is used for sign "+"
        if (viewModel.phoneFilesList.size < (4)) {
            openFileChooser()

        } // Note: the first one is used for sign "+"
        else {
            showToast("Possoble not more than 3 files")
        }
    }

    override fun onRemoveFile(ticketFile: PhoneFile) {
        viewModel.removeFileItem(ticketFile)
    }


}






interface  PhoneCreateListener {
    fun goToGoogleMap()
}

