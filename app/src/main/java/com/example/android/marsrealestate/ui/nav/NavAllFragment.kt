package com.example.android.marsrealestate.ui.nav

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.databinding.FragmentNavAllBinding

class NavAllFragment : Fragment(), NavAllListener {
    private val TAG = "NavMusicFragment"


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentNavAllBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.navAllListener  = this
        return binding.root
    }

    override fun onClickAuto() {
        this.findNavController().navigate(NavAllFragmentDirections.actionNavAllFragmentToNavAutoFragment())
    }

    override fun onClickElectric() {
        this.findNavController().navigate(NavAllFragmentDirections.actionNavAllFragmentToNavElectronikFragment())
    }

    override fun onClickDress()   {
        this.findNavController().navigate(NavAllFragmentDirections.actionNavAllFragmentToNavClothesFragment())
    }

    override fun onClickGuitars() {
        this.findNavController().navigate(NavAllFragmentDirections.actionNavAllFragmentToNavMusicFragment())
    }

}


interface NavAllListener {
    fun onClickAuto()
    fun onClickElectric()
    fun onClickDress()
    fun onClickGuitars()
}