package com.example.android.marsrealestate.ui.trailer

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.*

import com.akordirect.vmccnc.databinding.FragmentTrailerDetailBinding
import com.example.android.marsrealestate.di.mv.trailer.TrailerDetailViewModel
import com.example.android.marsrealestate.di.mv.trailer.TrailerViewModelFactory

class TrailerDetailFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding = FragmentTrailerDetailBinding.inflate(inflater)
        val application = requireNotNull(activity).application

        binding.lifecycleOwner = this

        val press = TrailerDetailFragmentArgs.fromBundle(arguments!!).selectedTrailer

        val viewModelFactory = TrailerViewModelFactory(press, application)

        binding.viewModel =
                ViewModelProviders.of(this, viewModelFactory).get(TrailerDetailViewModel::class.java)

        return binding.root
    }

}


