package com.example.android.marsrealestate.guitar.guitar_detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.guitar.Guitar

class GuitarDetailViewModel(guitar: Guitar) : ViewModel() {
    private val _currentGuitar = MutableLiveData<Guitar>()
    val currentGuitar: LiveData<Guitar>
        get() = _currentGuitar

    init {
        _currentGuitar.value = guitar
    }
}