package com.example.android.marsrealestate.bicycle.addNewBicycle

import android.Manifest
import android.app.Activity
import android.content.ContentResolver
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.webkit.PermissionRequest
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentAddBicycleBinding
import com.example.android.marsrealestate.di.mv.nav.NavViewModel
import com.example.android.marsrealestate.ui.authentification.LoginFragment.Companion.TAG
import com.example.android.marsrealestate.ui.dress.d_latitude
import com.example.android.marsrealestate.ui.dress.d_longitude
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
class AddBicycleFragment: Fragment(), BicycleNavigator{

lateinit var binding: FragmentAddBicycleBinding

    private val viewModel by viewModels<AddBicycleViewModel>()
    private lateinit var bicycleInfoViewModel: BicycleInfoViewModel
    private lateinit var navViewModel: NavViewModel


    //LOCATION
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    //For upload image
    lateinit var storage: FirebaseStorage
    lateinit var storageRef: StorageReference

    lateinit var imageView: ImageView
    lateinit var textResult: TextView

    private var imageUri: Uri = Uri.EMPTY // for saving image
    private var uploadedImageUri: Uri = Uri.EMPTY // for saving image
    private var fileIsSelected = false // true when we choose image

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val application = requireNotNull(activity).application

        binding = FragmentAddBicycleBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        viewModel.setNavigator(this)


        //LOCATION
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.requireActivity())
        //getLastLocation()

        this.fusedLocationClient.lastLocation
                .addOnSuccessListener { location->
                    if (location != null) {
                       d_latitude =  location.latitude
                       d_longitude = location.longitude
                        Log.d(TAG,"AddBicycleFragment: km->Location  latitude=${com.example.android.marsrealestate.ui.dress.d_latitude}, longitude=${com.example.android.marsrealestate.ui.dress.d_longitude}")
                    }
                    else if (location == null){
                        Log.d(TAG,"AddBicycleFragment: km->Location IS NULL latitude=${com.example.android.marsrealestate.ui.dress.d_latitude}, longitude=${com.example.android.marsrealestate.ui.dress.d_longitude}")
                    }

                }

        // // for saving image
        imageView = binding.imageView
        textResult = binding.textViewShowUploads
        storage = FirebaseStorage.getInstance()
        storageRef = storage.reference

        activity?.let {
            bicycleInfoViewModel = ViewModelProviders.of(it).get(BicycleInfoViewModel::class.java)
            navViewModel = ViewModelProviders.of(it).get(NavViewModel::class.java)
        }
        navViewModel.id.observe(this, Observer {
            if (navViewModel.isCreateNew.value == true)
                viewModel.receiveBicycleById(navViewModel.id.value ?: 1, true)
            else  {
                viewModel.receiveBicycleById(navViewModel.id.value ?: 1, false)
                navViewModel.isCreateNew.postValue(true)
            }
            Log.d("AddBicycleFragment", "navViewModel.id.value = ${navViewModel.id.value}}")
        })
        viewModel.navigateToBicycleCharacteristics.observe(this, Observer {
            if (it) {
                this.findNavController().navigate(
                        AddBicycleFragmentDirections.actionAddBicycleFragmentToBicycleCharacteristicFragment())
                viewModel.displaySettingComplete()
            }
        })

        binding.buttonChooseFile.setOnClickListener {
            preOpenFileChooserWithPermissionCheck() // // step 3 -add this fun (it is genegated!!)
            //odtieneLocationWithPermissionCheck()
        }

        return binding.root
    }

    @NeedsPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            /*android.Manifest.permission.LOCATION_HARDWARE*/)
    fun preOpenFileChooser(){
        openFileChooser()
    }

    // step 4
    @OnPermissionDenied(android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            /*android.Manifest.permission.LOCATION_HARDWARE*/)
    fun permissionDenied() {
        Log.d("AddDressFragment", "dm:: OnPermissionDenied")
    }
    // step 4
    @OnNeverAskAgain(android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            /*android.Manifest.permission.LOCATION_HARDWARE*/)
    fun neverAskAgain() {
        Log.d("AddDressFragment", "dm:: OnNeverAskAgain")
    }



    override fun openFileChooser() {
        textResult.text = "_"

        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, viewModel.PICK_IMAGE_REQUEST)
        fileIsSelected = true
    }
    // for saving image
    private fun getFileExtension(uri: Uri): String? {
        val cR: ContentResolver = activity!!.contentResolver
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(cR.getType(uri))
    }

    // for saving image
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == viewModel.PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK
                && data?.data != null) {
            imageUri = data.data!!
            Picasso.get().load(imageUri).into(imageView)
        }
    }

    override fun uploadFile() {
        if (imageUri != null) {
            val fileReference: StorageReference = storageRef.child(

                    "image/bicycle/${System.currentTimeMillis()}.${getFileExtension(imageUri)}"
            )

            val uploadTask = fileReference.putFile(imageUri)
                    .addOnCompleteListener() {
                        val handler = Handler()
                        handler.postDelayed({ binding.progressBar.progress = 0 }, 500)
                        Toast.makeText(context, "Upload successful", Toast.LENGTH_LONG).show()

                        Picasso.get().load(R.drawable.ic_image_is_uploded).into(imageView)

                        textResult.text = "OK!"
                        textResult.setTextColor(Color.GREEN)
                    }

                    .addOnFailureListener { e ->
                        Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
                    }

                    .addOnProgressListener {
                        val progress = 100.0 * it.bytesTransferred / it.totalByteCount
                        binding.progressBar.setProgress(progress.toInt())
                    }

            val urlTask = uploadTask.continueWithTask {
                        if (!it.isSuccessful) {
                            it.exception?.let { ex ->
                                throw ex
                            }
                        }
                        fileReference.downloadUrl
                    }
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            uploadedImageUri = it.result ?: Uri.EMPTY
                            Toast.makeText(context, "Url: $uploadedImageUri", Toast.LENGTH_SHORT).show()
                            Log.d("km->", "Url: $uploadedImageUri")
                            textResult.text = "1) Image is uploaded!"
                        } else {
                            // Handle failures
                            Toast.makeText(context, "something wrong!!!", Toast.LENGTH_SHORT).show()
                        }
                    }

        } else {
            Toast.makeText(context, "No file selected", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getPhotoId() = uploadedImageUri.toString()

    override fun showToast(model: String) {
        Toast.makeText(context, "model: $model", Toast.LENGTH_LONG).show()
    }

    override fun errorMessages(isModelFilled: Boolean, isManufacturerFilled: Boolean, isPriceFilled: Boolean) {
        if (!isModelFilled) {
            binding.bicycleModelLayout.isErrorEnabled = true
            binding.bicycleModelLayout.error = "Enter model"
        } else {
            binding.bicycleModelLayout.isErrorEnabled = false
        }


        if (!isPriceFilled) {
            binding.bicyclePriceLayout.isErrorEnabled = true
            binding.bicyclePriceLayout.error = "Enter price"
        } else {
            binding.bicyclePriceLayout.isErrorEnabled = false
        }

    }


    override fun getFolding() = bicycleInfoViewModel.folding
    override fun getMountain() = bicycleInfoViewModel.mountain
    override fun getCitybike() = bicycleInfoViewModel.citybike
    override fun getRoad() = bicycleInfoViewModel.road
    override fun getHibrid() = bicycleInfoViewModel.hibrid
    override fun getProductIsNew()=bicycleInfoViewModel.productIsNew

    private var currentSelectedManufacturerIndex = -1

    override fun createSelectManufacturerDialog() {
        val listCategory = listOf("Giant", "Trek", "Merida", "Scott", "Cube")

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setSingleChoiceItems(listCategory.toTypedArray(),
                        currentSelectedManufacturerIndex) { dialog, which ->
                    currentSelectedManufacturerIndex = which
                    viewModel.updateManufacturer(which + 1)
                    dialog.dismiss()
                }
                setTitle("Bicycle producer")
                setNegativeButton("Cancel") { _, _ ->
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }


}