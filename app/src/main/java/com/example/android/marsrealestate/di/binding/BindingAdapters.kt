package com.example.android.marsrealestate.di.binding

import android.view.View
import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.android.marsrealestate.car.Car
import com.example.android.marsrealestate.car.CarRecyclerAdapter
import com.example.android.marsrealestate.di.mv.main.MainApiStatus
import com.example.android.marsrealestate.di.mv.mycabinet.MarsApiStatus
import com.example.android.marsrealestate.ui.adapters.mycabinet.MyCabinetAdapter
import com.example.android.marsrealestate.model.Prod
import com.example.android.marsrealestate.ui.main.CategoriesAdapter


@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<Prod>?) {
    val adapter = recyclerView.adapter as MyCabinetAdapter
    adapter.submitList(data)
}


@BindingAdapter("listCategoriesBind")
fun bindRecyclerViewListCategoriesBind(recyclerView: RecyclerView, data: List<String>) {
    val adapter = recyclerView.adapter as CategoriesAdapter
    adapter.addItems(data)
}


@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        val imgUri = imgUrl.toUri().buildUpon().scheme("https").build()
        Glide.with(imgView.context)
                .load(imgUri)
                .apply(RequestOptions()
                        .placeholder(R.drawable.loading_animation)
                        .error(R.drawable.ic_broken_image))
                .into(imgView)
    }
}


@BindingAdapter("marsApiStatus")
fun bindStatus(statusImageView: ImageView, status: MarsApiStatus?) {
    when (status) {
        MarsApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        MarsApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        MarsApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}


@BindingAdapter("mainApiStatus")
fun bindMainStatus(statusImageView: ImageView, status: MainApiStatus?) {
    when (status) {
        MainApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        MainApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        MainApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}

@BindingAdapter("imageUrlLathe")
fun bindImageLathe(imgView: ImageView, imgUrl: String?) {

    imgUrl?.let {
        val imgUri = imgUrl.toUri().buildUpon().scheme("http").build()

        Glide.with(imgView.context)
                .load(imgUri)
                .apply(RequestOptions()
                        .placeholder(R.drawable.loading_animation)
                        .error(R.drawable.ic_broken_image))
                .into(imgView)
    }
}


@BindingAdapter("listDataCar")
fun bindRecyclerViewCar(recyclerView: RecyclerView, data: List<Car>?) {
    val adapter = recyclerView.adapter as CarRecyclerAdapter
    adapter.submitList(data)
}