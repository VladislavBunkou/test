package com.example.android.marsrealestate.bicycle.addNewBicycle

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.bicycle.BicycleSaveInfo
import com.example.android.marsrealestate.di.mv.BaseViewModel
import com.example.android.marsrealestate.enums.Status
import com.example.android.marsrealestate.ui.dress.d_latitude
import com.example.android.marsrealestate.ui.dress.d_longitude
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.*

class AddBicycleViewModel: BaseViewModel<BicycleNavigator>(){

    private val TAG = "AddBicycleViewModel"

    lateinit var bicycleRepository: BicycleRepository

    var latitudeDress = d_latitude
    var longtitudeDress = d_longitude

    var modelBicycle = ObservableField<String>("")
    var manufacturerBicycle = ObservableField<String>("")
    var wordsToSearchBicycle = ObservableField<String>("")
    var cityBicycle = ObservableField<String>("")
    var speciesBicycle = ObservableField<String>("")
    var priceBicycle = ObservableField<String>("")
    var sellerBicycle = ObservableField<String>("")
    var productConditionBicycle = ObservableField<String>("")
    var descriptionBicycle = ObservableField<String>("")

    val saveBicycleObservable = MutableLiveData<BicycleSaveInfo>() // replace to Repository

    var navigateToBicycleCharacteristics = MutableLiveData<Boolean>(false)

    private var manufacturerType: BicycleManufacturerType? = null

    var PICK_IMAGE_REQUEST: Int = 1

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val saveBicycleObserver = Observer<BicycleSaveInfo> {
        when (it.status) {
            Status.INITIAL, Status.LOADING -> {
                setIsLoading(true)
                Log.d("AddBicycleViewModel", "Status.Loading")
            }
            Status.SUCCESS -> {
                setIsLoading(false)
                Log.d("AddBicycleViewModel", "Status.SUCCESS")
            }
            Status.ERROR -> {
                setIsLoading(false)
                Log.d("AddBicycleViewModel", "Status.Error")
            }
            Status.ERROR_CONNECTION -> {
                setIsLoading(false)
            }
            else -> {
            }
        }
    }


    init {
        saveBicycleObservable.observeForever(saveBicycleObserver)
        bicycleRepository = BicycleRepository(coroutineScope)

        bicycleRepository.modelBicycleObservable.observeForever(Observer {modelBicycle.set(it) })
        bicycleRepository.manufacturerBicycleObservable.observeForever(Observer {manufacturerBicycle.set(it) })
        bicycleRepository.wordsToSearchBicycleObservable.observeForever(Observer {wordsToSearchBicycle.set(it) })
        bicycleRepository.cityBicycleObservable.observeForever(Observer { cityBicycle.set(it) })
        bicycleRepository.speciesBicycleObservable.observeForever(Observer { speciesBicycle.set(it) })
        bicycleRepository.priceBicycleObservable.observeForever(Observer { priceBicycle.set(it) })
        bicycleRepository.sellerBicycleObservable.observeForever(Observer { sellerBicycle.set(it) })
        bicycleRepository.productConditionBicycleObservable.observeForever(Observer { productConditionBicycle.set(it) })
        bicycleRepository.descriptionBicycleObservable.observeForever(Observer {descriptionBicycle.set(it) })

    }

    fun receiveBicycleById(id: Long, isSaveNewBicycleOnServer: Boolean) {
        bicycleRepository.receiveBicycleById(id, isSaveNewBicycleOnServer)
    }


    fun onSendButton() {

        if (allFilldsIGood()) {
            val idUserFirebase = if (FirebaseAuth.getInstance().currentUser != null) FirebaseAuth.getInstance().currentUser?.uid else "-"
            Log.d(TAG, "dm-> idUserFirebase = $idUserFirebase")
            val forSaveBicycle = ForSaveBicycle().apply {
                userId = idUserFirebase ?: ""
                model = modelBicycle.get() ?: ""
                manufacturer = manufacturerBicycle.get() ?: ""
                wordToSearch=wordsToSearchBicycle.get() ?:""
                city = cityBicycle.get() ?:""
                species = speciesBicycle.get() ?:""
                price = priceBicycle.get() ?:""
                seller = sellerBicycle.get()?:""
                productCondition = productConditionBicycle.get()?:""
                description = descriptionBicycle.get() ?: ""

            imageId = getNavigator()?.getPhotoId() ?: ""

            folding = getNavigator()?.getFolding()?.get() ?: false
            mountain = getNavigator()?.getMountain()?.get() ?: false
            road = getNavigator()?.getRoad()?.get() ?: false
            citybike = getNavigator()?.getCitybike()?.get() ?: false
            hibrid = getNavigator()?.getHibrid()?.get() ?: false

            productIsNew = getNavigator()?.getProductIsNew()?.get() ?: false

                latitude = d_latitude
                longitude = d_longitude

                Log.d(TAG,"AddBicycleViewModel: km->Location  latitude=$latitude, lontitude=$longitude")

                Log.d(TAG, "km-> saved $model")
        }

            saveBicycleWithDeferred(forSaveBicycle)

            getNavigator()?.showToast(model = "Bicycle ${forSaveBicycle.model} is saved!")

            //userIdBicycle.set("")
            modelBicycle.set("")
            wordsToSearchBicycle.set("")
            manufacturerBicycle.set("")
            speciesBicycle.set("")
            cityBicycle.set("")
            priceBicycle.set("")
            sellerBicycle.set("")
            productConditionBicycle.set("")
            descriptionBicycle.set("")

        } else {
            getNavigator()?.showToast(model = "Bicycle not Save !!")
        }


    }



    private fun saveBicycleWithDeferred(forSaveBicycle: ForSaveBicycle) {

        saveBicycleObservable.postValue(BicycleSaveInfo(Status.LOADING))

        coroutineScope.launch {
            var getPropertiesDeferred = RetrofitApi.retrofitApiService.saveBicycle(forSaveBicycle)
            try {
                Log.d(TAG, "all good")
                var bicycleResponse= getPropertiesDeferred.await()
                Log.d(TAG, "code->${bicycleResponse}")
                if(bicycleResponse.isSuccessful){
                    saveBicycleObservable.postValue(BicycleSaveInfo(Status.SUCCESS))
                }else{
                    saveBicycleObservable.postValue(BicycleSaveInfo(Status.ERROR))
                }
            } catch (e: Exception) {

                Log.d(TAG, "Error :: ${e.message}")
                saveBicycleObservable.postValue(BicycleSaveInfo(Status.ERROR_CONNECTION))
            }
        }
    }

    fun allFilldsIGood(): Boolean {
        var isModelFilled = false
        var isManufacturerFilled = false
        var isPriceFilled = false

        if (!modelBicycle.get().isNullOrEmpty()) isModelFilled = true
        if (!manufacturerBicycle.get().isNullOrEmpty()) isManufacturerFilled = true
        if (!priceBicycle.get().isNullOrEmpty()) isPriceFilled = true


        if (!isModelFilled || !isManufacturerFilled || !isPriceFilled) {
            getNavigator()?.errorMessages(isModelFilled, isManufacturerFilled, isPriceFilled)
            return false
        } else {
            return true
        }

    }

    fun updateManufacturer(selectedIndex:  Int){
        manufacturerType = when(selectedIndex.toString()){
            BicycleManufacturerType.GIANT.id -> BicycleManufacturerType.GIANT
            BicycleManufacturerType.TREK.id -> BicycleManufacturerType.TREK
            BicycleManufacturerType.MERIDA.id -> BicycleManufacturerType.MERIDA
            BicycleManufacturerType.SCOTT.id -> BicycleManufacturerType.SCOTT
            BicycleManufacturerType.CUBE.id -> BicycleManufacturerType.CUBE
            else -> BicycleManufacturerType.OTHER
        }
        manufacturerBicycle.set(manufacturerType?.getBrand() ?: "")
    }

    fun onBicycleManufacturerClick() {
        getNavigator()?.createSelectManufacturerDialog()
    }

    fun openFileChooser() = getNavigator()?.openFileChooser()
    fun uploadFile() = getNavigator()?.uploadFile()

    fun onCharacteristicsClick() {
        displaySetting()
    }

    fun displaySetting() {
        navigateToBicycleCharacteristics.value = true
    }

    fun displaySettingComplete() {
        navigateToBicycleCharacteristics.value = false
    }

   }

