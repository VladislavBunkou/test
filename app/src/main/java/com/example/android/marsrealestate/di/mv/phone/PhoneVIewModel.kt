package com.example.android.marsrealestate.di.mv.phone

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.android.marsrealestate.di.mv.BaseViewModel
import com.example.android.marsrealestate.enums.Status
import com.example.android.marsrealestate.model.Phone
import com.example.android.marsrealestate.repository.phone.PhoneGetInfo
import com.example.android.marsrealestate.repository.phone.PhoneRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import javax.inject.Inject


enum class PhoneApiStatus {
    LOADING, ERROR, DONE
}

class PhoneViewModel @Inject constructor() : BaseViewModel<Any>() {


    val phonesListObservable = ObservableArrayList<Phone>()



//    val status: LiveData<PhoneApiStatus>
//        get() = _status
//    private val _status = MutableLiveData<PhoneApiStatus>()


    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)


    private val allPhonsObserver = Observer<List<Phone>> { list ->
        list?.let {
            phonesListObservable.run {
                Log.d("PhoneViewModel", "dm-> 22 this.size = ${this.size}")
                clear()
                addAll(list)
            }
        }
    }

    private val receivePhoneObserver = Observer<PhoneGetInfo> {
        when (it.status) {
            Status.INITIAL, Status.LOADING -> {
                setIsLoading(true)
                Log.d("PhoneNewViewModel", "dm-> Status.Loading")
            }
            Status.SUCCESS -> {
                setIsLoading(false)
                Log.d("PhoneNewViewModel", "dm-> Status.SUCCESS")
            }
            Status.ERROR -> {
                setIsLoading(false)
                Log.d("PhoneNewViewModel", "dm-> Status.Error")
            }
            Status.ERROR_CONNECTION -> {
                setIsLoading(false)
            }
            else -> {
            }
        }
    }


    init {
        val phoneRepository = PhoneRepository(coroutineScope)
        phoneRepository.phones.observeForever(allPhonsObserver)
        phoneRepository.receivePhoneObservable.observeForever(receivePhoneObserver)
        phoneRepository.getPhonesList()
    }


    fun testRefresh(){
        Log.d("PhoneViewModel", "dm-> ttt REFRESH")
    }




    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel() // !! Important
    }


}