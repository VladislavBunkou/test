package com.example.android.marsrealestate.dress

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.R
import com.example.android.marsrealestate.ui.adapters.dress.DressRecyclerAdapter


@BindingAdapter("listDataDress", "listenerDress")
fun bindRecyclerViewDress(recyclerView: RecyclerView, listDataDress: List<Dress>, listenerDress: DressItemListener) {
    val adapter = recyclerView.adapter as DressRecyclerAdapter
   adapter.addItems(listDataDress)
   adapter.addListener(listenerDress)
    //adapter.submitList(listDataDress)

}

@BindingAdapter("dressApiStatus")
fun bindStatusPhoto(statusImageView: ImageView, status: DressApiStatus?) {
    when (status) {
        DressApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        DressApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        DressApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}