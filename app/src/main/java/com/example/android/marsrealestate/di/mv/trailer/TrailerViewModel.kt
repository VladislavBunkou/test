package com.example.android.marsrealestate.di.mv.trailer

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.android.marsrealestate.di.mv.BaseViewModel
import com.example.android.marsrealestate.enums.Status
import com.example.android.marsrealestate.repository.trailer.TrailerGetInfo
import com.example.android.marsrealestate.repository.trailer.TrailerRepository
import com.example.android.marsrealestate.trailer.model.Trailer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job


enum class TrailerApiStatus {
    LOADING, DONE, ERROR
}

class TrailerViewModel : BaseViewModel<Any>() {


    val trailerListObservable = ObservableArrayList<Trailer>()

//    val phones: LiveData<List<Phone>>
//        get() = _phones
//    private val _phones = MutableLiveData<List<Phone>>()

    val status: LiveData<TrailerApiStatus>
        get() = _status
    private val _status = MutableLiveData<TrailerApiStatus>()


//    val navigateToSelectedPhone: LiveData<Phone> get() = _navigateToSelectedPhone
//    private val _navigateToSelectedPhone = MutableLiveData<Phone>()


    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)



    private val allTrailersObserver = Observer<List<Trailer>> {list ->
        list?.let {
            trailerListObservable.run {
                Log.d("TrailerViewModel", "dm-> 22 this.size = ${this.size}")
                clear()
                addAll(list)
            }
        }
    }

    private val receiveTrailerObserver = Observer<TrailerGetInfo> {
        when (it.status) {
            Status.INITIAL, Status.LOADING -> {
                setIsLoading(true)
                Log.d("TrailerNewViewModel", "dm-> Status.Loading")
            }
            Status.SUCCESS -> {
                setIsLoading(false)
                Log.d("TrailerNewViewModel", "dm-> Status.SUCCESS")
            }
            Status.ERROR -> {
                setIsLoading(false)
                Log.d("TrailerNewViewModel", "dm-> Status.Error")
            }
            Status.ERROR_CONNECTION -> {
                setIsLoading(false)
            }
            else -> {
            }
        }
    }


    init {


        Log.d("TrailerViewModel", "dm-> 2")
        val trailerRepository = TrailerRepository(coroutineScope)
        trailerRepository.trailers.observeForever(allTrailersObserver)
        trailerRepository.receiveTrailerObservable.observeForever(receiveTrailerObserver)
        trailerRepository.getTrailerList()
//        getTrailerProperties()
    }

//    private fun getPhoneProperties() {
//
//        coroutineScope.launch {
//
//            var getPropertiesDeferred = RetrofitApi.retrofitApiService.getPhoneList()
//
//            try {
//                _status.value = PhoneApiStatus.LOADING
//                var listResult = getPropertiesDeferred.await()
//                _status.value = PhoneApiStatus.DONE
//                _phones.value = listResult
//            } catch (e: Exception) {
//                _status.value = PhoneApiStatus.ERROR
//                Log.d("Error dm - > ", "Internet connection not exist or Server is shot down")
//                _phones.value = ArrayList()
//            }
//
//        }
//
//    }

    fun testRefresh(){
        Log.d("TrailerViewModel", "dm-> ttt REFRESH")
    }


//    fun displayOneVmc(vmcProperty: Phone) {
//        _navigateToSelectedPhone.value = vmcProperty
//    }
//
//    fun displayOneVmcComplete() {
//        _navigateToSelectedPhone.value = null
//    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel() // !! Important
    }
}


