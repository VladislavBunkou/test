package com.example.android.marsrealestate.bicycle

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

import com.example.android.marsrealestate.bicycle.addNewBicycle.BicycleRepository
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.di.mv.BaseViewModel
import com.example.android.marsrealestate.enums.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job


class BicycleViewModel : BaseViewModel<Any>(){

    val bicycleListObservable = ObservableArrayList<Bicycle>()

    //status
    private val _status = MutableLiveData<BicycleApiStatus>()

    val status: LiveData<BicycleApiStatus>
        get() = _status


    private var viewModelJob = Job()

    //coroutine
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main )

    private val allBicycleObserver = Observer<List<Bicycle>> { list ->
        list?.let {
            bicycleListObservable.run {
                Log.d("BicycleViewModel", "22 this.size = ${this.size}")
                clear()
                addAll(list)
            }
        }
    }

    private val receiveBicycleObserver = Observer<BicycleGetInfo> {
        when (it.status) {
            Status.INITIAL, Status.LOADING -> {
                setIsLoading(true)
                Log.d("BicycleViewModel", "Status.Loading")
            }
            Status.SUCCESS -> {
                setIsLoading(false)
                Log.d("BicycleViewModel", "Status.SUCCESS")
            }
            Status.ERROR -> {
                setIsLoading(false)
                Log.d("BicycleViewModel", "Status.Error")
            }
            Status.ERROR_CONNECTION -> {
                setIsLoading(false)
            }
            else -> {
            }
        }
    }


    init {
        val bicycleRepository = BicycleRepository(coroutineScope)
        bicycleRepository.bicycle.observeForever(allBicycleObserver)
        bicycleRepository.receiveBicycleObservable.observeForever(receiveBicycleObserver)
        bicycleRepository.getBicycleList()
        Log.d("BicycleViewModel", "init")
    }

     override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun testRefresh(){
        Log.d("BicycleViewModel", "REFRESH")
    }

}