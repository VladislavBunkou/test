package com.example.android.marsrealestate.di.mv.phone

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.android.marsrealestate.model.Phone

class PhoneDetailViewModel(grindingCylindrical: Phone, app: Application) : AndroidViewModel(app) {
    val selectedProperty  = MutableLiveData<Phone>()
    init {
        selectedProperty.value = grindingCylindrical
    }
}



class PhoneDetailViewModelFactory(private val grilling: Phone,
                                  private val application: Application) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PhoneDetailViewModel::class.java)) {
            return PhoneDetailViewModel(grilling, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}
