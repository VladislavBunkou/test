package com.example.android.marsrealestate.guitar.guitar_add

import android.app.Activity
import android.content.ContentResolver
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentGuitarAddBinding
import com.example.android.marsrealestate.guitar.Guitar
import com.example.android.marsrealestate.guitar.guitar_add.guitar_enum.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso

//fragment which pushes new guitar to server
class GuitarAddFragment : Fragment(), GuitarAddNavigator {

    lateinit var binding: FragmentGuitarAddBinding

    private val viewModel: GuitarAddViewModel by lazy {
        ViewModelProviders.of(this).get(GuitarAddViewModel::class.java)
    }

    private lateinit var storage: FirebaseStorage
    private lateinit var storageRef: StorageReference
    private var imageUri: Uri = Uri.EMPTY // for saving image
    private var uploadedImageUri: Uri = Uri.EMPTY // for saving image
    private var fileIsSelected = false // true when we choose image

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentGuitarAddBinding.inflate(inflater)
        binding.data = viewModel
        binding.lifecycleOwner = this
        viewModel.setNavigator(this)

        storage = FirebaseStorage.getInstance()
        storageRef = storage.reference

        return binding.root
    }

    override fun openFileChooser() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, viewModel.PICK_IMAGE_REQUEST)
        fileIsSelected = true
    }

    private fun getFileExtension(uri: Uri): String? {
        val cR: ContentResolver = activity!!.contentResolver
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(cR.getType(uri))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == viewModel.PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK
                && data?.data != null) {
            imageUri = data.data!!
            Picasso.get().load(imageUri).into(binding.imgImage)
        }
    }

    // for saving image
    override fun uploadFile() {
        val fileReference: StorageReference = storageRef.child(
                "image/guitar/${System.currentTimeMillis()}.${getFileExtension(imageUri)}")

        val uploadTask = fileReference.putFile(imageUri)

                .addOnCompleteListener() {
                    val handler = Handler()
                    handler.postDelayed({ binding.imgProgress.progress = 0 }, 500)
                    Picasso.get().load(R.drawable.ic_image_is_uploded).into(binding.imgImage)
                }

                .addOnFailureListener { e ->
                    Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
                }

                .addOnProgressListener {
                    val progress = 100.0 * it.bytesTransferred / it.totalByteCount
                    binding.imgProgress.progress = progress.toInt()
                }

        val urlTask = uploadTask.continueWithTask {
                    if (!it.isSuccessful) {
                        it.exception?.let { ex ->
                            throw ex
                        }
                    }
                    fileReference.downloadUrl
                }
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        uploadedImageUri = it.result ?: Uri.EMPTY
                        Log.d("dm->2", "Url: $uploadedImageUri")
                        Toast.makeText(context, "Image uploaded!", Toast.LENGTH_SHORT).show()
                    } else {
                        // Handle failures
                        Toast.makeText(context, "Something wrong!!!", Toast.LENGTH_SHORT).show()
                    }
                }
    }

    override fun showToast(guitar: Guitar) {
        val output: String
        guitar.apply {
            output =
                    "Model: $model;\n" +
                    "Type: $typeOfProduct;\n" +
                    "Manufacturer: $manufacturer;\n" +
                    "Country: $manufacturerCountry;\n" +
                    "Neck material: $guitarNeck;\n" +
                    "Body type: $guitarBody;\n" +
                    "Scale: $guitarScale;\n" +
                    "Number of frets: $scaleNumberOfFrets;\n" +
                    "Color: $guitarColor;\n" +
                    "Is product new: ${if (productIsNew) "Yes" else "No"};\n" +
                    "Description: $description"
        }
        Toast.makeText(context, output, Toast.LENGTH_LONG).show()
    }

    override fun showErrors(isModelCorrect: Boolean, isTypeOfProductCorrect: Boolean,
                            isManufacturerCorrect: Boolean, isManufacturerCountryCorrect: Boolean,
                            isGuitarNeckCorrect: Boolean, isGuitarBodyCorrect: Boolean,
                            isGuitarScaleCorrect: Boolean, isScaleNumberOfFretsCorrect: Boolean,
                            isGuitarColorCorrect: Boolean, isDescriptionCorrect: Boolean, isUriCorrect: Boolean) {
        viewModel.apply {
            if (!isModelCorrect) binding.edtModel.error = "Input model"
            else binding.edtModel.error = null
            if (!isTypeOfProductCorrect) binding.typeOfProduct.error = "Input type"
            else binding.typeOfProduct.error = null
            if (!isManufacturerCorrect) binding.manufacturer.error = "Input manufacturer"
            else binding.manufacturer.error = null
            if (!isManufacturerCountryCorrect) binding.manufacturerCountry.error = "Input country"
            else binding.manufacturerCountry.error = null
            if (!isGuitarNeckCorrect) binding.guitarNeck.error = "Input neck material"
            else binding.guitarNeck.error = null
            if (!isGuitarBodyCorrect) binding.guitarBody.error = "Input body type"
            else binding.guitarBody.error = null
            if (!isGuitarScaleCorrect) binding.edtGuitarScale.error = "Input scale"
            else binding.edtGuitarScale.error = null
            if (!isScaleNumberOfFretsCorrect) binding.scaleNumberOfFrets.error = "Input number of frets"
            else binding.scaleNumberOfFrets.error = null
            if (!isGuitarColorCorrect) binding.edtGuitarColor.error = "Input color"
            else binding.edtGuitarColor.error = null
            if (!isDescriptionCorrect) binding.edtDescription.error = "Input description"
            else binding.edtDescription.error = null
            if (!isUriCorrect) Toast.makeText(context, "Error! Choose and upload image to add guitar", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getPhotoId() = uploadedImageUri.toString()

    override fun onProductIsNewSwitchClick() {
        viewModel.productIsNew = binding.switchIsNew.isChecked
    }

    override fun createSelectTypeOfProductDialog() {
        val listOfEnum = listOf(
                GuitarTypeOfProductEnum.ACOUSTIC.get(),
                GuitarTypeOfProductEnum.BASS.get(),
                GuitarTypeOfProductEnum.ELECTROACOUSTIC.get(),
                GuitarTypeOfProductEnum.ELECTRIC.get()
        )

        var currentIndex = -1

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setSingleChoiceItems(listOfEnum.toTypedArray(),
                        currentIndex) { dialog, which ->
                    currentIndex = which
                    viewModel.updateTypeOfProduct(which + 1)
                    dialog.dismiss()
                    binding.typeOfProduct.error = null
                }
                setTitle("Select type")
                setNegativeButton("Cancel") { _, _ ->
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }

    override fun createSelectManufacturerDialog() {
        val listOfEnum = listOf(
                GuitarManufacturerEnum.YAMAHA.get(),
                GuitarManufacturerEnum.GIBSON.get(),
                GuitarManufacturerEnum.FENDER.get(),
                GuitarManufacturerEnum.IBANEZ.get(),
                GuitarManufacturerEnum.WASHBURN.get(),
                GuitarManufacturerEnum.BEHRINGER.get(),
                GuitarManufacturerEnum.CORT.get(),
                GuitarManufacturerEnum.OTHER.get()
        )

        var currentIndex = -1

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setSingleChoiceItems(listOfEnum.toTypedArray(),
                        currentIndex) { dialog, which ->
                    currentIndex = which
                    viewModel.updateManufacturer(which + 1)
                    dialog.dismiss()
                    binding.manufacturer.error = null
                }
                setTitle("Select manufacturer")
                setNegativeButton("Cancel") { _, _ ->
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }

    override fun createSelectManufacturerCountryDialog() {
        val listOfEnum = listOf(
                GuitarManufacturerCountryEnum.JAPAN.get(),
                GuitarManufacturerCountryEnum.CHINA.get(),
                GuitarManufacturerCountryEnum.INDONESIA.get(),
                GuitarManufacturerCountryEnum.KOREA.get(),
                GuitarManufacturerCountryEnum.AUSTRALIA.get(),
                GuitarManufacturerCountryEnum.MEXICO.get(),
                GuitarManufacturerCountryEnum.USA.get(),
                GuitarManufacturerCountryEnum.UK.get(),
                GuitarManufacturerCountryEnum.FRANCE.get(),
                GuitarManufacturerCountryEnum.GERMANY.get(),
                GuitarManufacturerCountryEnum.OTHER.get()
        )

        var currentIndex = -1

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setSingleChoiceItems(listOfEnum.toTypedArray(),
                        currentIndex) { dialog, which ->
                    currentIndex = which
                    viewModel.updateManufacturerCountry(which + 1)
                    dialog.dismiss()
                    binding.manufacturerCountry.error = null
                }
                setTitle("Select country")
                setNegativeButton("Cancel") { _, _ ->
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }

    override fun createSelectGuitarNeckDialog() {
        val listOfEnum = listOf(
                GuitarNeckEnum.ALDER.get(),
                GuitarNeckEnum.ASH.get(),
                GuitarNeckEnum.BASSWOOD.get(),
                GuitarNeckEnum.MAHOGANY.get(),
                GuitarNeckEnum.MAPLE.get(),
                GuitarNeckEnum.OTHER.get()
        )

        var currentIndex = -1

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setSingleChoiceItems(listOfEnum.toTypedArray(),
                        currentIndex) { dialog, which ->
                    currentIndex = which
                    viewModel.updateGuitarNeck(which + 1)
                    dialog.dismiss()
                    binding.guitarNeck.error = null
                }
                setTitle("Select neck material")
                setNegativeButton("Cancel") { _, _ ->
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }

    override fun createSelectGuitarBodyDialog() {
        val listOfEnum = listOf(
                GuitarBodyEnum.DREADNOUGHT.get(),
                GuitarBodyEnum.PARLOUR.get(),
                GuitarBodyEnum.JUMBO.get(),
                GuitarBodyEnum.AUDITORIUM.get(),
                GuitarBodyEnum.CLASSICAL.get(),
                GuitarBodyEnum.TRAVEL.get(),
                GuitarBodyEnum.OTHER.get()
        )

        var currentIndex = -1

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setSingleChoiceItems(listOfEnum.toTypedArray(),
                        currentIndex) { dialog, which ->
                    currentIndex = which
                    viewModel.updateGuitarBody(which + 1)
                    dialog.dismiss()
                    binding.guitarBody.error = null
                }
                setTitle("Select body type")
                setNegativeButton("Cancel") { _, _ ->
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }

    override fun createSelectNumberOfFretsDialog() {
        val listOfEnum = listOf(
                GuitarNumberOfFretsEnum.NINETEEN.get(),
                GuitarNumberOfFretsEnum.TWENTY.get(),
                GuitarNumberOfFretsEnum.TWENTYONE.get(),
                GuitarNumberOfFretsEnum.TWENTYFOUR.get(),
                GuitarNumberOfFretsEnum.TWENTYSEVEN.get(),
                GuitarNumberOfFretsEnum.OTHER.get()
        )

        var currentIndex = -1

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setSingleChoiceItems(listOfEnum.toTypedArray(),
                        currentIndex) { dialog, which ->
                    currentIndex = which
                    viewModel.updateNumberOfFrets(which + 1)
                    dialog.dismiss()
                    binding.scaleNumberOfFrets.error = null
                }
                setTitle("Select number of frets")
                setNegativeButton("Cancel") { _, _ ->
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }
}
