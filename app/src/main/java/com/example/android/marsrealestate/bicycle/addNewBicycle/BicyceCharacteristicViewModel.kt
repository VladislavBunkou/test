package com.example.android.marsrealestate.bicycle.addNewBicycle

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentBicycleCharacteristicBinding

class BicycleCharacteristicFragment: Fragment() {

    private val TAG : String = "BicycleCharacteristicFragment"

    lateinit var binding: FragmentBicycleCharacteristicBinding

    private val viewModel: BicycleCharacteristicViewModel by lazy {
        ViewModelProviders.of(this).get(BicycleCharacteristicViewModel::class.java)
    }

    // for saving data
    private lateinit var bicycleInfoViewModel: BicycleInfoViewModel

    @SuppressLint("LongLogTag")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val application = requireNotNull(activity).application

        binding = FragmentBicycleCharacteristicBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        activity?.let {
            bicycleInfoViewModel = ViewModelProviders.of(it).get(BicycleInfoViewModel::class.java)

            viewModel.foldingSelected.value = bicycleInfoViewModel.folding.get()
            viewModel.mountainSelected.value = bicycleInfoViewModel.mountain.get()
            viewModel.roadSelected.value = bicycleInfoViewModel.road.get()
            viewModel.citybikeSelected.value = bicycleInfoViewModel.citybike.get()
            viewModel.hibridSelected.value = bicycleInfoViewModel.hibrid.get()

            viewModel.productIsNewSelected.value = bicycleInfoViewModel.productIsNew.get()

            viewModel.foldingSelected.observe(viewLifecycleOwner, Observer {
                bicycleInfoViewModel.folding.set(it)
                Log.d(TAG, "folding $it ")
            })
            viewModel.mountainSelected.observe(viewLifecycleOwner, Observer {
                bicycleInfoViewModel.mountain.set(it)
                Log.d(TAG, "mountain $it ")
            })
            viewModel.roadSelected.observe(viewLifecycleOwner, Observer {
                bicycleInfoViewModel.road.set(it)
                Log.d(TAG, "road $it ")
            })
            viewModel.citybikeSelected.observe(viewLifecycleOwner, Observer {
                bicycleInfoViewModel.citybike.set(it)
                Log.d(TAG, "citybike $it ")
            })
            viewModel.hibridSelected.observe(viewLifecycleOwner, Observer {
                bicycleInfoViewModel.hibrid.set(it)
                Log.d(TAG, "hibrid $it ")
            })
            viewModel.productIsNewSelected.observe(viewLifecycleOwner, Observer {
                bicycleInfoViewModel.productIsNew.set(it)
                Log.d(TAG, "productIsNew $it ")
            })
        }

        return binding.root
    }

}