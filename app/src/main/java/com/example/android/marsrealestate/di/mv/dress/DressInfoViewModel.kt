package com.example.android.marsrealestate.di.mv.dress

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel

class DressInfoViewModel( app: Application) : AndroidViewModel(app)  {

    val oneColor= ObservableField<Boolean>(false)
    val multiColor = ObservableField<Boolean>(false)
    val decoration = ObservableField<Boolean>(false)
    val print = ObservableField<Boolean>(false)

    val productIsNew = ObservableField<Boolean>(false)
}