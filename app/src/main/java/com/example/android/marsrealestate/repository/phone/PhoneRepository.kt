package com.example.android.marsrealestate.repository.phone

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.enums.Status
import com.example.android.marsrealestate.model.Phone
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class PhoneRepository(val coroutineScope: CoroutineScope) {

    val phones = MutableLiveData<List<Phone>>()
    val savePhoneObservable = MutableLiveData<PhoneSaveInfo>()
    val receivePhoneObservable = MutableLiveData<PhoneGetInfo>() // replace to Repository
    val modelPhoneObservable = MutableLiveData<String>()
    val brandPhoneObservable = MutableLiveData<String>()
    val countryPhoneObservable = MutableLiveData<String>()



    fun getPhonesList() {
        receivePhoneObservable.postValue(PhoneGetInfo(Status.LOADING))
        Log.d("PhoneRepository", "dm->  1")
        coroutineScope.launch {
            Log.d("PhoneRepository", "dm-> 2")
            var getPropertiesDeferred = RetrofitApi.retrofitApiService.getPhoneList()

            try {
//                _status.value = PhoneApiStatus.LOADING
                var listResult = getPropertiesDeferred.await()
//                _status.value = PhoneApiStatus.DONE
                phones.value = listResult
                Log.d("PhoneRepository", "dm-> 11 / phones.value = ${phones.value?.size}" )
                receivePhoneObservable.postValue(PhoneGetInfo(Status.SUCCESS))
//                receivePhoneObservable.postValue(PhoneGetInfo(Status.ERROR) //todo - add logic for it
            } catch (e: Exception) {
//                _status.value = PhoneApiStatus.ERROR
                Log.d("PhoneRepository", "dm-> Internet connection not exist or Server is shot down")
                phones.value = ArrayList()
                receivePhoneObservable.postValue(PhoneGetInfo(Status.ERROR_CONNECTION))
            }

        }
    }


    fun receivePhoneById(id: Long, isSaveNewPhoneOnServer: Boolean) {
        receivePhoneObservable.postValue(PhoneGetInfo(Status.LOADING)) // change
        if (isSaveNewPhoneOnServer) {
            receivePhoneObservable.postValue(PhoneGetInfo(Status.SUCCESS))
            Log.d("PhoneRepository", "dm->  getMarsRealEstateProperties / id = ${id} - means new Phone")
        } else
            coroutineScope.launch {
                Log.d("PhoneRepository", "dm->  getMarsRealEstateProperties / id = ${id}")
                //            var getPropertiesDeferred = MarsApi.retrofitService.getProperties(filter.value)
                var getPropertiesDeferred = RetrofitApi.retrofitApiService.getPhoneOne(id)
                try {
//                _status.value = MarsApiStatus.LOADING
                    // this will run on a thread managed by Retrofit
                    val tabletOne = getPropertiesDeferred.await()

                    Log.d("DetailViewModel", "dm-> try tablet = ${tabletOne.toString()}")
                    modelPhoneObservable.postValue(tabletOne.model)
                    brandPhoneObservable.postValue(tabletOne.manufacturer)
                    receivePhoneObservable.postValue(PhoneGetInfo(Status.SUCCESS))
//                _status.value = MarsApiStatus.DONE
//                listResult.id = id
//                _selectedProperty.value = tabletOne
                } catch (e: Exception) {
//                _status.value = MarsApiStatus.ERROR
//                _selectedProperty.value = Prod()
                    Log.d("DetailViewModel", "dm-> catch ")
                    receivePhoneObservable.postValue(PhoneGetInfo(Status.ERROR_CONNECTION))
                }
            }
    }

}