package com.example.android.marsrealestate.shoes

import android.app.Application
import androidx.lifecycle.*

class ShoesDetailViewModel (grindingCylindrical: Shoes,app: Application): AndroidViewModel(app){
    private val _selectedProperty = MutableLiveData<Shoes>()
    val selectedProperty: LiveData<Shoes>
        get() = _selectedProperty

    init {
        _selectedProperty.value = grindingCylindrical
    }
}

class ShoesDetailViewModelFactory(private val sh: Shoes, private val application: Application) :
        ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ShoesDetailViewModel::class.java)) {
            return ShoesDetailViewModel(sh, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}