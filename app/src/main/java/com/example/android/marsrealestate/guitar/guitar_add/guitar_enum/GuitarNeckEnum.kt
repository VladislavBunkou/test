package com.example.android.marsrealestate.guitar.guitar_add.guitar_enum

enum class GuitarNeckEnum (val id: String) {
    OTHER("0"){ override fun get(): String = "Other" },
    ALDER("1"){ override fun get(): String = "Alder" },
    ASH("2"){ override fun get(): String = "Ash" },
    BASSWOOD("3"){ override fun get(): String = "Basswood" },
    MAHOGANY("4"){ override fun get(): String = "Mahogany" },
    MAPLE("5"){ override fun get(): String = "Maple" };

    abstract fun get(): String
}