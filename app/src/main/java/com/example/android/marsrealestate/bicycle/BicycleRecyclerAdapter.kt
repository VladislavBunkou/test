package com.example.android.marsrealestate.bicycle

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.ItemBicycleBinding

class BicycleRecyclerAdapter() : RecyclerView.Adapter<BicycleRecyclerAdapter.BicyclePropertyviewHolder>(){


    private val bicycleList: ArrayList<Bicycle> = arrayListOf()

    private lateinit var listener: BicycleItemListener

    override fun getItemCount(): Int = bicycleList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BicyclePropertyviewHolder {
        return BicyclePropertyviewHolder(ItemBicycleBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: BicyclePropertyviewHolder, position: Int) {
        val itemBicycleViewModel = ItemBicycleViewModel()
        itemBicycleViewModel.bind(bicycleList[position], listener)
        holder.bind(itemBicycleViewModel)
    }



    fun addItems(list: List<Bicycle>) {
        this.bicycleList.clear()
        this.bicycleList.addAll(list)
        notifyDataSetChanged()
    }

    fun addListener(listener: BicycleItemListener){
        Log.d("BicycleRecyclerAdapter", "Listener is add  = $listener")
        this.listener = listener
    }

    class BicyclePropertyviewHolder(private var binding: ItemBicycleBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(itemBicycleViewModel: ItemBicycleViewModel) {
            binding.mv = itemBicycleViewModel
            binding.executePendingBindings()
        }

    }



}

