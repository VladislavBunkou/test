package com.example.android.marsrealestate.ui.map.boat

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentGoogleMap2Binding
import com.example.android.marsrealestate.boat.model.Boat
import com.example.android.marsrealestate.di.mv.map.GoogleMapFragment2VM
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.sothree.slidinguppanel.SlidingUpPanelLayout

class BoatMapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMapClickListener, GoogleApiClient.ConnectionCallbacks {
    private val TAG = "GoogleMapFragment2"

    companion object {
        const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2025
        const val DEF_ZOOM = 5f
    }

    lateinit var binding: FragmentGoogleMap2Binding
    private val viewModel: BoatMapViewModel by lazy { ViewModelProviders.of(this).get(BoatMapViewModel::class.java) }
    lateinit var googleMap: GoogleMap
    private var selectedBoat: Boat? = null

    private var boats = listOf<Boat>()
    private var selectedMarker: Marker? = null
    private var lastKnownLocation: Location? = null

    private val observerListBoat = Observer<List<Boat>> { storesList ->
        if (storesList.isNotEmpty()) {
            boats = storesList
            if (selectedBoat == null) selectedBoat = storesList[0]
            addMapMarkers(storesList)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(TAG, "dm:: onCreateView")
        binding = FragmentGoogleMap2Binding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "dm:: onViewCreated")
        binding.slidingLayout.panelState = SlidingUpPanelLayout.PanelState.HIDDEN
        binding.slidingLayout.setFadeOnClickListener {
            binding.slidingLayout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        viewModel.boats.observe(viewLifecycleOwner, observerListBoat)
        (childFragmentManager.findFragmentById(R.id.map_dm) as SupportMapFragment).getMapAsync(this)
        buildGoogleApiClient()
        getLocationPermission()
    }

    override fun onResume() {
        super.onResume()
        if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getCurrentLocation()
        }
    }

    private fun selectProd(boat: Boat) {
        binding.prodTitle.text = boat.model
        binding.mapDirectionTitle.text = boat.typeOfProduct
        googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(boat.latitude, boat.longitude), DEF_ZOOM))
        binding.mapDirectionLayout.setOnClickListener {
            val url = "http://maps.google.com/maps?saddr=${lastKnownLocation?.latitude ?: 0},${lastKnownLocation?.longitude ?: 0}&daddr=${boat.latitude},${boat.longitude}"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            intent.setPackage("com.google.android.apps.maps")

        }
//       Route - only for paid accaunt
    }

    private fun buildGoogleApiClient() {
        val mGoogleApiClient = GoogleApiClient.Builder(context!!)
                .addConnectionCallbacks(this) // work with GoogleApiClient.ConnectionCallbacks
                .addApi(LocationServices.API)
                .build()
        mGoogleApiClient.connect()
    }


    override fun onMapReady(googleMap2: GoogleMap) {
        Log.d(TAG, "dm:: onMapReady")
        googleMap = googleMap2
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnMapClickListener(this);
        val belarusLoc = LatLng(52.0, 24.0)
        val cameraPosition = CameraPosition.Builder().target(belarusLoc).zoom(DEF_ZOOM).build()
        googleMap2.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    private fun addMapMarkers(it: List<Boat>) {
        it.forEach {
            googleMap.addMarker(MarkerOptions().position(LatLng(it.latitude, it.longitude)).title(it.model))
        }
    }


    private fun getLocationPermission() {
        Log.d(TAG, "dm:: getLocationPermission")
        if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getCurrentLocation()
        } else {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }
    }

    private fun getCurrentLocation() {
        val mLocationRequest = LocationRequest.create().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        fusedLocationClient?.requestLocationUpdates(mLocationRequest, object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                super.onLocationResult(p0)
                if (p0 == null) return
                setLastLocation(p0.lastLocation)
                fusedLocationClient.removeLocationUpdates(this)
            }
        }, null)
    }

    private fun setLastLocation(lastLocation: Location?) {
        lastLocation?.let {
            lastKnownLocation = it
            if (selectedBoat != null && isAdded) {
                selectProd(selectedBoat!!)
            }
        }
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        Log.d(TAG, "dm:: onMarkerClick p0 = $p0")
        binding.slidingLayout.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        selectedMarker = p0
        boats.forEach {
            if (it.latitude == p0?.position?.latitude && it.longitude == p0?.position?.longitude) {
                selectedBoat = it
                selectProd(it)
                return@forEach
            }
        }
        return true
    }

    override fun onMapClick(p0: LatLng?) {
        Log.d(TAG, "dm:: onMapClick")
    }

    override fun onConnected(p0: Bundle?) {
        Log.d(TAG, "dm:: onConnected")
    }

    override fun onConnectionSuspended(p0: Int) {
        Log.d(TAG, "dm:: onConnectionSuspended")
    }
}

