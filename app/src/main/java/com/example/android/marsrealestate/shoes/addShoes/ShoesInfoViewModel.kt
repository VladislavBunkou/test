package com.example.android.marsrealestate.shoes.addShoes

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel

class ShoesInfoViewModel ( app: Application) : AndroidViewModel(app)  {

    val winter = ObservableField<Boolean>(false)
    val spring  = ObservableField<Boolean>(false)
    val fall  = ObservableField<Boolean>(false)
    val summer  = ObservableField<Boolean>(false)
    val demi  = ObservableField<Boolean>(false)

    val productIsNew  = ObservableField<Boolean>(false)
}