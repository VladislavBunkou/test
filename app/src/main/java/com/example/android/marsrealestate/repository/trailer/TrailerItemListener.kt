package com.example.android.marsrealestate.repository.trailer

import com.example.android.marsrealestate.trailer.model.Trailer


interface TrailerItemListener {
    fun onSubUserItemClick(s: Trailer)
}