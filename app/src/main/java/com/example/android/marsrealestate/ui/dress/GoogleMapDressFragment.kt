package com.example.android.marsrealestate.ui.dress

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentGoogleMapDressBinding
import com.example.android.marsrealestate.di.mv.dress.GoogleMapDressViewModel
import com.example.android.marsrealestate.dress.Dress
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.sothree.slidinguppanel.SlidingUpPanelLayout

class GoogleMapDressFragment: Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMapClickListener, GoogleApiClient.ConnectionCallbacks {



    private val TAG = "Dress Map"

    companion object {
        const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2025
        const val DEF_ZOOM = 5f
    }

    lateinit var binding: FragmentGoogleMapDressBinding
    private val viewModel: GoogleMapDressViewModel by lazy { ViewModelProviders.of(this).get(GoogleMapDressViewModel::class.java) }
    lateinit var googleMap: GoogleMap
    private var dress: Dress? = null

    private var dresses = listOf<Dress>()
    private var selectedMarker: Marker? = null
    private var lastKnownLocation: Location? = null

    val observerListDress = Observer<List<Dress>> {
        storesList -> if (storesList.isNotEmpty()){
        dresses = storesList
        if (dress == null) {
            dress = storesList[0]
            addMapMarkers(storesList)
        }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(TAG, ": onCreateView")
        binding = FragmentGoogleMapDressBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, ": onViewCreated")
        binding.slidingLayout.panelState = SlidingUpPanelLayout.PanelState.HIDDEN
        binding.slidingLayout.setFadeOnClickListener {
            binding.slidingLayout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        viewModel.dresses.observe(viewLifecycleOwner, observerListDress)
        (childFragmentManager.findFragmentById(R.id.dress_map) as SupportMapFragment).getMapAsync(this)
        buildGoogleApiClient()
        getLocationPermission()
    }

    override fun onResume() {
        super.onResume()
        if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getCurrentLocation()
        }
    }

    private fun selectDress(dress:Dress){
        binding.dressTitle.text = dress.model
        binding.mapDirectionTitle.text = dress.typeOfProduct
        googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(dress.latitude, dress.longitude), DEF_ZOOM))
        binding.mapDirectionLayout.setOnClickListener {
            val url = "http://maps.google.com/maps?saddr=${lastKnownLocation?.latitude ?: 0},${lastKnownLocation?.longitude ?: 0}&daddr=${dress.latitude},${dress.longitude}"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            intent.setPackage("com.google.android.apps.maps")

        }
    }

    private fun buildGoogleApiClient() {
        val mGoogleApiClient = GoogleApiClient.Builder(context!!)
                .addConnectionCallbacks(this) // work with GoogleApiClient.ConnectionCallbacks
                .addApi(LocationServices.API)
                .build()
        mGoogleApiClient.connect()
    }

    override fun onMapReady(p0: GoogleMap) {
        Log.d(TAG, ": onMapReady")
        googleMap = p0
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnMapClickListener(this);
        val belarusLoc = LatLng(52.0, 24.0)
        val cameraPosition = CameraPosition.Builder().target(belarusLoc).zoom(DEF_ZOOM).build()
        p0.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

    }

    private fun addMapMarkers(it: List<Dress>) {
        it.forEach {
            googleMap.addMarker(MarkerOptions().position(LatLng(it.latitude, it.longitude)).title(it.model))
        }
    }

    private fun getLocationPermission() {
        Log.d(TAG, ": getLocationPermission")
        if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getCurrentLocation()
        } else {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }
    }

    private fun getCurrentLocation() {
        val mLocationRequest = LocationRequest.create().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        fusedLocationClient?.requestLocationUpdates(mLocationRequest, object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                super.onLocationResult(p0)
                if (p0 == null) return
                setLastLocation(p0.lastLocation)
                fusedLocationClient.removeLocationUpdates(this)
            }
        }, null)
    }

    private fun setLastLocation(lastLocation: Location?) {
        lastLocation?.let {
            lastKnownLocation = it
            if (dress != null && isAdded) {
                selectDress(dress!!)
            }
        }
    }


    override fun onMarkerClick(p0: Marker?): Boolean {
        Log.d(TAG, ": onMarkerClick p0 = $p0")
        binding.slidingLayout.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        selectedMarker = p0
        dresses.forEach {
            if (it.latitude == p0?.position?.latitude && it.longitude == p0?.position?.longitude) {
                dress = it
                selectDress(it)
                return@forEach
            }
        }
        return true
    }

    override fun onMapClick(p0: LatLng?) {
        Log.d(TAG, ": onMapClick")
    }

    override fun onConnected(p0: Bundle?) {
        Log.d(TAG, ": onConnected")
    }

    override fun onConnectionSuspended(p0: Int) {
        Log.d(TAG, ": onConnectionSuspended")
    }

}