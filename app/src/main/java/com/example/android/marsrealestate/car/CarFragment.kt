package com.example.android.marsrealestate.car

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration

import com.akordirect.vmccnc.databinding.FragmentCar1Binding

class CarFragment : Fragment() {

    private val viewModel: AvtoViewModel by lazy {
        ViewModelProviders.of(this).get(AvtoViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentCar1Binding.inflate(inflater)
         binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.photosGrid.adapter = CarRecyclerAdapter(CarRecyclerAdapter.OnClickListener {
            viewModel.displayPropertyDetails(it)
        })
        binding.photosGrid.addItemDecoration(DividerItemDecoration(context,DividerItemDecoration.VERTICAL) )

        viewModel.navigateToSelectedProperty.observe(this, Observer {
            if ( null != it ) {
                // Must find the NavController from the Fragment
                this.findNavController().navigate(CarFragmentDirections.actionAvtoFragmentToDetailCarFragment(it))
                // Tell the ViewModel we've made the navigate call to prevent multiple navigation
                viewModel.displayPropertyDetailsComplete()
            }
        })

        return binding.root
    }
}
