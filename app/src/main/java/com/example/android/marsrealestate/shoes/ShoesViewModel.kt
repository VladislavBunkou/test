package com.example.android.marsrealestate.shoes

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.android.marsrealestate.di.mv.BaseViewModel
import com.example.android.marsrealestate.enums.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

class ShoesViewModel : BaseViewModel<Any>(){

    val shoesListObservable = ObservableArrayList<Shoes>()

    val status: LiveData<ShoesApiStatus>
        get() = _status
    private val _status = MutableLiveData<ShoesApiStatus>()

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main )

    private val allShoesObserver = Observer<List<Shoes>> { list ->
        list?.let {
            shoesListObservable.run {
                Log.d("ShoesViewModel", "22 this.size = ${this.size}")
                clear()
                addAll(list)
            }
        }
    }

    private val receiveShoesObserver = Observer<ShoesGetInfo> {
        when (it.status) {
            Status.INITIAL, Status.LOADING -> {
                setIsLoading(true)
                Log.d("ShoesViewModel", "Status.Loading")
            }
            Status.SUCCESS -> {
                setIsLoading(false)
                Log.d("ShoesViewModel", "Status.SUCCESS")
            }
            Status.ERROR -> {
                setIsLoading(false)
                Log.d("ShoesViewModel", "Status.Error")
            }
            Status.ERROR_CONNECTION -> {
                setIsLoading(false)
            }
            else -> {
            }
        }
    }

    init {
        val shoesRepository = ShoesRepository(coroutineScope)
        shoesRepository.shoes.observeForever(allShoesObserver)
        shoesRepository.receiveShoesObservable.observeForever(receiveShoesObserver)
        shoesRepository.getShoesList()
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun testRefresh(){
        Log.d("ShoesViewModel", "REFRESH")
    }
}