package com.example.android.marsrealestate.ui.dress

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentDressListBinding
import com.example.android.marsrealestate.di.mv.dress.DressViewModel
import com.example.android.marsrealestate.dress.Dress
import com.example.android.marsrealestate.dress.DressItemListener
import com.example.android.marsrealestate.ui.BaseFragment
import com.example.android.marsrealestate.ui.adapters.dress.DressRecyclerAdapter
import com.example.android.marsrealestate.ui.authentification.LoginFragment.Companion.TAG
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomsheet.BottomSheetDialog
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
class DressFragment : BaseFragment(), DressItemListener{


    private val viewModel: DressViewModel by lazy {
        ViewModelProviders.of(this).get(DressViewModel::class.java)
    }

    //LOCATION
    private lateinit var fusedLocationClient: FusedLocationProviderClient


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = FragmentDressListBinding.inflate(inflater)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.listenerDress = this

                //getLocation()
        fusedLocationClient  = LocationServices.getFusedLocationProviderClient(requireActivity())


        binding.lasersGrid.adapter = DressRecyclerAdapter()

        setHasOptionsMenu(true)

        binding.googleMapDress.setOnClickListener{
            this.findNavController().navigate(DressFragmentDirections.actionDressFragmentToGoogleMapDressFragment2())

        }

        return binding.root
    }

    override fun onSubUserItemClick(dress: Dress) {
        this.findNavController().navigate(DressFragmentDirections.actionDressFragmentToDressDetailFragment(dress))
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)

        val menuItem = menu.findItem(R.id.search_prod)
        val searchView = menuItem.actionView as SearchView
        searchView.queryHint = "Search..."
//        searchView.setBackgroundColor(Color.parseColor("#EFEFF4"))
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                Log.d("DressFragment", "dm:: onQueryTextSubmit")
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                Log.d("DressFragment", "dm:: onQueryTextChange = $newText")
                viewModel.getDressList(newText)
                return true
            }

        })
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val bottomView: View = layoutInflater.inflate(R.layout.sheet_main_filter,null)

        when (item.itemId) {
            R.id.set_location ->{
                getLocationWithPermissionCheck()
                Log.d("DressFragment","location sort")
            }
            R.id.filter_bottom -> {
                Log.d("DressFragment","filter")
                activity?.let {
                    val dialog = BottomSheetDialog(it)
                    dialog.setContentView(bottomView)
                    dialog.show()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    fun getLocation(){
        this.fusedLocationClient.lastLocation
                .addOnSuccessListener { location->
                    if (location != null) {
                        d_latitude =  location.latitude
                        d_longitude = location.longitude
                        Log.d(TAG,"AddDressFragment: km->Location  latitude=$d_latitude, longitude=$d_longitude")
                    }
                    else if (location == null){
                        Log.d(TAG,"AddDressFragment: km->Location IS NULL latitude=$d_latitude, longitude=$d_longitude")
                    }

                }
        viewModel.getSortingDressList("")
    }



    fun onClickMapFragment() {
        Log.d("DressFragment", "onClickMapFragment")
        val intent = Intent(activity,GoogleMapDressFragment::class.java)
        startActivity(intent)

        //this.findNavController().navigate(DressFragmentDirections.actionDressFragmentToGoogleMapDressFragment())
    }

}
