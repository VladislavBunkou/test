package com.example.android.marsrealestate.tablet.addtablet

enum class TabletManufacturerType(val id: String) {
    APPLE("1"){ override fun getBrand(): String = "Apple" },
    SAMSUNG("2"){ override fun getBrand(): String = "Samsung" },
    LENOVO("3"){ override fun getBrand(): String = "Lenovo" },
    OTHER("4"){ override fun getBrand(): String = "Other" };

    abstract fun getBrand(): String
}