package com.example.android.marsrealestate.ui.phone

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.ActivityCreatePhoneBinding
import com.akordirect.vmccnc.databinding.ActivityMainBinding
import com.example.android.marsrealestate.MyApplication
import com.example.android.marsrealestate.di.PhoneCreateComponent
import javax.inject.Inject

class PhoneCreateActivity : AppCompatActivity() {

    @Inject
    lateinit var registrationComponent: PhoneCreateComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        registrationComponent = (application as MyApplication).appComponent.loginComponent().create() // 2
        registrationComponent.inject(this) // 2
        super.onCreate(savedInstanceState)

        @Suppress("UNUSED_VARIABLE")
        val binding = DataBindingUtil.setContentView<ActivityCreatePhoneBinding>(this, R.layout.activity_create_phone)
//        drawerLayout = binding.drawerLayout
        val navController = this.findNavController(R.id.myNavHostFragment)

//        NavigationUI.setupActionBarWithNavController(this, navController)
//        NavigationUI.setupWithNavController(binding.navView, navController) // to display the navigation drawer.
//        NavigationUI.setupWithNavController(binding.bottomMenuNavigation, navController)

        val appBarConfiguration = AppBarConfiguration(navController.graph) // For back buttons
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration)
    }

    override fun onSupportNavigateUp(): Boolean { //This method is called when the up button is pressed. Just the pop back stack.
        supportFragmentManager.popBackStack()
        return true
    }
}