package com.example.android.marsrealestate.ui

import android.content.Context
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment

//import javax.inject.Inject


abstract class BaseFragment : Fragment() {





    protected open fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

//    protected open fun showToast(@StringRes messageId: Int) {
//        mDialogCallbacks.showToast(messageId)
//    }
}

//abstract class BaseFragment<VM : ViewModel, VB : ViewDataBinding> : Fragment() {
//
//    @Inject
//    protected lateinit var viewModelFactory: ViewModelProvider.Factory
//
//    lateinit var mViewModel: VM
//    lateinit var mViewDataBinding: VB
//
//    abstract fun getLayout(): Int
//
//
////    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
////       val view = inflater.inflate(getLayout(), container, false)
////
////        try {
////            mViewModel = ViewModelProviders.of(this, viewModelFactory).get(getVMTypeClass())
////        }
////
//////        return super.onCreateView(inflater, container, savedInstanceState)
////    }
////
////
////    @SuppressWarnings("unchecked")
////    private fun getVMTypeClass(): Class<VM?>? {
////        return try {
////            val classNameMy = (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0].toString()
////            val clazz = Class.forName(classNameMy.replace("class ", ""))
////            clazz as Class<VM?>
////        } catch (e: Exception) {
////            throw IllegalStateException(e.message)
////        }
////    }
//}

interface DialogCallbacks {

//    fun showProgressDialog(message: String?)
    fun showToast(message: String?)
}