package com.example.android.marsrealestate.di.mv.nav

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NavClothesViewModel : ViewModel() {


    val navigateToDress = MutableLiveData<Boolean>()
    val navigateToShoes = MutableLiveData<Boolean>()


    fun onDressClick() {
        navigateToDress.value = true
    }
    fun navigationDressFinished() {
        navigateToDress.value = false
    }

    fun onShoesClick(){
        navigateToShoes.value = true
    }
    fun navigationShoesFinished() {
        navigateToShoes.value = false
    }
}