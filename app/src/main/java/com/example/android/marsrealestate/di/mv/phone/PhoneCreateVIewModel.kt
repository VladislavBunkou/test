package com.example.android.marsrealestate.di.mv.phone

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.databinding.ObservableList
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.di.mv.BaseViewModel
import com.example.android.marsrealestate.enums.Status
import com.example.android.marsrealestate.repository.body.PhoneBody
import com.example.android.marsrealestate.repository.phone.PhoneRepository
import com.example.android.marsrealestate.ui.adapters.phone.PhoneNavigator
import com.example.android.marsrealestate.repository.phone.PhoneSaveInfo
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.*
import javax.inject.Inject


class PhoneCreateViewModel @Inject constructor() : BaseViewModel<PhoneNavigator>() {

    private val TAG = "PhoneNewViewModel"

    lateinit var phoneRepository: PhoneRepository

    val phoneFilesList: ObservableList<PhoneFile> = ObservableArrayList()

    var modelPhone = ObservableField<String>("") // for field
    var countryPhone = ObservableField<String>("") // for field
    var brandPhone = ObservableField<String>("") // for field

    val savePhoneObservable = MutableLiveData<PhoneSaveInfo>() // replace to Repository

    var navigateToPhoneCharacteristics = MutableLiveData<Boolean>(false)
    private var manufacturerType: PhoneManufacturerType? = null

    var PICK_IMAGE_REQUEST: Int = 1

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main) // Add repository


    private val savePhoneObserver = Observer<PhoneSaveInfo> {
        when (it.status) {
            Status.INITIAL, Status.LOADING -> {
                setIsLoading(true)
                Log.d("PhoneNewViewModel", "dm-> Status.Loading")
            }
            Status.SUCCESS -> {
                setIsLoading(false)
                Log.d("PhoneNewViewModel", "dm-> Status.SUCCESS")
                getNavigator()?.backOnMyCabinet()
            }
            Status.ERROR -> {
                setIsLoading(false)
                Log.d("PhoneNewViewModel", "dm-> Status.Error")
            }
            Status.ERROR_CONNECTION -> {
                setIsLoading(false)
            }
            else -> {
            }
        }
    }

    init {
        setItems()
        savePhoneObservable.observeForever(savePhoneObserver)
        phoneRepository = PhoneRepository(coroutineScope)

        phoneRepository.modelPhoneObservable.observeForever(Observer { modelPhone.set(it) })
        phoneRepository.brandPhoneObservable.observeForever(Observer { brandPhone.set(it) })
        phoneRepository.countryPhoneObservable.observeForever(Observer { countryPhone.set(it) })

    }


    fun receivePhoneById(id: Long, isSaveNewPhoneOnServer: Boolean) {
        phoneRepository.receivePhoneById(id, isSaveNewPhoneOnServer)
    }


    fun onSendButton() {


        if (allFilldsIGood()) {
            val idUserFirebase = if (FirebaseAuth.getInstance().currentUser != null) FirebaseAuth.getInstance().currentUser?.uid else "-"
            Log.d(TAG, "dm-> idUserFirebase = $idUserFirebase")
            val phone = PhoneBody().apply {
                userId = idUserFirebase ?: ""
                model = modelPhone.get() ?: ""
                manufacturer = brandPhone.get() ?: ""
                country = countryPhone.get() ?: ""
                has3g = getNavigator()?.get3G()?.get() ?: false
                has4g = getNavigator()?.get4G()?.get() ?: false
                url = getNavigator()?.getPhotoId() ?: ""
            }
            savePhoneWithDeferred(phone)
            getNavigator()?.showToast("Phone ${phone.model} is saved!")
            modelPhone.set("")
            countryPhone.set("")
            brandPhone.set("")
        } else {
            getNavigator()?.showToast("Phone not Save !!")
        }
    }


    private fun savePhoneWithDeferred(phone: PhoneBody) {
        savePhoneObservable.postValue(PhoneSaveInfo(Status.LOADING))

        coroutineScope.launch {
            var getPropertiesDeferred = RetrofitApi.retrofitApiService.savePhone(phone)
            try {
//                    _status.value = LatheApiStatus.LOADING
                Log.d(TAG, "dm-> all good")
                var phoneResponse = getPropertiesDeferred.await()
                Log.d(TAG, "dm-> code ${phoneResponse.code()}")
                if (phoneResponse.isSuccessful) {
                    Log.d(TAG, "dm-> code ${phoneResponse.code()}")
//                    var resposne = phoneResponse.body().toString()
//                    Log.d(TAG, "dm-> body ${phoneResponse.body().toString() }")
                    savePhoneObservable.postValue(PhoneSaveInfo(Status.SUCCESS))
                } else {
                    savePhoneObservable.postValue(PhoneSaveInfo(Status.ERROR))
                }
//                    _status.value = LatheApiStatus.DONE
//                _properties.value = listResult
            } catch (e: Exception) {
//                _status.value = LatheApiStatus.ERROR
                Log.d(TAG, "dm-> Error :: ${e.message}")
                savePhoneObservable.postValue(PhoneSaveInfo(Status.ERROR_CONNECTION))
            }
        }
    }


    fun allFilldsIGood(): Boolean {
        var isModelFilled = false
        var isCountryFilled = false
        var isBrandFilled = false

        if (!modelPhone.get().isNullOrEmpty()) isModelFilled = true
        if (!countryPhone.get().isNullOrEmpty()) isCountryFilled = true
        if (!brandPhone.get().isNullOrEmpty()) isBrandFilled = true

        Log.d(TAG, "dm-> brand: ${brandPhone.get()}")

        if (!isModelFilled || !isCountryFilled || !isBrandFilled) {
            getNavigator()?.errorMessages(isModelFilled, isCountryFilled)
            return false
        } else {
            return true
        }
    }

    fun updateManufacturer(selectedIndex: Int) {
        manufacturerType = when (selectedIndex.toString()) {
            PhoneManufacturerType.APPLE.id -> PhoneManufacturerType.APPLE
            PhoneManufacturerType.XIOMI.id -> PhoneManufacturerType.XIOMI
            PhoneManufacturerType.HTC.id -> PhoneManufacturerType.HTC
            else -> PhoneManufacturerType.OTHER
        }
        brandPhone.set(manufacturerType?.getBrand() ?: "")
    }

    fun onPhoneManufacturerClick() = getNavigator()?.createSelectManufacturerDialog()
    fun openFileChooser() = getNavigator()?.openFileChooser()
    fun uploadFile() = getNavigator()?.uploadFile()

    fun displaySetting() {
        navigateToPhoneCharacteristics.value = true
    }

    fun displaySettingComplete() {
        navigateToPhoneCharacteristics.value = false
    }

    fun removeFileItem(phoneFile: PhoneFile) {
        phoneFilesList.remove(phoneFile)
        restoreButtonPlus()
    }

    fun restoreButtonPlus() {
        phoneFilesList.add(PhoneFile(PhoneFileType.ADD_BTN_TYPE))
        Log.d(TAG, "dm:: restore button '+'")
    }

    private fun setItems(newFilesList: List<PhoneFile> = ArrayList()) {
        phoneFilesList.clear()
        phoneFilesList.addAll(newFilesList)
        phoneFilesList.add(PhoneFile(PhoneFileType.ADD_BTN_TYPE))
//        isPhoneFilesListEmpty.set(phoneFilesList.isEmpty())  // !!
    }

}


enum class PhoneManufacturerType (val id: String){
    APPLE("1"){ override fun getBrand(): String = "Apple" },
    XIOMI("2"){ override fun getBrand(): String = "Xiomi" },
    HTC("3"){ override fun getBrand(): String = "HTC" },
    OTHER("4"){ override fun getBrand(): String = "Other" };

    abstract fun getBrand(): String
}
