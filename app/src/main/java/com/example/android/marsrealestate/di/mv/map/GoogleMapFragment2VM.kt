package com.example.android.marsrealestate.di.mv.map

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.model.Prod
import com.example.android.marsrealestate.network.RetrofitApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class GoogleMapFragment2VM() : ViewModel() {
    private val TAG = "GoogleMapFragment2VM"

    //    val status = MutableLiveData<MainApiStatus>()
    val prods = MutableLiveData<List<Prod>>()

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
        getListProducts()
    }

    fun getListProducts(str: String = "") {
        coroutineScope.launch {
            var getPropertiesDeferred = if (str.isEmpty()) RetrofitApi.retrofitApiService.getListProd()
            else RetrofitApi.retrofitApiService.getListProd(str)
            try {
//                status.value = MainApiStatus.LOADING
                // this will run on a thread managed by Retrofit
                val listResult = getPropertiesDeferred.await()
//                status.value = MainApiStatus.DONE
                prods.value = listResult
            } catch (e: Exception) {
//                status.value = MainApiStatus.ERROR
                prods.value = listOf()
            }
        }
    }
}