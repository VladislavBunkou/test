package com.example.android.marsrealestate.trailer.addtrailer

///MANUFACTURERS
enum class TrailerManufacturerType (val id: String){
    FRUEHAUF("1"){ override fun getBrand(): String = "Fruehauf" },
    GREAT_DANE("2"){ override fun getBrand(): String = "Great Dane" },
    MAC("3"){ override fun getBrand(): String = "MAC" },
    MANAC("4"){ override fun getBrand(): String = "Manac" },
    OTHER("5"){ override fun getBrand(): String = "Other" };

    abstract fun getBrand(): String
}
