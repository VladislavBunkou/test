package com.example.android.marsrealestate.ui.phone

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentPhoneBinding
import com.example.android.marsrealestate.MainActivity
import com.example.android.marsrealestate.MyApplication
import com.example.android.marsrealestate.di.mv.phone.PhoneViewModel
import com.example.android.marsrealestate.model.Phone
import com.example.android.marsrealestate.repository.phone.PhoneItemListener
import com.example.android.marsrealestate.ui.adapters.phone.PhoneRecyclerAdapter
import javax.inject.Inject


class PhonesFragment : Fragment(), PhoneItemListener {

    lateinit var toolbarNavigator: ToolbarNavigator
    private val viewModel by viewModels<PhoneViewModel>()
//    @Inject
//    lateinit var viewModel: PhoneViewModel

    lateinit var binding: FragmentPhoneBinding

//    override fun onAttach(context: Context) {
//        super.onAttach(context)
//        (activity as MainActivity).appComponent.inject(this)
//    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentPhoneBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.listenerPhone = this
        binding.viewModel = viewModel
        binding.lasersGrid.adapter = PhoneRecyclerAdapter()
        return binding.root
    }

    override fun onSubUserItemClick(phone: Phone) {
        this.findNavController().navigate(PhonesFragmentDirections.actionPhoneFragmentToPhone2Fragment(phone)) // Example 1

//        findNavController().navigate(R.id.action_phoneFragment_to_phone2Fragment,bundleOf(PhoneFragment.PHONE_ID to phone.id)) // Example 2
    }
}

interface ToolbarNavigator {
    fun setToolbar(toolbar: Toolbar?)
    fun setTitle(title: String)
}
