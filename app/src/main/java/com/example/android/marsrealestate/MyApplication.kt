package com.example.android.marsrealestate

import android.app.Application
import com.example.android.marsrealestate.di.AppComponent
import com.example.android.marsrealestate.di.DaggerAppComponent

open class MyApplication : Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.factory().create(applicationContext)
    }


}
