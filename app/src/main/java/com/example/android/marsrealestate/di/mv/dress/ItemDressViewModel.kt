package com.example.android.marsrealestate.di.mv.dress

import android.location.Location
import android.util.Log
import com.example.android.marsrealestate.di.mv.BaseViewModel
import com.example.android.marsrealestate.dress.Dress
import com.example.android.marsrealestate.dress.DressItemListener
import com.example.android.marsrealestate.ui.dress.d_latitude
import com.example.android.marsrealestate.ui.dress.d_longitude

class ItemDressViewModel: BaseViewModel<Any>() {

    var listener : DressItemListener? = null
    lateinit var dress: Dress
   var idistance : String = "-"  //= distance


    fun bind(dress: Dress, listener : DressItemListener?){
        this.dress = dress
        this.listener = listener
        Log.d("ItemDressViewModel:","location longitude ${dress.longitude}")
        Log.d("ItemDressViewModel:","location latitude ${dress.latitude}")

        val l1 = Location("")
        l1.latitude= d_latitude
        l1.longitude= d_longitude

        val l2 = Location("")
        l2.latitude=dress.latitude
        l2.longitude=dress.longitude

        val distance = (l1.distanceTo(l2)/ 1000)
        Log.d("Item", " $distance")
        idistance= "${distance.toString()}km"
        Log.d("Item", " $idistance")
    }

    fun onClickItem(){
        Log.d("", "Is clicked dress = ${dress.model}")
        listener?.onSubUserItemClick(dress)
    }
}