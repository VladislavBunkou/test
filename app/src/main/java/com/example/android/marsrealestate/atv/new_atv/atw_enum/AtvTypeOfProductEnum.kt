package com.example.android.marsrealestate.atv.new_atv.atw_enum

enum class AtvTypeOfProductEnum (val id: String) {
    UTILITY("1"){ override fun get(): String = "Utility" },
    SPORT("2"){ override fun get(): String = "Sport" },
    RHINO("3"){ override fun get(): String = "Rhino" },
    CHILDREN("4"){ override fun get(): String = "For children" };

    abstract fun get(): String
}