package com.example.android.marsrealestate.ui.adapters.dress

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.ItemDressBinding
import com.example.android.marsrealestate.di.mv.dress.ItemDressViewModel
import com.example.android.marsrealestate.dress.Dress
import com.example.android.marsrealestate.dress.DressItemListener


class DressRecyclerAdapter : RecyclerView.Adapter<DressRecyclerAdapter.DressPropertyviewHolder>() {

    private val dressList: ArrayList<Dress> = arrayListOf()

    private lateinit var listener: DressItemListener

    override fun getItemCount(): Int = dressList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DressPropertyviewHolder {
        return DressPropertyviewHolder(ItemDressBinding.inflate(LayoutInflater.from(parent.context)))
    }
    override fun onBindViewHolder(holder: DressPropertyviewHolder, position: Int){
        val itemDressViewModel = ItemDressViewModel()
        itemDressViewModel.bind(dressList[position], listener)
        holder.bind(itemDressViewModel)
    }

    fun addItems(list: List<Dress>) {
        this.dressList.clear()
        this.dressList.addAll(list)
        notifyDataSetChanged()
    }

    fun addListener(listener: DressItemListener){
        Log.d("DressRecyclerAdapter", "Listener is add  = $listener")
        this.listener = listener
    }

    class DressPropertyviewHolder(private var binding: ItemDressBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(itemDressViewModel: ItemDressViewModel) {
            binding.mv = itemDressViewModel
            binding.executePendingBindings()
        }

    }


}