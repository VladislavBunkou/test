package com.example.android.marsrealestate.di.mv.nav

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NavAutoViewModel : ViewModel() {

}

interface NavAutoListener {
    fun clickAvto()
    fun clickBoat()
    fun clickAtv()
    fun clickTrailer()
    fun clickMotorbike()
    fun clickBicycle()
}