package com.example.android.marsrealestate.bicycle

import com.example.android.marsrealestate.enums.ResponseCode
import com.example.android.marsrealestate.enums.Status

class BicycleSaveInfo(
//        protected
        var status: Status,
        val code: ResponseCode = ResponseCode.SUCCESS,
        val message: String = "")


class BicycleGetInfo(
//        protected
        var status: Status,
        val code: ResponseCode = ResponseCode.SUCCESS,
        val message: String = "")