package com.example.android.marsrealestate.tablet.addtablet

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel

class TabletInfoViewModel(app: Application): AndroidViewModel(app) {
    val isTabletHasSIM = ObservableField<Boolean>()
    val isNew = ObservableField<Boolean>()
    val seller = ObservableField<String>("")
    val year = ObservableField<String>("")
}