package com.example.android.marsrealestate.ui.website

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.databinding.FragmentWebSiteBinding

class WebSiteFragment : Fragment() {

    lateinit var webView: WebView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var binding = FragmentWebSiteBinding.inflate(inflater)
        binding.lifecycleOwner = this
        webView = binding.webSiteView

        webView.settings.javaScriptEnabled = true
        getWebview("http://akordirect.com/site_agreement.html")
//        getWebview("http://q11.jvmhost.net/admin/shirt")
        return binding.root
    }


    fun getWebview(myurl: String) {
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                Log.d("WebSiteFragment", "dm:: onPageFinished  url = ${url}")
                if(url.contains("http://akordirect.com/site_agreement_accepted.html")){
                    findNavController().navigate(WebSiteFragmentDirections.actionWebSiteFragmentToMainFragment())
                }
                super.onPageFinished(view, url)
            }
        }
        webView.loadUrl(myurl)
    }
}

