package com.example.android.marsrealestate.tablet.model

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class TabletOneViewModelFactory(private val tablet: Tablet, private val application: Application) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TabletOneViewModel::class.java)) {
            return TabletOneViewModel(tablet, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}