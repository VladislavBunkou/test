package com.example.android.marsrealestate.ui.nav

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.databinding.FragmentNavElectronicBinding
import com.example.android.marsrealestate.di.mv.nav.NavElectronikViewModel

class NavElectronikFragment : Fragment() {

    val TAG = "NavElectronikFragment"

    private val viewModel: NavElectronikViewModel by lazy { ViewModelProviders.of(this).get(NavElectronikViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentNavElectronicBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        viewModel.navigateToPhone.observe(this, Observer {
            if (it) {
                this.findNavController().navigate(NavElectronikFragmentDirections.actionNavElectronikFragmentToPhoneFragment())
                viewModel.navigationPhoneFinished()
            }
        })

        viewModel.navigateToTablet.observe(this, Observer {
            if (it) {
                this.findNavController().navigate(NavElectronikFragmentDirections.actionNavElectronikFragmentToTabletFragment())
                viewModel.navigationTabletFinished()
            }
        })

        viewModel.navigateToComputer.observe(this, Observer {
            if (it) {
                this.findNavController().navigate(NavElectronikFragmentDirections.actionNavElectronikFragmentToComputerFragment())
                viewModel.navigationComputerFinished()
            }
        })

        return binding.root
    }
}