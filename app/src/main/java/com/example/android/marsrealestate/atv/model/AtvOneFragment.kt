package com.example.android.marsrealestate.atv.model

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.akordirect.vmccnc.databinding.FragmentAtvDetailBinding

class AtvOneFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding =  FragmentAtvDetailBinding.inflate(inflater)
        binding.lifecycleOwner = this

        val atvProperty = AtvOneFragmentArgs.fromBundle(arguments!!).selectedAtvProperty
        val viewModel = AtvOneViewModel(atvProperty)

        binding.viewModel = viewModel

        binding.isNew.text = if (atvProperty.productIsNew) "Yes" else "No"

        return binding.root
    }
}