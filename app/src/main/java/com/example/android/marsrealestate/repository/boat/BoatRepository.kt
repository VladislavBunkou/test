package com.example.android.marsrealestate.repository.boat

import android.location.Location
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.MutableLiveData
import com.example.android.marsrealestate.boat.model.Boat
import com.example.android.marsrealestate.enums.Status
import com.example.android.marsrealestate.globalLatitude
import com.example.android.marsrealestate.globalLongitude
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.ui.boat.boat_latitude
import com.example.android.marsrealestate.ui.boat.boat_longitude
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.lang.Math.abs
import java.lang.Math.pow
import java.util.stream.Collectors
import kotlin.math.pow
import kotlin.math.roundToInt
import kotlin.math.sqrt

var distance: Int = 0

class BoatRepository(val coroutineScope: CoroutineScope) {

    val boats = MutableLiveData<List<Boat>>()
    val saveBoatObservable = MutableLiveData<BoatSaveInfo>()
    val idBoat = MutableLiveData<Long>()
    val receiveBoatObservable = MutableLiveData<BoatGetInfo>() // replace to Repository
    val wordsToSearchBoatObservable = MutableLiveData<String>()
    val modelBoatObservable = MutableLiveData<String>()
    val brandBoatObservable = MutableLiveData<String>()
    val countryBoatObservable = MutableLiveData<String>()
    val cityBoatObservable = MutableLiveData<String>()
    val priceBoatObservable = MutableLiveData<String>()
    val productConditionBoatObservable = MutableLiveData<String>()
    val descriptionBoatObservable = MutableLiveData<String>()
    val imageUrlBoatObservable = MutableLiveData<String>()



    fun getBoatsList(str: String = "") {
        receiveBoatObservable.postValue(BoatGetInfo(Status.LOADING))
        Log.d("BoatRepository", "dm->  1")
        coroutineScope.launch {
            Log.d("BoatRepository", "dm-> 2")
            var getPropertiesDeferred =  if (str.isEmpty()) RetrofitApi.retrofitApiService.getBoatList()
            else RetrofitApi.retrofitApiService.getBoatList(str)
            try {
                Log.d("sorted_Boats", "=================")
//                _status.value = PhoneApiStatus.LOADING
                var listResult = getPropertiesDeferred.await()

                boats.value = sortingBoatList(listResult, boat_latitude, boat_longitude)//.reversed()
                Log.d("BoatRepository", "dm-> 11 / phones.value = ${boats.value?.size}" )
                receiveBoatObservable.postValue(BoatGetInfo(Status.SUCCESS))
//                receivePhoneObservable.postValue(PhoneGetInfo(Status.ERROR)
            } catch (e: Exception) {
//                _status.value = PhoneApiStatus.ERROR
                Log.d("BoatRepository", "dm-> Internet connection not exist or Server is shot down")
                boats.value = ArrayList()
                receiveBoatObservable.postValue(BoatGetInfo(Status.ERROR_CONNECTION))
            }

        }
    }

    fun sortingBoatList(list: List<Boat>, latitude: Double, longitude: Double): List<Boat> {
        val sortedList = list.sortedWith(object : Comparator<Boat> {
            override fun compare(o1: Boat?, o2: Boat?): Int {
                var distance1 = FloatArray(1)
                var distance2 = FloatArray(1)
                Location.distanceBetween(
                        latitude,
                        longitude,
                        o1?.latitude?.toDouble() ?: 0.0,
                        o1?.longitude?.toDouble() ?: 0.0,
                        distance1
                )
                Location.distanceBetween(
                        latitude,
                        longitude,
                        o2?.latitude?.toDouble() ?: 0.0,
                        o2?.longitude?.toDouble() ?: 0.0,
                        distance2
                )
                val difference = (distance1[0] - distance2[0]).roundToInt()
                Log.d("DressRepository", "$difference")
                distance = kotlin.math.abs(difference) /1000
                Log.d("DressRepository", "$distance")
                return difference
            }
        })
        return sortedList
    }


    fun receiveBoatById(id: Long, isSaveNewBoatOnServer: Boolean) {
        receiveBoatObservable.postValue(BoatGetInfo(Status.LOADING)) // change
        if (isSaveNewBoatOnServer) {
            receiveBoatObservable.postValue(BoatGetInfo(Status.SUCCESS))
            Log.d("BoatRepository", "dm->  getMarsRealEstateProperties / id = $id - means new Boat")
        } else
        {
            coroutineScope.launch {
                idBoat.postValue(id)
                Log.d("BoatRepository", "dm->  getMarsRealEstateProperties / id = $id")
                //            var getPropertiesDeferred = MarsApi.retrofitService.getProperties(filter.value)
                var getPropertiesDeferred = RetrofitApi.retrofitApiService.getBoatOne(id)
                try {
//                _status.value = MarsApiStatus.LOADING
                    // this will run on a thread managed by Retrofit
                    val boatOne = getPropertiesDeferred.await()

                    Log.d("Boat DetailViewModel", "dm-> try boat = $boatOne")
                    wordsToSearchBoatObservable.postValue(boatOne.wordsToSearch)
                    modelBoatObservable.postValue(boatOne.model)
                    brandBoatObservable.postValue(boatOne.manufacturer)
                    countryBoatObservable.postValue(boatOne.manufacturerCountry)
                    cityBoatObservable.postValue(boatOne.city)
                    priceBoatObservable.postValue(boatOne.price)
                    imageUrlBoatObservable.postValue(boatOne.url)
                    descriptionBoatObservable.postValue(boatOne.description)
                    productConditionBoatObservable.postValue(boatOne.productCondition)
                    receiveBoatObservable.postValue(BoatGetInfo(Status.SUCCESS))
//                _status.value = MarsApiStatus.DONE
//                listResult.id = id
//                _selectedProperty.value = tabletOne
                } catch (e: Exception) {
//                _status.value = MarsApiStatus.ERROR
//                _selectedProperty.value = Prod()
                    Log.d("Boat DetailViewModel", "dm-> catch "+e)
                    receiveBoatObservable.postValue(BoatGetInfo(Status.ERROR_CONNECTION))
                }
                }
        }
    }
}

