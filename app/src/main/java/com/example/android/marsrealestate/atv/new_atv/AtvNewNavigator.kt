package com.example.android.marsrealestate.atv.new_atv

import com.example.android.marsrealestate.atv.AtvBody

interface AtvNewNavigator {
    fun showToast(atv: AtvBody)
    fun showErrors(isModelFilled: Boolean, isManufacturerFilled: Boolean,
                   isManufacturerCountryFilled: Boolean, isTypeOfProductFilled: Boolean,
                   isDescriptionFilled: Boolean, isMileageFilled: Boolean,
                   isFirstRegistrationFilled: Boolean, isCubicCapacityFilled: Boolean,
                   isPowerSupplyFilled: Boolean, isGearBoxFilled: Boolean, isUriFilled: Boolean)

    fun createSelectTypeOfProductDialog()
    fun createSelectManufacturerDialog()
    fun createSelectManufacturerCountryDialog()

    //fun onProductIsNewSwitchClick()

    fun openFileChooser()
    fun uploadFile()
    fun getPhotoId(): String
}