package com.example.android.marsrealestate.ui.dress

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentDressDetailBinding
import com.example.android.marsrealestate.di.mv.dress.DressDetailViewModel
import com.example.android.marsrealestate.di.mv.dress.DressDetailViewModelFactory



class DressDetailFragment : Fragment() {

    private val viewModel: DressDetailViewModel by lazy {
        ViewModelProviders.of(this).get(DressDetailViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val application = requireNotNull(activity).application
        val binding =  FragmentDressDetailBinding.inflate(inflater)

        binding.lifecycleOwner = this


        val dress = DressDetailFragmentArgs.fromBundle(arguments!!).selectedDressProperty

        val viewModelFactory = DressDetailViewModelFactory(dress, application)

        binding.viewModel = ViewModelProviders.of(this, viewModelFactory).get(DressDetailViewModel::class.java)

        return binding.root
    }
}

