package com.example.android.marsrealestate.guitar.guitar_list

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.databinding.FragmentGuitarListBinding
import com.akordirect.vmccnc.databinding.FragmentGuitarListBindingImpl

class GuitarListFragment : Fragment() {

    private val viewModel: GuitarListViewModel by lazy {
        ViewModelProviders.of(this).get(GuitarListViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentGuitarListBinding.inflate(inflater)
        binding.data = viewModel
        binding.lifecycleOwner = this

        binding.guitarList.adapter = GuitarRecyclerAdapter(GuitarRecyclerAdapter.OnClickListenerGuitar {
            viewModel.displayDetailGuitar(it)
        })

        viewModel.navigateToSelectedGuitar.observe(this, Observer {
            if ( null != it ) {
                this.findNavController()
                        .navigate(GuitarListFragmentDirections.actionShowGuitarDetail(it))
                viewModel.displayDetailGuitarComplete()
            }
        })

        return binding.root
    }
}
