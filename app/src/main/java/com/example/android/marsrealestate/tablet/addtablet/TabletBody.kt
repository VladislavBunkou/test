package com.example.android.marsrealestate.tablet.addtablet

import com.google.gson.annotations.SerializedName

class TabletBody (
        @SerializedName("model")
        var model: String = "",

        @SerializedName("manufacturer")
        var manufacturer: String = "",

        @SerializedName("manufacturerCountry")
        val manufacturerCountry: String,
        //  val url: String,

        @SerializedName("productionYear")
        val productionYear: String,

        @SerializedName("screenDiagonal")
        val screenDiagonal: String,

        @SerializedName("screenResolution")
        val screenResolution: String,

        @SerializedName("ram")
        val ram: String,

        @SerializedName("hasSim")
        val hasSim: Boolean,

        @SerializedName("cameraResolution")
        val cameraResolution: String,

        @SerializedName("seller")
        val seller: String,

        @SerializedName("productIsNew")
        val productIsNew: Boolean
)