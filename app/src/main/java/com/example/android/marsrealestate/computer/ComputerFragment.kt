package com.example.android.marsrealestate.computer

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.databinding.FragmentComputerBinding

/**
 * Класс ComputerFragment
 * @author Ilya Osipov (mailto:il.osipov.gm@gmail.com)
 * @since 27.04.2020
 * @version $Id$
 */

class ComputerFragment : Fragment() {

    private val viewModel : ComputerViewModel by lazy {
        ViewModelProviders.of(this).get(ComputerViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?) : View? {
        val binding = FragmentComputerBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        binding.lasersGrid.adapter = ComputerRecyclerAdapter(ComputerRecyclerAdapter.OnClickListenerComputer {
            viewModel.displayOneLathe(it)
            Log.d("LaserListFragment", " dm-> It will redirect to to One Computer")
        })

        viewModel.navigateToSelectedComputer.observe(this.viewLifecycleOwner, Observer {
            if (null != it) { this.findNavController().navigate(ComputerFragmentDirections.actionComputerFragmentToComputerOneFragment(it))
                viewModel.displayOneLatheComplete()
            }
        })
        return binding.root
    }
}