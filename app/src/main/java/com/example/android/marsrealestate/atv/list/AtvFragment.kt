package com.example.android.marsrealestate.atv.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.databinding.FragmentAtvBinding

class AtvFragment: Fragment() {
    private val viewModel: AtvViewModel by lazy {
        ViewModelProviders.of(this).get(AtvViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentAtvBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        binding.lasersGrid.adapter = AtvRecyclerAdapter(AtvRecyclerAdapter.OnClickListenerAtv {
            viewModel.displayOneAtv(it)
        })

        viewModel.navigateToSelectedAtv.observe(this, Observer {
            if ( null != it ) {
                this.findNavController()
                        .navigate(AtvFragmentDirections.actionAtvFragmentToAtvOneFragment(it))
                viewModel.displayOneAtvComplete()
            }
        })

        return binding.root
    }
}