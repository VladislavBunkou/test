package com.example.android.marsrealestate.guitar.guitar_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.GridViewItemGuitarBinding
import com.example.android.marsrealestate.guitar.Guitar

class GuitarRecyclerAdapter(private val onClickListenerGuitar: OnClickListenerGuitar ) :
        ListAdapter<Guitar, GuitarRecyclerAdapter.GuitarViewHolder>(GuitarDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = GuitarViewHolder(GridViewItemGuitarBinding.inflate(LayoutInflater.from(parent.context)))

    override fun onBindViewHolder(holder: GuitarViewHolder, position: Int) {
        val guitar = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListenerGuitar.onClick(guitar)
        }
        holder.bind(guitar)
    }

    companion object GuitarDiffCallback : DiffUtil.ItemCallback<Guitar>() {
        override fun areItemsTheSame(oldItem: Guitar, newItem: Guitar) = oldItem === newItem

        override fun areContentsTheSame(oldItem: Guitar, newItem: Guitar) = oldItem.model == newItem.model
    }

    class GuitarViewHolder(private var binding: GridViewItemGuitarBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(guitar: Guitar) {
            binding.data = guitar
            binding.executePendingBindings()
        }
    }

    //when clicking guitar item
    class OnClickListenerGuitar(val clickListener: (guitar: Guitar) -> Unit) {
        fun onClick(guitar: Guitar) = clickListener(guitar)
    }
}