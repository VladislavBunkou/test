package com.example.android.marsrealestate.di.mv.dress

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.dress.Dress
import com.example.android.marsrealestate.network.RetrofitApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class GoogleMapDressViewModel:ViewModel(){
    private val TAG = "Dress Map ViewModel"

    val dresses = MutableLiveData<List<Dress>>()

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
        getDressesList()
    }

    fun getDressesList(){
        coroutineScope.launch {
            var getPropertiesDeferred = RetrofitApi.retrofitApiService.getDressList()
            try {
//                status.value = MainApiStatus.LOADING
                // this will run on a thread managed by Retrofit
                val listResult = getPropertiesDeferred.await()
//                status.value = MainApiStatus.DONE
                dresses.value = listResult
            } catch (e: Exception) {
//                status.value = MainApiStatus.ERROR
                dresses.value = listOf()
            }
        }
    }

}