package com.example.android.marsrealestate.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.BoatItemBinding
import com.example.android.marsrealestate.boat.model.Boat
import com.example.android.marsrealestate.di.mv.boat.ItemBoatViewModel
import com.example.android.marsrealestate.repository.boat.BoatItemListener
import com.example.android.marsrealestate.showDistance

class BoatRecyclerAdapter() : RecyclerView.Adapter<BoatRecyclerAdapter.BoatPropertyviewHolder>() {

    private val boatsList: ArrayList<Boat> = arrayListOf()
    private lateinit var listener: BoatItemListener

    override fun getItemCount(): Int = boatsList.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BoatPropertyviewHolder {
        return BoatPropertyviewHolder(BoatItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: BoatPropertyviewHolder, position: Int) {
        val itemPhoneViewModel = ItemBoatViewModel()
        itemPhoneViewModel.distanceShow = showDistance
        itemPhoneViewModel.bind(boatsList[position], listener)
        holder.bind(itemPhoneViewModel)

    }

    fun addItems(list: List<Boat>) {
        this.boatsList.clear()
        this.boatsList.addAll(list)
        notifyDataSetChanged()
    }

    fun addListener(listener: BoatItemListener){
        this.listener = listener
    }

    class BoatPropertyviewHolder(private var binding: BoatItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(itemPhoneViewModel: ItemBoatViewModel) {
            binding.mv = itemPhoneViewModel
            binding.executePendingBindings()
        }

    }
}