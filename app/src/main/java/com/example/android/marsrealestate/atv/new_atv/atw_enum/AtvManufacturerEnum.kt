package com.example.android.marsrealestate.atv.new_atv.atw_enum

enum class AtvManufacturerEnum (val id: String) {
    OTHER("0"){ override fun get(): String = "Other" },
    HONDA("1"){ override fun get(): String = "Honda" },
    POLARIS("2"){ override fun get(): String = "Polaris" },
    YAMAHA("3"){ override fun get(): String = "Yamaha" },
    SUZUKI("4"){ override fun get(): String = "Suzuki" },
    KAWASAKI("5"){ override fun get(): String = "Kawasaki" },
    ARCTIC_CAT("6"){ override fun get(): String = "Arctic Cat" };

    abstract fun get(): String
}