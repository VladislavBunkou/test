package com.example.android.marsrealestate.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.akordirect.vmccnc.R
import com.example.android.marsrealestate.ui.nav.GlobalNavListener

class GlobalNavExpandableListAdapter(var context: Context,
                                     var expandableListView: ExpandableListView,
                                     var header: MutableList<String>,
                                     var body: MutableList<MutableList<String>>,
                                     private var listenerGlobal: GlobalNavListener) : BaseExpandableListAdapter() {

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean = true
    override fun hasStableIds(): Boolean = false

    override fun getGroup(groupPosition: Int): Any = header[groupPosition]
    override fun getGroupCount(): Int = header.size
    override fun getGroupId(groupPosition: Int): Long = groupPosition.toLong()

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View? {
        var convertView = convertView
        if (convertView == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.layout_global_group, null)
        }
        val title = convertView?.findViewById<TextView>(R.id.tv_title)
        title?.text = getGroup(groupPosition).toString()
        title?.setOnClickListener {
            if (expandableListView.isGroupExpanded(groupPosition))
                expandableListView.collapseGroup(groupPosition)
            else
                expandableListView.expandGroup(groupPosition)
        }

        var imgExpandCollapse = convertView?.findViewById<ImageView>(R.id.imagGroupIndicator)
        if(isExpanded){
            imgExpandCollapse?.setImageResource(R.drawable.ic_group_indicator_arrow_left);
        }
        else {
            imgExpandCollapse?.setImageResource(R.drawable.ic_group_indicator_arrow_right);
        }
        return convertView
    }


    override fun getChild(groupPosition: Int, childPosition: Int): Any = body[groupPosition][childPosition] //To change body of created functions use File | Settings | File Templates.
    override fun getChildId(groupPosition: Int, childPosition: Int): Long = childPosition.toLong() //To change body of created functions use File | Settings | File Templates.
    override fun getChildrenCount(groupPosition: Int): Int = body[groupPosition].size

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View? {
        var convertView = convertView
        if (convertView == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.layout_global_child, null)
        }
        val title = convertView?.findViewById<TextView>(R.id.tv_title)
        title?.text = getChild(groupPosition, childPosition).toString()
        title?.setOnClickListener {
            listenerGlobal.clickAll(getChild(groupPosition, childPosition).toString())
        }
        return convertView
    }


}