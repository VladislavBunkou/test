package com.example.android.marsrealestate.di.mv.boat

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.android.marsrealestate.boat.model.Boat

class BoatOneViewModelFactory(private val boat: Boat, private val application: Application) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(BoatOneViewModel::class.java)) {
            return BoatOneViewModel(boat, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}