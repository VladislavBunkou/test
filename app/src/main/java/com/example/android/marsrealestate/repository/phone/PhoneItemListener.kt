package com.example.android.marsrealestate.repository.phone

import com.example.android.marsrealestate.model.Phone

interface PhoneItemListener {
    fun onSubUserItemClick(s: Phone)
}