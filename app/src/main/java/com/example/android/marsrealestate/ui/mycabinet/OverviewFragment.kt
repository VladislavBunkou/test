package com.example.android.marsrealestate.ui.mycabinet

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentOverviewBinding
import com.example.android.marsrealestate.di.mv.mycabinet.OverviewViewModel
import com.example.android.marsrealestate.di.mv.nav.NavViewModel
import com.example.android.marsrealestate.itemInCabinet
import com.example.android.marsrealestate.ui.adapters.mycabinet.MyCabinetAdapter
import com.example.android.marsrealestate.ui.phone.PhoneCreateActivity
import com.google.android.material.bottomsheet.BottomSheetDialog


//class MyCabinet : Fragment(R.layout.fragment_my_cabinet)
class OverviewFragment : Fragment(), Ttt {

    private val viewModel by viewModels<OverviewViewModel>()
    private lateinit var navViewModel: NavViewModel // Where we save data about phone

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = FragmentOverviewBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.listener = this
        binding.photosGrid.adapter = MyCabinetAdapter(MyCabinetAdapter.OnClickListener {
            viewModel.displayPropertyDetails(it)
        })
        activity?.let {
            navViewModel = ViewModelProviders.of(it).get(NavViewModel::class.java)
        }

        viewModel.navigateToSelectedProperty.observe(this, Observer {
            if (null != it) {
                navViewModel.id.value = it.id
                Log.d("OverviewFragment", "dm-> prod = ${it.typeOfProduct}")
                when (it.typeOfProduct) {
                    "tablet" -> this.findNavController().navigate(OverviewFragmentDirections.actionOverviewFragmentToTabletNewFragment())
                    "phone" -> {
                        this.findNavController().navigate(OverviewFragmentDirections.actionOverviewFragmentToPhoneNewFragment())
                        navViewModel.isCreateNew.value = false
                    }
                    "motorbike" -> {
                        Log.d("OverviewFragment", "dm-> goin to go on Motorbike")
//                        this.findNavController().navigate(OverviewFragmentDirections.actionOverviewFragmentToMotorbikeItem()
                    }

                    "dress" -> {
                        itemInCabinet = true
                        Log.d("OverviewFragment", "dm-> goin to go on Dress $itemInCabinet")
                        this.findNavController().navigate(OverviewFragmentDirections.actionOverviewFragmentToAddDressFragment())
                        navViewModel.isCreateNew.value = false
                    }

                    "boat" -> {
                        itemInCabinet = true
                        Log.d("OverviewFragment", "dm-> na boat")
                        this.findNavController().navigate(OverviewFragmentDirections.actionOverviewFragmentToBoatNewFragment())
                        navViewModel.isCreateNew.value = false
                    }

                    "trailer" -> {
                        itemInCabinet = true
                        Log.d("GridItem", "dm-> na trailer $itemInCabinet")
                        this.findNavController().navigate(OverviewFragmentDirections.actionOverviewFragmentToNewTrailer())
                        navViewModel.isCreateNew.value = false
                    }

                    else -> {
                        Log.d("OverviewFragment", "dm-> else")

                    }
                }
                viewModel.displayPropertyDetailsComplete()
            }
        })



        setHasOptionsMenu(true)
        return binding.root
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)
        val menuItem = menu.findItem(R.id.search_prod);
        val searchView = menuItem.actionView as SearchView
        searchView.queryHint = "Search..."
//        searchView.setBackgroundColor(Color.parseColor("#EFEFF4"))
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                Log.d("OverviewFragment", "dm:: onQueryTextSubmit")
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                Log.d("OverviewFragment", "dm:: onQueryTextChange = $newText")
                return true
            }

        })
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onClickCreatePhone() {
        val categories = arrayOf("Auto", "Electric", "Dress")

        val subCategoty = arrayOf("Auto", "Bicycle", "ATV", "Motorbike")

        //this is list for selected category in "categories"
        val subAuto = arrayOf("Car","Bicycle","Atv","Motorbike","Boat", "Trailer")
        val subElectric = arrayOf("Tablet", "Phone")
        val subDress = arrayOf("Dress", "Shoes")

        val bottomView: View =
                layoutInflater.inflate(R.layout.sheet_select_prod_to_add, null)
        val botton: Button = bottomView.findViewById(R.id.btnGoToEdit) as Button
        val spinner = bottomView.findViewById(R.id.cities) as Spinner
        val spinner2 = bottomView.findViewById(R.id.cities2) as Spinner

        val adapter = ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, categories)
        var adapter2 = ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, subCategoty)

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        activity?.let {
            val dialog = BottomSheetDialog(it)
            dialog.setContentView(bottomView)
            dialog.show()

            spinner.adapter = adapter;
            spinner.onItemSelectedListener = object : OnItemSelectedListener {
                override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View?, position: Int, id: Long) { // your code here
                    Log.d("List1", "$position $id")
                    when(id) {
                        //use id long to detect
                        //0L - auto    1L - electric    2L - dress
                        0L -> {
                            adapter2 = ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, subAuto)
                        }
                        1L -> {
                            adapter2 = ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, subElectric)
                        }
                        2L -> {
                            adapter2 = ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, subDress)
                        }
                    }
                    spinner2.adapter = adapter2
                }
                override fun onNothingSelected(parentView: AdapterView<*>?) { // your code here
                }
            }
            spinner2.adapter = adapter2;



            botton.setOnClickListener {
                Log.d("OverviewFragment", "dm:: onClickCreatePhone")
                val intent = Intent(activity, PhoneCreateActivity::class.java)
                startActivity(intent)
            }

        }


    }


//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        viewModel.updateFilter(
//                when (item.itemId) {
//                    R.id.show_rent_menu -> MarsApiFilter.SHOW_RENT
//                    R.id.show_buy_menu -> MarsApiFilter.SHOW_BUY
//                    R.id.go_to_vmc -> MarsApiFilter.SHOW_BUY
//                    else -> MarsApiFilter.SHOW_ALL
//                }
//        )
//        return true
//    }
}


interface Ttt {
    fun onClickCreatePhone()
}