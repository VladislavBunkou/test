package com.example.android.marsrealestate.computer.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Computer - модель компьютера
 * @author Ilya Osipov (mailto:il.osipov.gm@gmail.com)
 * @since 27.04.2020
 * @version $Id$
 */

@Parcelize
class Computer (
        val id : Long,
        val userId : String,
        val model : String,
        val typeOfProduct : String,
        val manufacturer : String,
        val manufacturerCountry : String,
        val productIsNew : Boolean,
        val description : String,
        val city : String,
        val latitude : Double,
        val longitude : Double,
        val price : Int,
        val category : String,
        val seller : String,
        val productCondition : String,
        val url : String,
        val wordsToSearch : String
) : Parcelable

@Parcelize
class ComputerOne (
        val userId : String,
        val model : String,
        val typeOfProduct : String,
        val manufacturer : String,
        val manufacturerCountry : String,
        val productIsNew : Boolean,
        val description : String,
        val city : String,
        val latitude : Double,
        val longitude : Double,
        val price : Int,
        val category : String,
        val seller : String,
        val productCondition : String,
        val url : String,
        val wordsToSearch : String
) : Parcelable