package com.example.android.marsrealestate.boat.addboat

import com.google.gson.annotations.SerializedName

data class BoatBody (
        var title: String = "",
        var userId: String = "",//+
        var model: String = "",//+
        var typeOfProduct: String = "boat",//+
        var manufacturer: String = "",//+
        var manufacturerCountry: String = "",//+
        var productIsNew: Boolean = false,
        var description: String = "",//+
        var city: String = "",//+
        var wordsToSearch: String = "",
        //lat & long
        //var year: Int = 0,//-
        //var engineType: String = "",//-
        //var enginePower: Int = 0,//-
        //var hullMaterial: String = "",//-
        var price: String = "",//+
        var category: String = "", //+
        var seller: String = "",//+
        var productCondition: String = "",//+

        var latitude: Double = 0.0,
        var longitude: Double = 0.0,

        var length: Double = 0.0,//+
        var width: Double = 0.0,//+
        var weight: Double = 0.0,//+

        var url: String = "https://cdn.boatinternational.com/bi_prd/bi/library_images/F1dOdQKWRSSwiVACmGJw_alfa-nero-burgesss-med-charter-1600x900.jpg"//+
)