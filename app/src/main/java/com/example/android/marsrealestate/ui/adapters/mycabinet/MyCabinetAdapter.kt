package com.example.android.marsrealestate.ui.adapters.mycabinet

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.GridViewItemBinding
import com.example.android.marsrealestate.model.Prod


class MyCabinetAdapter(private val onClickListener: OnClickListener) :
        ListAdapter<Prod, MyCabinetAdapter.ProdViewHolder>(DiffCallback) {

    class ProdViewHolder(private var binding: GridViewItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(prod: Prod) {
            binding.property = prod
            binding.executePendingBindings()
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Prod>() {
        override fun areItemsTheSame(oldItem: Prod, newItem: Prod): Boolean = oldItem === newItem
        override fun areContentsTheSame(oldItem: Prod, newItem: Prod): Boolean = oldItem.id == newItem.id
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProdViewHolder =
            ProdViewHolder(GridViewItemBinding.inflate(LayoutInflater.from(parent.context)))


    override fun onBindViewHolder(holder: ProdViewHolder, position: Int) {
        val prod = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(prod)
        }
        holder.bind(prod)
    }


    class OnClickListener(val clickListener: (prod: Prod) -> Unit) {
        fun onClick(prod: Prod) = clickListener(prod)
    }
}

