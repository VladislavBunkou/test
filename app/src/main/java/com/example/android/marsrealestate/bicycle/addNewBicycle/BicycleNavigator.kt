package com.example.android.marsrealestate.bicycle.addNewBicycle

import androidx.databinding.ObservableField

interface BicycleNavigator {

    fun showToast (model: String)

    fun errorMessages(
            isModelFilled: Boolean,
            isManufacturerFilled: Boolean,
            isPriceFilled: Boolean

    )

    fun getFolding() : ObservableField<Boolean>
    fun getMountain() : ObservableField<Boolean>
    fun getCitybike() : ObservableField<Boolean>
    fun getRoad() : ObservableField<Boolean>
    fun getHibrid() : ObservableField<Boolean>

    fun getProductIsNew():ObservableField<Boolean>

    fun createSelectManufacturerDialog()

    fun openFileChooser()
    fun uploadFile()
    fun getPhotoId(): String
}