package com.example.android.marsrealestate.dress.addDress

import com.example.android.marsrealestate.enums.ResponseCode
import com.example.android.marsrealestate.enums.Status

class DressSaveInfo(
//        protected
        var status: Status,
        val code: ResponseCode = ResponseCode.SUCCESS,
        val message: String = "")


class DressGetInfo(
//        protected
        var status: Status,
        val code: ResponseCode = ResponseCode.SUCCESS,
        val message: String = "")