package com.example.android.marsrealestate.dress.addDress

enum class DressSleeveType(val id: String) {
    FULL("1"){ override fun getSleeveType(): String = "Full" },
    SLEEVELESS("2"){ override fun getSleeveType(): String = "Sleeveless" },
    SHORT("3"){ override fun getSleeveType(): String = "Short" },
    THREEQ("4"){ override fun getSleeveType(): String = "3/4" },
    HALF("5"){ override fun getSleeveType(): String = "1/2" },
    OTHER("6"){ override fun getSleeveType(): String = "Other" };

    abstract fun getSleeveType(): String

}