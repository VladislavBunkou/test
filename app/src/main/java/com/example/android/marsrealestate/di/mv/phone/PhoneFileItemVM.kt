package com.example.android.marsrealestate.di.mv.phone

import com.example.android.marsrealestate.di.mv.BaseViewModel

class PhoneFileItemVM(
        val item: PhoneFile,
        private val mListener: PhoneFileItemVMListener) : BaseViewModel<Any>() {

    fun onAddProfileFileClick() = mListener.onAddPhoneFile()
    fun onDeleteProfileFileClick()  =  mListener.onDeletePhoneFile(item)

    interface PhoneFileItemVMListener {
        fun onAddPhoneFile()
        fun onDeletePhoneFile(profileFile: PhoneFile)
    }
}


data class PhoneFile(
        val profileFileType: PhoneFileType,
        var profileUploadedId: String? = null,
        val item: String? = null)

enum class PhoneFileType {
    JPEG_TYPE, ADD_BTN_TYPE
}

fun List<PhoneFile>.getAllUploadedProfileIds(): Array<String?> =
        this.mapNotNull { prifileFile -> prifileFile.profileUploadedId }.toTypedArray()
