package com.example.android.marsrealestate.repository.phone

import com.example.android.marsrealestate.enums.ResponseCode
import com.example.android.marsrealestate.enums.Status

class PhoneSaveInfo(
//        protected
        var status: Status,
        val code: ResponseCode = ResponseCode.SUCCESS,
        val message: String = "")


class PhoneGetInfo(
//        protected
        var status: Status,
        val code: ResponseCode = ResponseCode.SUCCESS,
        val message: String = "")