package com.example.android.marsrealestate.bicycle.addNewBicycle

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel

class BicycleInfoViewModel( app: Application) : AndroidViewModel(app)  {
    val folding = ObservableField<Boolean>(false)
    val mountain = ObservableField<Boolean>(false)
    val road = ObservableField<Boolean>(false)
    val citybike = ObservableField<Boolean>(false)
    val hibrid = ObservableField<Boolean>(false)

    val productIsNew = ObservableField<Boolean>(false)
}