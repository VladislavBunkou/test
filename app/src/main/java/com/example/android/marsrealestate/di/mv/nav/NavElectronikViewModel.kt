package com.example.android.marsrealestate.di.mv.nav

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NavElectronikViewModel : ViewModel() {


    val navigateToPhone = MutableLiveData<Boolean>()
    val navigateToTablet = MutableLiveData<Boolean>()
    val navigateToComputer = MutableLiveData<Boolean>()


    fun onPhoneClick() {
        navigateToPhone.value = true
    }
    fun navigationPhoneFinished() {
        navigateToPhone.value = false
    }

    // Tablet
    fun onTabletClick(){
        navigateToTablet.value = true
    }
    fun navigationTabletFinished() {
        navigateToTablet.value = false
    }

    // Computer
    fun onComputerClick() {
        navigateToComputer.value = true
    }
    fun navigationComputerFinished() {
        navigateToComputer.value = false
    }
}