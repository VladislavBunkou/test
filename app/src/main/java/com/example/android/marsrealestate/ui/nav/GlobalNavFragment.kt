package com.example.android.marsrealestate.ui.nav

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import android.widget.Toast
import androidx.navigation.fragment.findNavController

import com.akordirect.vmccnc.R
import com.example.android.marsrealestate.ui.adapters.GlobalNavExpandableListAdapter

class GlobalNavFragment : Fragment(), GlobalNavListener {
    private val TAG = "GlobalNavFragment"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.global_nav_fragment, container, false)

        val header = mutableListOf("Auto", "Electric", "Dress","Music")
        val body = mutableListOf<MutableList<String>>()

        body.add(mutableListOf("ATV","Boat", "Bicycle", "Car", "Motorbike", "Trailer")) // auto
        body.add(mutableListOf("Computer","Phone", "Tablet")) // electric
        body.add(mutableListOf("Dress", "Shoes")) // clothes
        body.add(mutableListOf("Guitar")) // music


        var expList = root.findViewById<ExpandableListView>(R.id.expandableListView)

        expList.setAdapter(context?.let {
            GlobalNavExpandableListAdapter(it, expList, header, body, this)
        })
        return root
    }

    override fun clickAll(str: String) {
        Log.d(TAG, "dm:: Avto is clicked! - $str")
        when {
            //some categories not work, cause not created yet
            //AUTO
            str.toLowerCase() == "car" -> Toast.makeText(context, "not exist yet", Toast.LENGTH_SHORT).show()//this.findNavController().navigate(GlobalNavFragmentDirections.actionGlobalNav)
            str.toLowerCase() == "boat" -> this.findNavController().navigate(GlobalNavFragmentDirections.actionGlobalNavToCreateBoat())
            str.toLowerCase() == "bicycle" -> this.findNavController().navigate(GlobalNavFragmentDirections.actionGlobalNavFragment2ToAddBicycleFragment2())
            str.toLowerCase() == "atv" -> this.findNavController().navigate(GlobalNavFragmentDirections.actionGlobalNavFragment2ToAtvNewFragment())
            str.toLowerCase() == "motorbike" -> Toast.makeText(context, "not exist yet", Toast.LENGTH_SHORT).show()//this.findNavController().navigate(GlobalNavFragmentDirections.actionGlobalNavToCreateBoat())
            str.toLowerCase() == "trailer" -> this.findNavController().navigate(GlobalNavFragmentDirections.actionGlobalNavToNewTrailer())

            //ELECTRICK
            str.toLowerCase() == "computer" -> this.findNavController().navigate(GlobalNavFragmentDirections.actionGlobalNavFragment2ToComputerNewFragment())
            str.toLowerCase() == "phone" -> Toast.makeText(context, "not ready yet", Toast.LENGTH_SHORT).show()//this.findNavController().navigate(GlobalNavFragmentDirections.actionGlobalNavToCretePhone()) // Currently does not work
            str.toLowerCase() == "tablet" -> this.findNavController().navigate(GlobalNavFragmentDirections.actionGlobalNavToCreateTablet())

            //DRESS
            str.toLowerCase() == "dress" -> this.findNavController().navigate(GlobalNavFragmentDirections.actionGlobalNavFragment2ToAddDressFragment())
            str.toLowerCase() == "shoes" -> this.findNavController().navigate(GlobalNavFragmentDirections.actionGlobalNavFragment2ToAddShoesFragment())

            //MUSIC
            str.toLowerCase() == "guitar" -> this.findNavController().navigate(GlobalNavFragmentDirections.actionGlobalNavFragment2ToGuitarAddFragment())
        }

    }
}

interface GlobalNavListener {
    fun clickAll(str: String)
}