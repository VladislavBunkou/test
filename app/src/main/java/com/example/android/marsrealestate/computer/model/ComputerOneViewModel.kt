package com.example.android.marsrealestate.computer.model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/**
 * Класс ComputerOneViewModel
 * @author Ilya Osipov (mailto:il.osipov.gm@gmail.com)
 * @since 27.04.2020
 * @version $Id$
 */

class ComputerOneViewModel(computer: Computer, app: Application) : AndroidViewModel(app) {

    val selectedProperty: LiveData<Computer> get() = _selectedProperty
    private val _selectedProperty = MutableLiveData<Computer>()

    init {
        _selectedProperty.value = computer
    }
}