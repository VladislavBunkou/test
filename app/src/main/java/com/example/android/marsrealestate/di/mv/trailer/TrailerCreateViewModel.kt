package com.example.android.marsrealestate.di.mv.trailer

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.android.marsrealestate.network.RetrofitApi
import com.example.android.marsrealestate.di.mv.BaseViewModel
import com.example.android.marsrealestate.enums.Status
import com.example.android.marsrealestate.itemInCabinet
import com.example.android.marsrealestate.repository.trailer.TrailerRepository
import com.example.android.marsrealestate.repository.trailer.TrailerSaveInfo
import com.example.android.marsrealestate.trailer.addtrailer.TrailerBody
import com.example.android.marsrealestate.trailer.addtrailer.TrailerManufacturerType
import com.example.android.marsrealestate.ui.adapters.TrailerNavigator
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class TrailerCreateViewModel : BaseViewModel<TrailerNavigator>() {


    private val TAG = "TrailerNew"

    var trailerRepository: TrailerRepository

    var trailerSavedId = ObservableField<Long>()
    var modelTrailer = ObservableField<String>("")
    var countryTrailer = ObservableField<String>("")
    var brandTrailer = ObservableField<String>("")
    var productIsNew = ObservableField<Boolean>(false)
    var productLatitude = 0.0
    var productLongitude = 0.0
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    val saveTrailerObservable = MutableLiveData<TrailerSaveInfo>()

    private var manufacturerType: TrailerManufacturerType? = null

    var PICK_IMAGE_REQUEST = 1

    //lateinit private var trailer: TrailerBody

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope( viewModelJob + Dispatchers.Main )


    private val saveTrailerObserver = Observer<TrailerSaveInfo> {
        when (it.status) {
            Status.INITIAL, Status.LOADING -> {
                setIsLoading(true)
                Log.d("TrailerNewViewModel", " -> Status.Loading")
            }
            Status.SUCCESS -> {
                setIsLoading(false)
                Log.d("TrailerNewViewModel", " -> Status.SUCCESS")
            }
            Status.ERROR -> {
                setIsLoading(false)
                Log.d("TrailerNewViewModel", " -> Status.Error")
            }
            Status.ERROR_CONNECTION -> {
                setIsLoading(false)
            }
            else -> {
            }
        }
    }

    init {
        saveTrailerObservable.observeForever(saveTrailerObserver)
        trailerRepository = TrailerRepository(coroutineScope)


        if(itemInCabinet){
            //fill fields if update trailer
            trailerRepository.idTrailer.observeForever(Observer { trailerSavedId.set(it) })
            trailerRepository.modelTrailerObservable.observeForever(Observer { modelTrailer.set(it) })
            trailerRepository.brandTrailerObservable.observeForever(Observer { brandTrailer.set(it) })
            trailerRepository.countryTrailerObservable.observeForever(Observer { countryTrailer.set(it) })
            Log.d("observerForever","update trailer")
        } else {
            //if add trailer fields be empty
            Log.d("observerForever","new trailer")
        }
    }

    fun receiveTrailerById(id: Long, isSaveNewTrailerOnServer: Boolean) {
        trailerRepository.receiveTrailerById(id, isSaveNewTrailerOnServer)
    }

    fun saveButton(){
        if(allFieldsFill()) {
            saveTrailerObservable.postValue(TrailerSaveInfo(Status.LOADING))
            uploadFile()
        } else {
            getNavigator()?.showToast(model = "Trailer not saved")
        }
    }

    //fun onSendButton(activity: Activity){
    fun onSendButton(){

        val idUserFirebase = if (FirebaseAuth.getInstance().currentUser != null) FirebaseAuth.getInstance().currentUser?.uid else "-"
        Log.d(TAG, "dm-> idUserFirebase = $idUserFirebase")

        val trailer = TrailerBody().apply {
            userId = idUserFirebase ?: ""
            model = modelTrailer.get() ?: ""
            typeOfProduct = "trailer"
            manufacturer = brandTrailer.get() ?: ""
            manufacturerCountry = countryTrailer.get() ?: ""
            productIsNew = productIsNew
            url = getNavigator()?.getPhotoId() ?: ""
            latitude = productLatitude
            longitude = productLongitude
        }

        Log.d("trailer"," nu id nu pozhaluiyta ${trailerSavedId.get() ?: -1}")

        if (!itemInCabinet){
            Log.d("GridItem", "new trailer $itemInCabinet")
            saveTrailerWithDeffered(trailer)
        }else {
            Log.d("GridItem", "update $itemInCabinet id: ${trailerSavedId.get()}")
            updateTrailer(trailerSavedId.get() ?: 328, trailer)
        }

        modelTrailer.set("")
        countryTrailer.set("")
        brandTrailer.set("")
        productIsNew.set(false)
    }

    private fun updateTrailer(id: Long, trailer: TrailerBody) {
        val idUserFirebase = if (FirebaseAuth.getInstance().currentUser != null) FirebaseAuth.getInstance().currentUser?.uid else "-"
        Log.d(TAG, "dm-> idUserFirebase = $idUserFirebase")

        //Call<Trailer>
        var call: Call<TrailerBody> = RetrofitApi.retrofitApiService.updateTrailer(id, trailer)
        call.enqueue(object: Callback<TrailerBody>{
            override fun onFailure(call: Call<TrailerBody>, t: Throwable) {
                Log.d("TEST PUT trailer", t.message)

                saveTrailerObservable.postValue(TrailerSaveInfo(Status.ERROR))
            }

            override fun onResponse(call: Call<TrailerBody>, response: Response<TrailerBody>) {
                Log.d("TEST PUT trailer", response.message())
                saveTrailerObservable.postValue(TrailerSaveInfo(Status.SUCCESS))
            }

        })
        //clear fields
//        modelTrailer.set("")
//        countryTrailer.set("")
//        brandTrailer.set("")
//        productIsNew.set(false)
    }

    fun allFieldsFill(): Boolean {
        var isModelFilled = false
        var isCountryFilled = false
        var isBrandFilled = false

        if(!modelTrailer.get().isNullOrEmpty()) isModelFilled = true
        Log.d("IsFilled", "model "+isModelFilled.toString())

        if(!countryTrailer.get().isNullOrEmpty()) isCountryFilled = true
        Log.d("IsFilled", isCountryFilled.toString())

        if(!brandTrailer.get().isNullOrEmpty()) isBrandFilled = true
        Log.d("IsFilled", isModelFilled.toString())

        if(!isBrandFilled || !isCountryFilled || !isModelFilled) {
            getNavigator()?.errorMessage(isModelFilled, isCountryFilled)
            return false
        } else {
            return  true
        }
    }

    fun saveTrailerWithDeffered(trailerBody: TrailerBody) {
        saveTrailerObservable.postValue(TrailerSaveInfo(Status.LOADING))

        coroutineScope.launch {
            val getPropertiesDeffered = RetrofitApi.retrofitApiService.saveTrailer(trailerBody)
            try {
                Log.d(TAG, "all_good")
                var trailerResponse = getPropertiesDeffered.await()
                Log.d(TAG, trailerResponse.code().toString())
                if(trailerResponse.isSuccessful)
                    Log.d(TAG, "in isSuccessfull ${trailerResponse.code()}")
                saveTrailerObservable.postValue(TrailerSaveInfo(Status.SUCCESS))
            } catch (e: Exception) {
                saveTrailerObservable.postValue(TrailerSaveInfo(Status.ERROR))
                Log.d("CHECK PUT", "${e.message}")
                saveTrailerObservable.postValue(TrailerSaveInfo(Status.ERROR_CONNECTION))
            }
        }
    }

    fun updateManufacturer(selectedIndex: Int) {
        manufacturerType =
                when (selectedIndex.toString()) {
                    TrailerManufacturerType.FRUEHAUF.id -> TrailerManufacturerType.FRUEHAUF
                    TrailerManufacturerType.GREAT_DANE.id -> TrailerManufacturerType.GREAT_DANE
                    TrailerManufacturerType.MAC.id -> TrailerManufacturerType.MAC
                    TrailerManufacturerType.MANAC.id -> TrailerManufacturerType.MANAC
                    else -> TrailerManufacturerType.OTHER
                }
        brandTrailer.set(manufacturerType?.getBrand() ?: "")
    }

    fun onTrailerManufacturerClick() = getNavigator()?.createSelectManufacturerDialog()
    fun openFileChooser() = getNavigator()?.openFileChooser()
    fun uploadFile() = getNavigator()?.uploadFile()
    fun onIsProductNewClick() = getNavigator()?.onProductIsNew()

}


