package com.example.android.marsrealestate.shoes

import com.example.android.marsrealestate.enums.ResponseCode
import com.example.android.marsrealestate.enums.Status

class ShoesSaveInfo(
//        protected
        var status: Status,
        val code: ResponseCode = ResponseCode.SUCCESS,
        val message: String = "")


class ShoesGetInfo(
//        protected
        var status: Status,
        val code: ResponseCode = ResponseCode.SUCCESS,
        val message: String = "")