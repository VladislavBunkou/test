package com.example.android.marsrealestate.ui.boat

import android.Manifest
import android.app.Activity
import android.content.ContentResolver
import android.content.Intent
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.akordirect.vmccnc.R
import com.akordirect.vmccnc.databinding.FragmentBoatNewBinding
import com.example.android.marsrealestate.di.mv.boat.BoatInfoViewModel
import com.example.android.marsrealestate.di.mv.boat.BoatNewViewModel
import com.example.android.marsrealestate.di.mv.nav.NavViewModel
import com.example.android.marsrealestate.globalLatitude
import com.example.android.marsrealestate.globalLongitude
import com.example.android.marsrealestate.ui.adapters.BoatNavigator
import com.example.android.marsrealestate.ui.map.MapsActivity
import com.example.android.marsrealestate.utils.ValidationUtils
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_boat.*
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions

var boat_latitude = 0.0
var boat_longitude = 0.0

@RuntimePermissions
class BoatNewFragment : Fragment(), BoatNavigator {


    lateinit var binding: FragmentBoatNewBinding

    lateinit var fusedLocationClient: FusedLocationProviderClient// = LocationServices.getFusedLocationProviderClient(activity)

    //For upload image
    lateinit var storage: FirebaseStorage
    lateinit var storageRef: StorageReference
    lateinit var imageView: ImageView
    lateinit var textResult: TextView

    private var imageUri: Uri = Uri.EMPTY
    private var uploadedImageUri: Uri = Uri.EMPTY
    private var fileIsSelected = false

    private val viewModel by viewModels<BoatNewViewModel>()
    // Where we save data about boat
//    private  var boatDetaileViewModel:  BoatInfoViewModel by activityViewModels<>()
    private lateinit var boatInfoViewModel: BoatInfoViewModel
    private lateinit var navViewModel: NavViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val application = requireNotNull(activity).application

        binding = FragmentBoatNewBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        viewModel.setNavigator(this)

        imageView = binding.imageView
        textResult = binding.textViewShowUploads
        storage = FirebaseStorage.getInstance()
        storageRef = storage.reference


        activity?.let {
            boatInfoViewModel = ViewModelProviders.of(it).get(BoatInfoViewModel::class.java)
            navViewModel = ViewModelProviders.of(it).get(NavViewModel::class.java)

        }

        navViewModel.id.observe(this, Observer {
            if(navViewModel.isCreateNew.value == true) {
                viewModel.receiveBoatById(navViewModel.id.value ?: 1, true)
            }else {
            viewModel.receiveBoatById(navViewModel.id.value ?: 1, false)
                navViewModel.isCreateNew.postValue(true)
            }
         })

        viewModel.navigateToBoatCharacteristics.observe(this, Observer {
            if (it) {
                this.findNavController().navigate(
                        BoatNewFragmentDirections.actionBoatNewFragmentToBoatSettingsFragment())
                viewModel.displaySettingComplete()
            }
        })

        binding.buttonChooseFile.setOnClickListener{
            preOpenFileChooserWithPermissionCheck()
        }

                viewModel.navigateToGoogleMap.observe(this, Observer {
                    if(it) {
//                this.findNavController().navigate(
//                        BoatNewFragmentDirections.actionBoatNewFragmentToGoogleMapFragment()
                        Log.d("PhoneCreateFragment", "dm:: onClickCreatePhone")
                        val intent = Intent(activity, MapsActivity::class.java)
                        startActivity(intent)

//                )
                viewModel.displayGoogleMapComplete()
            }
        })

        binding.btnSaveLocation.setOnClickListener {
            Toast.makeText(context, "click", Toast.LENGTH_SHORT).show()
            Log.d("PhoneCreateFragment", "dm:: onClickCreatePhone")
            val intent = Intent(activity, MapsActivity::class.java)
            startActivity(intent)
        }

        preSaveLocationWithPermissionCheck()

        return binding.root
    }

    // step 1
    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun preOpenFileChooser(){
        openFileChooser()
    }

    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    fun preSaveLocation(){
        val task: Task<Location> = fusedLocationClient?.lastLocation
        task?.addOnSuccessListener(OnSuccessListener {
            if (it != null) {
                viewModel.boat_latitude = it.latitude
                viewModel.boat_longitude = it.longitude
                globalLatitude = it.latitude
                globalLongitude = it.longitude
                Log.d("LOCATION", "dm::  latitude = ${viewModel.boat_latitude} / longitude = ${viewModel.boat_longitude}")
            }
        }).addOnFailureListener {
            Log.d("LOCATION", "error: ${it.message}")
        }
    }


    // step 4
    @OnPermissionDenied(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun permissionDenied() {
        Log.d("BoatNewFragment", "dm:: OnPermissionDenied")
    }
    // step 4
    @OnNeverAskAgain(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun neverAskAgain() {
        Log.d("BoatNewFragment", "dm:: OnNeverAskAgain")
    }

    override fun showToast(model: String, country: String) {  // for easier understanding this code can be deleted
        Toast.makeText(context, "model: $model, country: $country", Toast.LENGTH_LONG).show()
    }

    override fun errorMessages(isModelFilled: Boolean, isCountryFilled: Boolean, isWordsToSearchFilled: Boolean) {

        if (!isModelFilled) {
            binding.boatModelLayout.isErrorEnabled = true
            binding.boatModelLayout.error = "Enter model"
        } else {
            binding.boatModelLayout.isErrorEnabled = false
        }

        if (!isCountryFilled) {
            binding.boatCountryLayout.isErrorEnabled = true
            binding.boatCountryLayout.error = "Enter country"
        } else {
            binding.boatCountryLayout.isErrorEnabled = false
        }


        if (!isWordsToSearchFilled) {
            binding.boatWordsToSerachLayout.isErrorEnabled = true
            binding.boatWordsToSerachLayout.error = "Enter title"
        } else {
            binding.boatWordsToSerachLayout.isErrorEnabled = false
        }

    }



    private var currentSelectedManufacturerIndex = -1

    override fun createSelectManufacturerDialog() {
        val listCategory = listOf("BigBoat", "BRIG", "Linder", "Walker Bay")

        val alertDialog: AlertDialog? = requireActivity().let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setSingleChoiceItems(listCategory.toTypedArray(),
                        currentSelectedManufacturerIndex) { dialog, which ->
                    currentSelectedManufacturerIndex = which
                    viewModel.updateManufacturer(which + 1)
                    dialog.dismiss()
                }
                setTitle("Boat producer")
                setNegativeButton("Cancel") { _, _ ->
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }

    override fun getPhotoId() = uploadedImageUri.toString()


    override fun uploadFile() {

        var pathStr: String = "image/boat/${System.currentTimeMillis()}.${getFileExtension(imageUri)}"


        if (imageUri != null) {
            val fileReference: StorageReference = storageRef.child(
                    pathStr
            )


            if(ValidationUtils.isFileHasGoodSize(pathStr))
                Log.d("ValidationUtils", "all good")

            val uploadTask = fileReference.putFile(imageUri)
                    .addOnCompleteListener(){
                        val handler = Handler()
                        handler.postDelayed( {binding.progressBar.progress = 0} , 500)
                        Toast.makeText(context, "Upload succed", Toast.LENGTH_SHORT).show()
                        Picasso.get().load(R.drawable.ic_image_is_uploded).into(imageView)
                    }
                    .addOnFailureListener{ e ->
                        Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
                    }
                    .addOnProgressListener {
                        val progress = 100.0 * it.bytesTransferred / it.totalByteCount
                        binding.progressBar.progress = progress.toInt()
                    }

            val urlTask = uploadTask.continueWithTask{
                if(!it.isSuccessful)
                    it.exception?.let { ex ->
                        throw ex
                    }
                fileReference.downloadUrl
            }
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            uploadedImageUri = it.result ?: Uri.EMPTY
                            Toast.makeText(context, "Url: $uploadedImageUri", Toast.LENGTH_SHORT).show()
                            Log.d("UploadURL", "Url: $uploadedImageUri")
                            viewModel.onSendButton()
                            textResult.text = "Image is uploaded!"
                        } else {
                            Toast.makeText(context, "Something wrong", Toast.LENGTH_SHORT).show()
                        }
                    }
        } else {
            Toast.makeText(context, "No file selected", Toast.LENGTH_SHORT).show()
        }
//        } else {
//            Toast.makeText(context, "File too large", Toast.LENGTH_SHORT)
//        }
    }

    override fun openFileChooser() {
        textResult.text = "_"

        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, viewModel.PICK_IMAGE_REQUEST)
        fileIsSelected = true
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == viewModel.PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK
                && data?.data != null) {
            imageUri = data.data!!
            Picasso.get().load(imageUri).into(imageView)
        }
    }

    private fun getFileExtension(uri: Uri): String? {
        val cR: ContentResolver = activity!!.contentResolver
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(cR.getType(uri))
    }


}